import os
from sys import argv, exit
import subprocess
import json
from multiprocessing import cpu_count as cpuCount
from collections import OrderedDict as OD
from re import findall
from re import compile as comp
from PIL import Image
# for testing
from pprint import pprint as pP

class mkVideo(object):
   ## test video: /home/$USER/Desktop/NULL Code - The Path (Original Mix) FREE.webm

    def __init__(self, *args, **kwargs):
        self.baseDir = os.path.join(os.getcwd())

        ## ______________________________________________________________________________________________##

        ## ----------------------------------- ** setup the args ** -------------------------------------##

        iFiles = list(args)

        if len(iFiles) == 0 and len(kwargs) == 0:
            exit()
        else:
            if not len(iFiles) == 0:
                if iFiles[0] == '-h' or iFiles[0] == '--help':
                    self.mkHelp()
                    exit()
                if iFiles[0] == '-v' or iFiles[0] == '--version':
                    print 'mkH5videos.py version: 0.5'
                    exit()

                fData = {}
                oData = {}
                ask = True
                ffQuestions = False
            self.mkVids = True
        if len(iFiles) > 0:
            for val in iFiles:
                if val == '-y':
                    ask = False
                elif val == '-n':
                    ffQuestions = True
                else:
                    if not os.path.exists(val):
                        print 'File does not exist: %s'%val
                        exit()
                    else:
                        if not os.path.isabs(val):
                            val = os.path.join(os.getcwd(), val)

                        bName = os.path.basename(val).split('.')
                        oData = self.getVidInfo(val)
                        pP(oData)
                        try:
                            r = round(float(oData['streams'][0]['width']) / float(oData['streams'][0]['height']), 3)
                        except Exception, e:
                            if oData['streams'][1]['coded_width'] > 0 and oData['streams'][1]['coded_height'] > 0:
                                r = round(float(oData['streams'][1]['coded_width']) / float(oData['streams'][1]['coded_height']), 3)
                            else:
                                aR = oData['streams'][1]['display_aspect_ratio'].split(':')
                                r = round(float(aR[0]) / float(aR[1]), 3)

                        isMov = bool(bName[1] == 'mov')

                        fData[bName[0]] = {'name': bName[0],
                                           'loc': val,
                                           'size': os.path.getsize(val),
                                           'bitRate': oData['format']['bit_rate'],
                                           'duration': oData['format']['duration'],
                                           'isMov': isMov,
                                           'ratio': r}
        else:
            self.mkVids = False
        ## __________________________________$$ end of args setup $$_____________________________________##

        ## ---------------------------------- ** setup the kwargs ** ------------------------------------##

        ## ---------------** widescreen kwarg **----------------##

        if kwargs.has_key('widescreen'):
            self.widescreen = kwargs['widescreen']
            if self.widescreen == 'false' or self.widescreen is False:
                self.widescreen = False
            elif self.widescreen == 'true' or self.widescreen is True:
                self.widescreen = True
            else:
                print 'Invalid wide screen argument. Please use true or false.'
                exit()
            for fName in fData:
                if self.widescreen is True:
                    if fData[fName]['ratio'] <= 1.3333333 and ask is True:
                        question = 'We have detected that your video is not wide screen format.\n Continuing may distort the final video.\n Would you like to continue encoding your video as wide screen?'

                        print question
                        answer = raw_input('> Yes or No:\n')

                        if answer == 'Yes' or answer == 'yes' or answer == 'y' or answer == 'Y':
                            self.widescreen = True
                        elif answer == 'No' or answer == 'no' or answer == 'n' or answer == 'N':
                            self.widescreen = False
                        else:
                            print question
                            answer = raw_input('> Yes or No:\n')

                else:
                    if not fData[fName]['ratio'] <= 1.3333333 and ask is True:
                        question = 'We have detected that your video is wide screen format.\n Continuing may distort the final video.\n Would you like to continue encoding your video as not wide screen?'

                        print question
                        answer = raw_input('> Yes or No:\n')

                        if answer == 'Yes' or answer == 'yes' or answer == 'y' or answer == 'Y':
                            self.widescreen = False
                        elif answer == 'No' or answer == 'no' or answer == 'n' or answer == 'N':
                            self.widescreen = True
                        else:
                            print question
                            answer = raw_input('> Yes or No:\n')
        else:
            if self.mkVids:
                self.widescreen = None
                for fName in fData:
                    # print fData[fName]['ratio']
                    if fData[fName]['ratio'] <= 1.3333333:
                        # print '4:3'
                        self.widescreen = False
                    else:
                        # print '16:9'
                        self.widescreen = True
            else:
                self.widescreen = True

        ## _______________$$ end of widescreen $$_______________##

        ## -----------------** setsize kwarg **-----------------##
        if kwargs.has_key('setsize'):
            self.setsize = kwargs['setsize']
            if not kwargs.has_key('bitrate'):
                print 'If you set the size of your video you must specify your desired bitrate.'
                exit()
            elif not kwargs.has_key('maxbit'):
                print 'If you set the size of your video you must specify your desired maximum bitrate.'
                exit()
            elif not kwargs.has_key('buffersize'):
                print 'If you set the size of your video you must specify your desired buffer size.'
                exit()
            else:
                width, height = kwargs['setsize'].split('x')

                self.sizs = [{'width': width, 'height': height, 'bit_rate': int(kwargs['bitrate']), 'max_bit': int(kwargs['maxbit']), 'buffer_size': int(kwargs['buffersize'])}]
        else:
            if self.mkVids:
                self.setsize = None
                self.sizs = []
                self.sizs = self.sizes(self.setsize, self.widescreen)

        ## ________________$$ end of setsize $$_________________##

        ## ------------------** cores kwarg **------------------##

        if kwargs.has_key('threds'):
            self.cores = kwargs['threds']
        else:
            self.cores = cpuCount() - 1

        ## _________________$$ end of cores $$__________________##

        ## ------------------** playerSize kwarg **------------------##

        if kwargs.has_key('playersize'):
            self.playerSize = kwargs['playersize'].split('x')
        else:
            if self.widescreen is False:
                self.playerSize = [400, 300]
            else:
                self.playerSize = [400, 240]

        ## _________________$$ end of playerSize $$__________________##

        ## ------------------** thumbSize kwarg **------------------##

        if kwargs.has_key('thumbsize'):
            self.thumbSize = kwargs['thumbsize'].split('x')
        else:
            if self.widescreen is False:
                self.thumbSize = [120, 90]
            else:
                self.thumbSize = [120, 72]

        ## _________________$$ end of thumbSize $$__________________##

        ## ------------------** json kwarg **-------------------##

        if kwargs.has_key('json'):
            self.json = kwargs['json']
            if self.json is False or self.json == 'False' or self.json == 'false':
                self.json = False

            if self.json is True or self.json == 'True' or self.json == 'true':
                self.json = True
        else:
            self.json = False
        ## _________________$$ end of json $$___________________##

        ## ------------------** html kwarg **-------------------##

        if kwargs.has_key('html'):
            self.html = kwargs['html']
            if self.html is False or self.html == 'False' or self.html == 'false':
                self.html = False

            if self.html is True or self.html == 'True' or self.html == 'true':
                self.html = True
        else:
            self.html = False
        ## _________________$$ end of html $$___________________##

        ## ------------------** jScript kwarg **-------------------##

        if kwargs.has_key('jScript'):
            self.jScript = kwargs['jScript']
            if self.jScript is False or self.jScript == 'False' or self.jScript == 'false':
                self.jScript = False

            if self.jScript is True or self.jScript == 'True' or self.jScript == 'true':
                self.jScript = True
        else:
            self.jScript = False
        ## _________________$$ end of jScript $$___________________##

        ## ------------------** getJS kwarg **-------------------##

        if kwargs.has_key('getJS'):
            self.getJS = kwargs['getJS']
            if self.getJS is False or self.getJS == 'False' or self.getJS == 'false':
                self.getJS = False

            if self.getJS is True or self.getJS == 'True' or self.getJS == 'true':
                self.getJS = True
        else:
            self.getJS = False
        ## _________________$$ end of getJS $$___________________##


        ## ------------------** css kwarg **-------------------##

        if kwargs.has_key('css'):
            self.css = kwargs['css']
            if self.css is False or self.css == 'False' or self.css == 'false':
                self.css = False

            if self.css is True or self.css == 'True' or self.css == 'true':
                self.css = True
        else:
            self.css = False
        ## _________________$$ end of css $$___________________##

        ## ------------------** php kwarg **-------------------##

        if kwargs.has_key('php'):
            self.php = kwargs['php']
            if self.php is False or self.php == 'False' or self.php == 'false':
                self.php = False

            if self.php is True or self.php == 'True' or self.php == 'true':
                self.php = True
        else:
            self.php = False
        ## _________________$$ end of allWeb $$___________________##

        ## ------------------** allWeb kwarg **-------------------##

        if kwargs.has_key('allWeb'):
            self.allWeb = kwargs['allWeb']
            if self.allWeb is False or self.allWeb == 'False' or self.allWeb == 'false':
                self.allWeb = False

            if self.allWeb is True or self.allWeb == 'True' or self.allWeb == 'true':
                self.allWeb = True
        else:
            self.allWeb = False
        ## _________________$$ end of allWeb $$___________________##

        ## _________________________________$$ end of kwargs setup $$____________________________________##

        # give location for first pass encoding based on OS
        if os.name == 'nt':
            devNull = 'Null'
        else:
            devNull = '/dev/null'

        ## ----------------------------------- ** make the videos ** -------------------------------------##
        if self.mkVids:
            self.mkDir()
            # pP(fData)
            for fName in fData:
                ## make all the directories needed for videos, html and thumbnales
                nPath = os.path.join(self.baseDir, 'mkH5videos', 'webVideos', fData[fName]['name'].replace(' ', '_').replace('(', '').replace(')', '').replace("'", ''))
                if not os.path.exists(nPath):
                    os.mkdir(nPath)

                webmPath = os.path.join(nPath, 'webm')
                if not os.path.exists(webmPath):
                    os.mkdir(webmPath)

                mp4Path = os.path.join(nPath, 'mp4')
                if not os.path.exists(mp4Path):
                    os.mkdir(mp4Path)

                imgPath = os.path.join(nPath, 'images')
                if not os.path.exists(imgPath):
                    os.mkdir(imgPath)

                thumbPath = os.path.join(imgPath, 'thumb')
                if not os.path.exists(thumbPath):
                    os.mkdir(thumbPath)

                tOffset = 4
                for x, fSize in enumerate(self.sizs):
                    if fData[fName]['bitRate'] >= fSize['bit_rate']:
                        ## ---->  Commands to execute <---- ##
                        ## The below ws taken and adapted from -> https://github.com/adexin-team/refinerycms-videojs/wiki/Encoding-files-to-.webm-(VP8)-and-.mp4-(h.264)-using-ffmpeg
                        ## create the ffmpeg commands for mp4

                        self.temp = '%s'%(os.path.join(self.baseDir, 'mkH5videos', 'webVideos', fName.replace(' ', '_').replace('(', '').replace(')', '').replace("'", ''), 'mp4', 'temp.mp4'))

                        self.mp4outFile = '%s-%s'%(os.path.join(self.baseDir, 'mkH5videos', 'webVideos', fName.replace(' ', '_').replace('(', '').replace(')', '').replace("'", ''), 'mp4', fName.replace(' ', '_').replace('(', '').replace(')', '')), str(fSize['width']) + 'x' + str(fSize['height']))

                        self.webMoutFile = '%s-%s'%(os.path.join(self.baseDir, 'mkH5videos', 'webVideos', fName.replace(' ', '_').replace('(', '').replace(')', '').replace("'", ''), 'webm', fName.replace(' ', '_').replace('(', '').replace(')', '')), str(fSize['width']) + 'x' + str(fSize['height']))

                        # I removed this because I wanted to see it it was necessary
                        # '-crf', '23',
                        self.mp4Pass1 = ['ffmpeg',
                                         '-i', '%s'%fData[fName]['loc'],
                                         '-vcodec', 'libx264',
                                         '-vprofile', 'high',
                                         '-preset', 'slow',
                                         '-b:v', '%s'%fSize['bit_rate'],
                                         '-maxrate', '%s'%fSize['max_bit'],
                                         '-bufsize', '%s'%fSize['buffer_size'],
                                         '-vf',
                                         'scale=%s:%s'%(fSize['width'], fSize['height']),
                                         '-threads', '%s'%self.cores,
                                         '-pass', '1',
                                         '-an',
                                         '-pix_fmt', 'yuv420p',
                                         '-f', 'mp4',
                                         '%s'%devNull,
                                         '-y']

                        self.mp4Pass2 = ['ffmpeg',
                                         '-i', '%s'%fData[fName]['loc'],
                                         '-vcodec', 'libx264',
                                         '-vprofile', 'high',
                                         '-preset', 'slow',
                                         '-b:v', '%s'%fSize['bit_rate'],
                                         '-maxrate', '%s'%fSize['max_bit'],
                                         '-bufsize', '%s'%fSize['buffer_size'],
                                         '-vf',
                                         'scale=%s:%s'%(fSize['width'], fSize['height']),
                                         '-threads', '%s'%self.cores,
                                         '-pass', '2',
                                         '-strict', '-2',
                                         '-pix_fmt', 'yuv420p',
                                         '-f', 'mp4',
                                         '%s'%self.temp,
                                         '-y']

                        self.fastStart = ['qt-faststart', '%s'%self.temp, '%s.mp4'%self.mp4outFile]

                        self.webmPass1 = ['ffmpeg',
                                          '-i', '%s'%fData[fName]['loc'],
                                          '-codec:v', 'libvpx',
                                          '-quality', 'good',
                                          '-cpu-used', '0',
                                          '-b:v', '%s'%fSize['bit_rate'],
                                          '-qmin', '10',
                                          '-qmax', '42',
                                          '-maxrate', '%s'%fSize['max_bit'],
                                          '-bufsize', '%s'%fSize['buffer_size'],
                                          '-threads', '%s'%self.cores,
                                          '-vf',
                                          'scale=%s:%s'%(fSize['width'], fSize['height']),
                                          '-an',
                                          '-pass', '1',
                                          '-f', 'webm',
                                          '%s'%devNull,
                                          '-y']

                        self.webmPass2 = ['ffmpeg',
                                          '-i', '%s'%fData[fName]['loc'],
                                          '-codec:v', 'libvpx',
                                          '-quality', 'good',
                                          '-cpu-used', '0',
                                          '-b:v', '%s'%fSize['bit_rate'],
                                          '-qmin', '10',
                                          '-qmax', '42',
                                          '-maxrate', '%s'%fSize['max_bit'],
                                          '-bufsize', '%s'%fSize['buffer_size'],
                                          '-threads', '%s'%self.cores,
                                          '-vf',
                                          'scale=%s:%s'%(fSize['width'], fSize['height']),
                                          '-codec:a', 'libvorbis',
                                          '-b:a', '128k',
                                          '-pass', '2',
                                          '-f', 'webm',
                                          '%s.webm'%self.webMoutFile,
                                          '-y']

                        self.thumb = ['ffmpeg',
                                      '-itsoffset', '-%s'%tOffset,
                                      '-i', '%s.mp4'%self.mp4outFile,
                                      '-vcodec', 'png',
                                      '-vframes', '1',
                                      '-an',
                                      '-f', 'rawvideo',
                                      '-s', '%sx%s'%(self.thumbSize[0], self.thumbSize[1]),
                                      '-y',
                                      '%s-%s.png'%(os.path.join(thumbPath, fData[fName]['name'].replace(' ', '_').replace('(', '').replace(')', '')), x)]

                        self.plcPic = ['ffmpeg',
                                       '-itsoffset', '-%s'%tOffset,
                                       '-i', '%s.mp4'%self.mp4outFile,
                                       '-vcodec', 'png',
                                       '-vframes', '1',
                                       '-an',
                                       '-f', 'rawvideo',
                                       '-s', '%sx%s'%(self.playerSize[0], self.playerSize[1]),
                                       '-y',
                                       '%s-%s.png'%(os.path.join(imgPath, fData[fName]['name'].replace(' ', '_').replace('(', '').replace(')', '')), x)]

                        if not fData[fName]['isMov']:
                            self.mp4Pass1.remove('-pix_fmt')
                            self.mp4Pass1.remove('yuv420p')
                            self.mp4Pass2.remove('-pix_fmt')
                            self.mp4Pass2.remove('yuv420p')

                        if ffQuestions is True:
                            self.mp4Pass1.remove('-y')
                            self.mp4Pass2.remove('-y')
                            self.webmPass1.remove('-y')
                            self.webmPass2.remove('-y')
                            self.thumb.remove('-y')
                            self.plcPic.remove('-y')

                        if os.name == 'nt':
                            # self.rmLog = ['del', '%s*log*'%self.baseDir]
                            self.rmTemp = ['del', '%s'%self.temp]
                            self.encPass = [self.mp4Pass1, self.mp4Pass2, self.fastStart, self.plcPic, self.thumb, self.rmTemp, self.webmPass1, self.webmPass2]
                        else:
                            self.rmLog = ['rm', '%s*log*'%self.baseDir]
                            self.rmTemp = ['rm', '%s'%self.temp]
                            self.encPass = [self.mp4Pass1, self.mp4Pass2, self.fastStart, self.plcPic, self.thumb, self.rmTemp, self.webmPass1, self.webmPass2, self.rmLog]

                        for ePass in self.encPass:
                            #pP(ePass)
                            print ePass
                            process = subprocess.Popen(ePass, stdout=subprocess.PIPE)
                            output = process.communicate()[0]
                            print output

                    tOffset = tOffset + 15

        ## __________________________________$$ end make videos $$_____________________________________##

        if self.json:
            self.mkJson()

        if self.html:
            self.mkHTML()

        if self.jScript:
            self.mkJS(False)

        if self.getJS:
            self.jsFile = self.mkJS(True)

        if self.php:
            self.mkPHP()

        if self.css:
            self.mkCSS()

        if self.allWeb:
            self.mkJson()
            self.mkHTML()
            self.mkJS(False)
            self.mkPHP()
            self.mkCSS()


    def sizes(self, setsize=None, widescreen=True):
        '''This function sets the default sizes and bitrates to be used.'''
        if widescreen:
            if setsize is None:
                # 16:9
                setsize = [{'width': 1920, 'height': 1080, 'bit_rate': 2200000, 'max_bit': 2400000, 'buffer_size': 4800000},
                           {'width': 1280, 'height': 720, 'bit_rate': 1800000, 'max_bit': 2000000, 'buffer_size': 4000000},
                           {'width': 720, 'height': 406, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
                           {'width': 480, 'height': 270, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
                           {'width': 320, 'height': 180, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}]
            return setsize
        else:
            if setsize is None:
                # 4:3
                setsize = [{'width': 1440, 'height': 1080, 'bit_rate': 2200000, 'max_bit': 2400000, 'buffer_size': 4800000},
                           {'width': 1024, 'height': 768, 'bit_rate':1800000, 'max_bit': 2000000, 'buffer_size': 4000000},
                           {'width': 640, 'height': 480, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
                           {'width': 400, 'height': 300, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
                           {'width': 272, 'height': 204, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}]
            return setsize

    def mkDir(self):
        '''This function builds the necessary directories'''
        h5Vid = os.path.join(self.baseDir, 'mkH5videos')
        if not os.path.exists(h5Vid):
            os.mkdir(h5Vid)

        if not os.path.exists(os.path.join(h5Vid, 'webVideos')):
            webVideos = os.path.join(h5Vid, 'webVideos')
            os.mkdir(webVideos)

        if not os.path.exists(os.path.join(h5Vid, 'scripts')):
            scrip = os.path.join(h5Vid, 'scripts')
            os.mkdir(scrip)

    def getVidInfo(self, vPath):
        '''This function gets json data from ffprobe'''
        # print vPath
        if os.path.exists(vPath):
            command = ['ffprobe', '-loglevel', 'quiet', '-print_format', 'json', '-show_format', '-show_streams', vPath]
            pipe = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            out, err = pipe.communicate()
            if not err is None:
                print 'err = '+str(err)
            return json.loads(out)

    def imgResize(self, wantW, wantH, origW, origH):
        r = wantH / wantW
        if origW * r > origH:
            r = wantW / origW
        else:
            r = wantH / origH

        origW *= r
        origH *= r
        return origW, origH

    def mkJson(self):
        jsonFile = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
        # print jsonFile
        if os.path.exists(jsonFile):
            jFexists = True
            try:
                jsData = open(jsonFile)
                jData = json.load(jsData)
                jsData.close()
            except ValueError:
                jData = json.loads(jsonFile)
        else:
            jFexists = False

        # print jData['settings']

        oData = OD()
        fnd = comp(ur'(.*-([0-9]{3,4})x[0-9]{3,4}.*)')
        topDir = os.path.join(os.getcwd(), 'mkH5videos', 'webVideos')
        if not jFexists:
            oData['settings'] = OD()
            oData['settings']['topDir'] = os.path.join(os.getcwd(), 'mkH5videos')
            oData['settings']['thumbColor'] = '#33daff'
            oData['settings']['trackColor'] = '#878686'
            oData['settings']['trackColorDone'] = '#184A50'

            oData['settings']['videoOrder'] = 'false'
            oData['settings']['playlist'] = 'false'
            oData['settings']['uSetRez'] = 'SD'
            oData['settings']['rezColorUp'] = '#FD5600'
            oData['settings']['rezColorSelect'] = '#458CCB'
            oData['settings']['rezColorOver'] = '#ffffff'
            oData['settings']['shoCtrlOnStart'] = 'visible'
            # oData['settings']['buttonW'] = noPxNum(player.style.height) * 0.10 + 10 + 'px'
            # oData['settings']['buttonH'] = noPxNum(player.style.height) * 0.10 + 'px'
            oData['settings']['btnColUp'] = '#458CCB'
            oData['settings']['btnColOver'] = '#72ABDD'
            oData['settings']['btnBgTop'] = '#040202'
            oData['settings']['btnBgTopAlpha'] = 1
            oData['settings']['btnBgBottom'] = '#10100A'
            oData['settings']['btnBgBottomAlpha'] = 1
            oData['settings']['nowPlayColor'] = '#91C7F6'
            oData['settings']['volOffset'] = 10
            oData['settings']['hideCtrlsTime'] = 3

        else:
            oData['settings'] = OD()
            oData['settings'] = jData['settings']

        for movieName in sorted(os.listdir(topDir)):
            if os.path.isdir(os.path.join(topDir, movieName)):
                oData[movieName] = OD()
                oData[movieName]['name'] = movieName.replace('_', ' ').replace("'", '')
                if jData.has_key(movieName) and not jData[movieName]['about'] == 'About your video?':
                    oData[movieName]['about'] = jData[movieName]['about']
                else:
                    oData[movieName]['about'] = 'About your video?'
                oData[movieName]['loc'] = os.path.join('webVideos', movieName)

            for dataDir in sorted(os.listdir(os.path.join(topDir, movieName))):
                if os.path.isdir(os.path.join(topDir, movieName, dataDir)):
                    oData[movieName][dataDir] = OD()
                    p = 0
                    for i, data in enumerate(sorted(os.listdir(os.path.join(topDir, movieName, dataDir)))):
                        if os.path.isfile(os.path.join(topDir, movieName, dataDir, data)):
                            if dataDir == 'mp4' or dataDir == 'webm':

                                match = findall(fnd, data)

                                if str(match[0][1]) == '1920' or str(match[0][1]) == '1440':
                                    oData[movieName][dataDir]['HD'] = os.path.join(dataDir, match[0][0])

                                elif str(match[0][1]) == '1280' or str(match[0][1]) == '1024':
                                    oData[movieName][dataDir]['HQ'] = os.path.join(dataDir, match[0][0])

                                elif str(match[0][1]) == '720' or match[0][1] == '640':
                                    oData[movieName][dataDir]['SD'] = os.path.join(dataDir, match[0][0])

                                elif str(match[0][1]) == '480' or str(match[0][1]) == '400':
                                    oData[movieName][dataDir]['LQ'] = os.path.join(dataDir, match[0][0])

                                elif str(match[0][1]) == '320' or str(match[0][1]) == '272':
                                    oData[movieName][dataDir]['M'] = os.path.join(dataDir, match[0][0])

                                # else:
                                #     oData[movieName][dataDir]['UD %s'%i] = data

                            if dataDir == 'images':
                                oData[movieName][dataDir]['p_%s'%p] = OD()
                                loc = os.path.join('webVideos', movieName, dataDir, data)
                                im = Image.open(os.path.join(topDir, movieName, dataDir, data))
                                width, height = im.size
                                oData[movieName][dataDir]['p_%s'%p]['loc'] = loc
                                oData[movieName][dataDir]['p_%s'%p]['width'] = width
                                oData[movieName][dataDir]['p_%s'%p]['height'] = height
                                p += 1

                        else:
                            if os.path.isdir(os.path.join(topDir, movieName, dataDir, data)):
                                oData[movieName][data] = OD()
                                for x, thumb in enumerate(sorted(os.listdir(os.path.join(topDir, movieName, dataDir, data)))):
                                    if os.path.isfile(os.path.join(topDir, movieName, dataDir, data, thumb)):
                                        oData[movieName][data]['t_%s'%x] = OD()
                                        loc = os.path.join('webVideos', movieName, dataDir, data, thumb)
                                        im = Image.open(os.path.join(topDir, movieName, dataDir, data, thumb))
                                        width, height = im.size
                                        oData[movieName][data]['t_%s'%x]['loc'] = loc
                                        oData[movieName][data]['t_%s'%x]['width'] = width
                                        oData[movieName][data]['t_%s'%x]['height'] = height
        oFile = '{\n'
        i = 0
        for key, o in oData.items():
            i += 1
            if i != len(oData):
                oFile += '  "%s": %s,\n'%(key, json.dumps(o, indent=4, sort_keys=True))
            else:
                oFile += '  "%s": %s'%(key, json.dumps(o, indent=4, sort_keys=True))
        oFile += '}'
        print oFile
        jData = R'%s'%oFile

        jsonPath = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
        jFile = open(jsonPath, 'w')
        jFile.write(jData.replace('\\', '/').replace('\\\\', '/'))
        jFile.close()

    def mkHTML(self):
        jsonFile = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
        print jsonFile
        if os.path.exists(jsonFile):
            try:
                jsData = open(jsonFile)
                jData = json.load(jsData)
                jsData.close()
            except ValueError:
                jData = json.loads(jsonFile)

            for key in sorted(jData):
                if not key == 'settings':
                    print key
                    theKey = key
                    break


            htmlPath = os.path.join(self.baseDir, 'mkH5videos', 'movie.html')
            jsonPath = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
            mp4Def = os.path.join('%s', '%s')%(jData[theKey]['loc'], jData[theKey]['mp4']['SD'])
            webmDef = os.path.join('%s', '%s')%(jData[theKey]['loc'], jData[theKey]['webm']['SD'])
            vidThumb = os.path.join('%s')%jData[theKey]['images']['p_0']['loc']

            defSet = '{'
            defSet += "'videoOrder': %s,\n"%jData['settings']['videoOrder']
            defSet += "\t\t\t\t\t       'uSetRez': '%s',\n"%jData['settings']['uSetRez']
            defSet += "\t\t\t\t\t       'rezColorUp': '%s',\n"%jData['settings']['rezColorUp']
            defSet += "\t\t\t\t\t       'rezColorSelect': '%s',\n"%jData['settings']['rezColorSelect']
            defSet += "\t\t\t\t\t       'rezColorOver': '%s',\n"%jData['settings']['rezColorOver']
            defSet += "\t\t\t\t\t       'shoCtrlOnStart': '%s',\n"%jData['settings']['shoCtrlOnStart']
            defSet += "\t\t\t\t\t       'btnColUp': '%s',\n"%jData['settings']['btnColUp']
            defSet += "\t\t\t\t\t       'btnColOver': '%s',\n"%jData['settings']['btnColOver']
            defSet += "\t\t\t\t\t       'btnBgTop': '%s',\n"%jData['settings']['btnBgTop']
            defSet += "\t\t\t\t\t       'btnBgTopAlpha': %d,\n"%jData['settings']['btnBgTopAlpha']
            defSet += "\t\t\t\t\t       'btnBgBottom': '%s',\n"%jData['settings']['btnBgBottom']
            defSet += "\t\t\t\t\t       'btnBgBottomAlpha': %d,\n"%jData['settings']['btnBgBottomAlpha']
            defSet += "\t\t\t\t\t       'nowPlayColor': '%s',\n"%jData['settings']['nowPlayColor']
            defSet += "\t\t\t\t\t       'volOffset': %d,\n"%jData['settings']['volOffset']
            defSet += "\t\t\t\t\t       'hideCtrlsTime': %d"%jData['settings']['hideCtrlsTime']
            defSet += '}'

            htmlFile = '''<!DOCTYPE html>
<html>
    <head>
        <title>Your HTML5 Videos</title>
        <meta charset="utf-8">
        <link type="text/css" rel="stylesheet" href="scripts/mkH5videos.css" ></link>
        <script src="scripts/mkH5videos.js" type="application/javascript" charset="utf-8"></script>
        <script src="scripts/TweenMax.min.js" type="text/javascript" charset="utf-8"></script>
        <script>
            window.addEventListener("load", function(){
                var vid, crtls, player;
                // get the video vars
                vid = document.getElementById('%s');
                ctrls = document.getElementById('vidCtrl');
                player = document.getElementById('player');
                // mkH5video(args, object or nothing); The object is set to the json['settings']
                mkH5video(player, vid, ctrls, '%s', %s);
            });
        </script>
    </head>
    <body>
        <!-- use inline style for positioning. JS is loaded before CSS and needs to do math for controls -->
        <div id="player" style="position: absolute; top: 100px; left: 100px; width:%spx; height:%spx;">
            <!-- set height and width for JS math -->
            <!-- video id dynamically changes when video is loaded. Default is set to first video in json file.-->
            <!-- For the resolution changer function to work the initial video id must be set to the object key of the video -->
            <video id="%s" class="video-js vjs-default-skin"
              preload="auto" width="%spx" height="%spx"
              poster="%s">
              <source src="%s" type='video/mp4'>
              <source src="%s" type='video/webm'>
            </video>
            <div id="vidCtrl"></div>
        </div>
    </body>
</html>'''%(theKey, jsonPath, defSet, self.playerSize[0], self.playerSize[1], theKey, self.playerSize[0], self.playerSize[1], vidThumb, mp4Def, webmDef)

            print htmlFile
            htmlPath = os.path.join(self.baseDir, 'mkH5videos', 'mkH5videos.html')
            hFile = open(htmlPath, 'w')
            hFile.write(htmlFile)
            hFile.close()
        else:
            print 'cannot create HTML file. You must first make the json file.'

    def mkJS(self, getJS):

        jsFile = R'''
'use strict';
if(!window.TweenLte){
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js";
    document.getElementsByTagName("head")[0].appendChild(script);
}

function mkH5video(player, video, configFile, setCtrl, verbose){
    var loc;
    if(player === undefined || player === null || player === ""){
        player = document.getElementById('player');
    }
    if(video === undefined || video === null || video === ""){
        video = document.getElementsByTagName('video')[0];
    }
    if(configFile === undefined || configFile === null || configFile === ""){
        loc = 'scripts/mkH5videos.json';
    }else{
        loc = configFile;
    }
    if(verbose === undefined || verbose === null || verbose !== true){
        verbose = false;
    }

    if(!video.controls){
        var xmlns = 'http://www.w3.org/2000/svg';

        // ** -Start- Build user costomizations -Start- ** //

        // maybe add these again if we want to figure out some crazy button effects.
        // 'btnBgTopOver': '#404249', 'btnBgBottomOver': '#2C365C',
        if(setCtrl === undefined || setCtrl === null || Object.getPrototypeOf(setCtrl) !== Object.prototype){
            setCtrl = {'topDir': '',
                       'videoOrder': false,
                       'playlist': false,
                       'uSetRez': 'SD',
                       'rezColorUp': '#FD5600',
                       'rezColorSelect': '#458CCB',
                       'rezColorOver': '#ffffff',
                       'shoCtrlOnStart': 'visible',
                       'buttonW': parseInt(player.style.height) * 0.10 + 10 + 'px',
                       'buttonH': parseInt(player.style.height) * 0.10 + 'px',
                       'btnColUp': '#458CCB',
                       'btnColOver': '#72ABDD',
                       'btnBgTop': '#040202',
                       'btnBgTopAlpha': 1.0,
                       'btnBgBottom': '#10100A',
                       'btnBgBottomAlpha': 1.0,
                       'nowPlayColor': '#91C7F6',
                       'bufferColor': '#FFFFFF',
                       'volOffset': 10,
                       'hideCtrlsTime': 3,
                       'progressFix': false};
        }else{
            if(setCtrl.topDir === undefined || typeof setCtrl.topDir !== 'string'){
                setCtrl.topDir = '';
                mkLog('Setting-topDir ( * Default * )');
            }
            else{
                mkLog('Setting-topDir (Type expected, String:URL): '+setCtrl.topDir);
            }
            if(setCtrl.videoOrder === undefined || Object.prototype.toString.call(setCtrl.videoOrder) !== '[object Array]'){
                setCtrl.videoOrder = false;
                mkLog('Setting-videoOrder ( * Default * )');
            }else{
                mkLog('Setting-videoOrder (Type expected, Array): '+setCtrl.videoOrder);
            }
            if(setCtrl.playlist === undefined || Object.prototype.toString.call(setCtrl.playlist) !== '[object Array]'){
                setCtrl.playlist = false;
                mkLog('Setting-playlist ( * Default * )');
            }else{
                mkLog('Setting-playlist (Type expected, Array): '+setCtrl.playlist);
            }
            if(setCtrl.uSetRez === undefined || typeof setCtrl.uSetRez !== 'string'){
                setCtrl.uSetRez = 'SD';
                mkLog('Setting-uSetRez ( * Default * ):');
            }else{
                mkLog('Setting-uSetRez (Default video resolution, HD | HQ | SD | LQ | M): '+setCtrl.uSetRez);
            }
            if(setCtrl.rezColorUp === undefined || typeof setCtrl.rezColorUp !== 'string'){
                setCtrl.rezColorUp = '#FD5600';
                mkLog('Setting-rezColorUp ( * Default * )');
            }else{
                mkLog('Setting-rezColorUp (Type expected, Hexadecimal Color String): '+setCtrl.rezColorUp);
            }
            if(setCtrl.rezColorSelect === undefined || typeof setCtrl.rezColorSelect !== 'string'){
                setCtrl.rezColorSelect = '#458CCB';
                mkLog('Setting-rezColorSelect ( * Default * )');
            }else{
                mkLog('Setting-rezColorSelect (Type expected, Hexadecimal Color String): '+setCtrl.rezColorSelect);
            }
            if(setCtrl.rezColorOver === undefined || typeof setCtrl.rezColorOver !== 'string'){
                setCtrl.rezColorOver = '#ffffff';
                mkLog('Setting-rezColorOver ( * Default * )');
            }else{
                mkLog('Setting-rezColorOver (Type expected, Hexadecimal Color String): '+setCtrl.rezColorOver);
            }
            if(setCtrl.shoCtrlOnStart === undefined || typeof setCtrl.shoCtrlOnStart !== 'string'){
                setCtrl.shoCtrlOnStart = 'visible';
                mkLog('Setting-shoCtrlOnStart ( * Default * )');
            }else{
                mkLog('Setting-shoCtrlOnStart (Type expected, hidden | visible): '+setCtrl.shoCtrlOnStart);
            }
            if(setCtrl.buttonH === undefined || typeof setCtrl.buttonH !== 'string'){
                setCtrl.buttonH = parseInt(player.style.height) * 0.10 + 'px';
                mkLog('Setting-buttonH ( * Default * )');
            }else{
                mkLog('Setting-buttonH (Type expected, String-number in pixels): '+setCtrl.buttonH);
            }
            if(setCtrl.buttonW === undefined || typeof setCtrl.buttonW !== 'string'){
                setCtrl.buttonW = parseInt(player.style.height) * 0.10 + 10 + 'px';
                mkLog('Setting-buttonW ( * Default * )');
            }else{
                mkLog('Setting-buttonW (Type expected, String-number in pixels): '+setCtrl.buttonW);
            }
            if(setCtrl.btnColUp === undefined || typeof setCtrl.btnColUp !== 'string'){
                setCtrl.btnColUp = '#458CCB';
                mkLog('Setting-btnColUp ( * Default * )');
            }else{
                mkLog('Setting-btnColUp (Type expected, Hexadecimal Color String): '+setCtrl.btnColUp);
            }
            if(setCtrl.btnColOver === undefined || typeof setCtrl.btnColOver !== 'string'){
                setCtrl.btnColOver = '#72ABDD';
                mkLog('Setting-btnColOver ( * Default * )');
            }else{
                mkLog('Setting-btnColOver (Type expected, Hexadecimal Color String): '+setCtrl.btnColOver);
            }
            if(setCtrl.btnBgTop === undefined || typeof setCtrl.btnBgTop !== 'string'){
                setCtrl.btnBgTop = '#040202';
                mkLog('Setting-btnBgTop ( * Default * )');
            }else{
                mkLog('Setting-btnBgTop (Type expected, Hexadecimal Color String): '+setCtrl.btnBgTop);
            }
            // if(setCtrl.btnBgTopOver === undefined || typeof setCtrl.btnBgTopOver !== 'string'){
            //     setCtrl.btnBgTopOver = '#404249'
            //     mkLog('Setting-btnBgTopOver ( * Default * )');
            // }else{
            //     mkLog('Setting-btnBgTopOver (Type expected, Hexadecimal Color String): '+setCtrl.btnBgTopOver);
            // }
            if(setCtrl.btnBgTopAlpha === undefined ||  setCtrl.btnBgTopAlpha === setCtrl.btnBgTopAlpha && setCtrl.btnBgTopAlpha !== (setCtrl.btnBgTopAlpha|0)){
                setCtrl.btnBgTopAlpha = 1.0;
                mkLog('Setting-btnBgTopAlpha ( * Default * )');
            }else{
                mkLog('Setting-btnBgTopAlpha (Type expected, Intager 0.0-1.0): '+setCtrl.btnBgTopAlpha);
            }
            if(setCtrl.btnBgBottom === undefined || typeof setCtrl.btnBgBottom !== 'string'){
                setCtrl.btnBgBottom = '#10100A';
                mkLog('Setting-btnBgBottom ( * Default * )');
            }else{
                mkLog('Setting-btnBgBottom (Type expected, Hexadecimal Color String): '+setCtrl.btnBgBottom);
            }
            // if(setCtrl.btnBgBottomOver === undefined || typeof setCtrl.btnBgBottomOver !== 'string'){
            //     setCtrl.btnBgBottomOver = '#2C365C';
            //     mkLog('Setting-btnBgBottomOver ( * Default * )');
            // }else{
            //     mkLog('Setting-btnBgBottomOver (Type expected, Hexadecimal Color String): '+setCtrl.btnBgBottomOver);
            // }
            if(setCtrl.btnBgBottomAlpha === undefined || setCtrl.btnBgBottomAlpha === setCtrl.btnBgBottomAlpha && setCtrl.btnBgBottomAlpha !== (
                setCtrl.btnBgBottomAlpha|0)){
                setCtrl.btnBgBottomAlpha = 1.0;
                mkLog('Setting-btnBgBottomAlpha ( * Default * )');
            }else{
                mkLog('Setting-btnBgBottomAlpha (Type expected, Intager 0.0-1.0): '+setCtrl.btnBgBottomAlpha);
            }
            if(setCtrl.nowPlayColor === undefined || typeof setCtrl.nowPlayColor !== 'string'){
                // Now playing thumbnail text`
                setCtrl.nowPlayColor = '#458CCB';
                mkLog('Setting-nowPlayColor ( * Default * )');
            }else{
                mkLog('Setting-nowPlayColor (Type expected, Hexadecimal Color String): '+setCtrl.nowPlayColor);
            }
            if(setCtrl.bufferColor === undefined || typeof setCtrl.bufferColor !== 'string' && Object.prototype.toString.call(setCtrl.bufferColor) !== '[object Array]'){
                setCtrl.bufferColor = '#FFFFFF';
                mkLog('Setting-bufferColor ( * Default * )');
            }else{
                mkLog('Setting-bufferColor (Type expected, Hexadecimal Color String | Array | rainbow): '+setCtrl.bufferColor);
            }
            if(setCtrl.volOffset === undefined || typeof setCtrl.volOffset !== 'number'){
                setCtrl.volOffset = 0;
                mkLog('Setting-volOffset ( * Default * )');
            }else{
                mkLog('Setting-volOffset (Type expected, Number): '+setCtrl.volOffset);
            }
            if(setCtrl.hideCtrlsTime === undefined || typeof setCtrl.hideCtrlsTime !== 'number'){
                setCtrl.hideCtrlsTime = 3;
                mkLog('Setting-hideCtrlsTime ( * Default * )');
            }else{
                mkLog('Setting-hideCtrlsTime (Type expected, Number): '+setCtrl.hideCtrlsTime);
            }
            if(setCtrl.progressFix === undefined && Object.prototype.toString.call(setCtrl.progressFix) !== '[object Array]'){
                setCtrl.progressFix = false;
                mkLog('Setting-progressFix ( * Default * )');
            }else{
                mkLog('Setting-progressFix (Type expected, Array): '+setCtrl.progressFix);
            }
        }
        // ** -END- Build user costomizations -END- ** //

        var ctrl = document.createElement('div');
        player.appendChild(ctrl);
        ctrl.id = 'vidCtrl';
        ctrl.style.visibility = setCtrl.shoCtrlOnStart;

        var btnH = setCtrl.buttonH;
        var btnW = setCtrl.buttonW;
        
        // Video Player size variables
        var playerW = player.style.width;
        var playerH = player.style.height;
        var playerT = player.style.top;
        var playerL = player.style.left;

        video.style.height = playerH;
        video.style.width = playerW;

        var mouseIdel, nowPlay;  
        var ctrlHidden = false;
        var padding = 5;
        var isPlaying = false;
        var btnS = [];

        var wW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        // this (wH) may be usefull in the future? As of right now it does nothing!
        // var wH = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        var userA = getUserAgent();

        /*/ create buttons and SVG's /*/

        var play = document.createElement('div');
        play.id = 'playBtn';
        play.style.width = btnW;
        play.style.height = btnH;
        play.style.position = 'absolute';
        play.style.visibility = 'visible';
        play.style.zIndex = '12';
        play.style.padding = '5px';

        var play_bg = document.createElementNS(xmlns, 'svg');
        play_bg.setAttribute('baseProfile', 'tiny');
        play_bg.setAttribute('xlmns', xmlns);
        play_bg.setAttribute('viewBox', '0 0 20 15');
        play_bg.setAttribute('preserveAspectRatio', 'none');
        play_bg.setAttribute('height', parseInt(btnH));
        play_bg.setAttribute('width', parseInt(btnW));
        play_bg.setAttribute('x', '0');
        play_bg.setAttribute('y', '0');
        play_bg.style.display = 'block';

        var playG = document.createElementNS(xmlns, 'g');
        playG.setAttribute('fill', setCtrl.btnBgBottom);
        play_bg.appendChild(playG);

        var bottomFillMain = document.createElementNS(xmlns, 'path');
        bottomFillMain.setAttribute('d', 'M4.283 14.637c-2.14 0-3.88-1.294-3.88-2.883V3.256c0-1.59 1.74-2.883 3.88-2.883h11.433c2.14 0 3.88 1.293 3.88 2.883v8.498c0 1.59-1.74 2.883-3.88 2.883H4.283z');
        playG.appendChild(bottomFillMain);

        var bottomFill = document.createElementNS(xmlns, 'path');
        bottomFill.setAttribute('d', 'M4.283 14.637c-2.14 0-3.88-1.294-3.88-2.883V3.256c0-1.59 1.74-2.883 3.88-2.883h11.433c2.14 0 3.88 1.293 3.88 2.883v8.498c0 1.59-1.74 2.883-3.88 2.883H4.283z');
        playG.appendChild(bottomFill);

        var outLine = document.createElementNS(xmlns, 'path');
        outLine.setAttribute('d', 'M15.716.524c2.026 0 3.676 1.226 3.676 2.73v8.5c0 1.505-1.65 2.73-3.676 2.73H4.283c-2.026 0-3.675-1.226-3.675-2.73v-8.5c0-1.505 1.648-2.73 3.675-2.73h11.433m0-.304H4.283C2.037.22.2 1.587.2 3.257v8.498c0 1.67 1.837 3.034 4.083 3.034h11.433c2.246 0 4.084-1.37 4.084-3.04v-8.5c0-1.67-1.838-3.035-4.084-3.035z');
        playG.appendChild(outLine);

        var linGrad = document.createElementNS(xmlns, 'linearGradient');
        linGrad.setAttribute('id', 'a');
        linGrad.setAttribute('gradientUnits', 'userSpaceOnUse');
        linGrad.setAttribute('x1', '9.999');
        linGrad.setAttribute('y1', '4.301');
        linGrad.setAttribute('x2', '9.999');
        linGrad.setAttribute('y2', '11.851');

        var stopTop = document.createElementNS(xmlns, "stop");
        stopTop.setAttribute('offset', '0');
        stopTop.setAttribute('stop-color', '#fff');
        linGrad.appendChild(stopTop);

        var stopBottom = document.createElementNS(xmlns, "stop");
        stopBottom.setAttribute('offset', '1');
        linGrad.appendChild(stopBottom);

        play_bg.appendChild(linGrad);

        var highlightLine = document.createElementNS(xmlns, 'path');
        highlightLine.setAttribute('d', 'M15.716.828H4.282c-1.8 0-3.265 1.09-3.265 2.428v8.498c0 1.34 1.464 2.427 3.265 2.427h11.434c1.8 0 3.267-1.08 3.267-2.42v-8.5c0-1.34-1.466-2.426-3.267-2.426zm2.858 10.926c0 1.17-1.283 2.124-2.858 2.124H4.282c-1.575 0-2.857-.953-2.857-2.124V3.256c0-1.17 1.282-2.125 2.857-2.125h11.434c1.575 0 2.858.96 2.858 2.13v8.5z');
        highlightLine.setAttribute('fill', 'url(#a)');
        play_bg.appendChild(highlightLine);

        var topFill = document.createElementNS(xmlns, 'path');
        topFill.setAttribute('d', 'M15.716.828H4.283c-1.8 0-3.267 1.09-3.267 2.428V7.65c2.72.522 5.766.815 8.984.815s6.263-.293 8.983-.814V3.26c0-1.34-1.465-2.428-3.267-2.428z');
        topFill.setAttribute('fill', setCtrl.btnBgTop);
        play_bg.appendChild(topFill);

        var playBtn = document.createElementNS(xmlns, 'svg');
        playBtn.setAttribute('xlmns', xmlns);
        playBtn.setAttribute('viewBox', '0 0 20 15');
        playBtn.setAttribute('preserveAspectRatio', 'none');
        playBtn.setAttribute('height', parseInt(btnH));
        playBtn.setAttribute('width', parseInt(btnW));
        playBtn.setAttribute('x', '0px');
        playBtn.setAttribute('y', '0px');
        playBtn.style.position = 'absolute';

        var play_btn = document.createElementNS(xmlns, 'path');
        play_btn.setAttribute('d', 'M6.212 3.974l7.913 3.834-7.913 3.834');
        play_btn.setAttribute('fill', setCtrl.btnColUp);
        play_btn.setAttribute('id', 'play_btn');

        var playTop = document.createElement('div');
        playTop.id = 'playTop';
        playTop.style.height = btnH;
        playTop.style.width = btnW;
        playTop.style.position = 'absolute';
        playTop.style.zIndex = '15';
        playTop.style.top = '0px';
        playTop.style.left = '0px';
        playTop.style.padding = '5px';
        btnS.push(play);
        
        play.appendChild(playTop);
        playBtn.appendChild(play_btn);
        play.appendChild(playBtn);
        play.appendChild(play_bg);

        // create slider thumbnail svg

        // var slideThumb = play_bg.cloneNode(true);

        // console.log(slideThumb); 

        // var slideThumb = document.createElementNS(xmlns, 'svg');
        // slideThumb.setAttribute('baseProfile', 'tiny');
        // slideThumb.setAttribute('xlmns', xmlns);
        // slideThumb.setAttribute('viewBox', '0 0 20 15');
        // slideThumb.setAttribute('preserveAspectRatio', 'none');
        // slideThumb.setAttribute('height', parseInt(btnH));
        // slideThumb.setAttribute('width', parseInt(btnW));
        // slideThumb.setAttribute('x', '0');
        // slideThumb.setAttribute('y', '0');
        // slideThumb.style.display = 'block';

        // var slideThumb_Defs = document.createElementNS(xmlns, 'defs');
        // slideThumb.setAttribute('id', 'defs4');

        // var filter1 = document.createElementNS(xmlns, 'filter');
        // filter1.setAttribute('style', 'color-interpolation-filters:sRGB');
        // filter1.setAttribute('id', 'filter4934');
        // filter1.setAttribute('x', '-0.31627015');
        // filter1.setAttribute('width', '1.6325403');
        // filter1.setAttribute('y', '-0.48292748');
        // filter1.setAttribute('height', '1.83904');

        // var gBlur = document.createElementNS(xmlns, 'feGaussianBlur');
        // gBlur.setAttribute('stdDeviation', '1.8931408');
        // gBlur.setAttribute('id', 'feGaussianBlur4936');
        // gBlur.setAttribute('result', 'blurOut');

        // filter1.appendChild(gBlur);

        // var filter2 = document.createElementNS(xmlns, 'filter');
        // filter2.setAttribute('style', 'color-interpolation-filters:sRGB');
        // filter2.setAttribute('id', 'filter4521');

        // var fScreen = document.createElementNS(xmlns, 'feBlend');
        // fScreen.setAttribute('id', 'rnd_blend');
        // fScreen.setAttribute('in', 'blurOut');
        // fScreen.setAttribute('in2', 'BackgroundImage');
        // fScreen.setAttribute('mode', 'screen');

        // filter2.appendChild(fScreen);

        // slideThumb_Defs.appendChild(filter1);
        // slideThumb_Defs.appendChild(filter2);

        // var slideThumb_G = document.createElementNS(xmlns, 'g');
        // slideThumb_G.setAttribute('style', 'opacity:1;filter:url(#filter4521)');
        // slideThumb_G.setAttribute('transform', 'translate(0,-1037.3622)');
        // slideThumb_G.setAttribute('id', 'layer1');

        // var slideThumb_P = document.createElementNS(xmlns, 'path');
        // slideThumb_P.setAttribute('id', 'path4361');
        // slideThumb_P.setAttribute('d', 'm 17.94261,1044.9976 a 7.8700361,5.4151626 0 0 1 -7.870036,5.4151 7.8700361,5.4151626 0 0 1 -7.8700365,-5.4151 7.8700361,5.4151626 0 0 1 7.8700365,-5.4152 7.8700361,5.4151626 0 0 1 7.870036,5.4152 z');
        // slideThumb_P.setAttribute('style', 'fill:#FFFFFF;fill-opacity:1;filter:url(#filter4934)');

        // slideThumb_G.appendChild(slideThumb_P);

        // slideThumb.appendChild(slideThumb_Defs);
        // slideThumb.appendChild(slideThumb_G);

        // // slideThumb_bg.appendChild(slideThumb);
        // player.appendChild(slideThumb);

        // slideThumb.style.position = 'absolute';
        // slideThumb.style.top = '-10px';
        // slideThumb.style.left = '-60px';
        // slideThumb.style.width = 100 +'px';
        // slideThumb.style.height =  65+'px';
        // slideThumb.id = 'vThumb';

        // elem.style.setProperty('--blend-mode', 'screen');



        // sets the background alpha on the buttons top and bottom
        chBtnBgAlpha(setCtrl.btnBgTopAlpha, setCtrl.btnBgBottomAlpha);
        
        var pause = document.createElement('div');
        pause.id = 'pauseBtn';
        // pause.src = 'bgImgs/pause.svg';
        pause.style.width = btnW;
        pause.style.height = btnH;
        pause.style.position = 'absolute';
        pause.style.visibility = 'hidden';
        pause.style.zIndex = '10';
        pause.style.padding = '5px';

        var pause_bg = play_bg.cloneNode(true);

        var pauseBtn = document.createElementNS(xmlns, 'svg');
        pauseBtn.setAttribute('xlmns', xmlns);
        pauseBtn.setAttribute('viewBox', '0 0 20 15');
        pauseBtn.setAttribute('preserveAspectRatio', 'none');
        pauseBtn.setAttribute('height', parseInt(btnH));
        pauseBtn.setAttribute('width', parseInt(btnW));
        pauseBtn.setAttribute('x', '0px');
        pauseBtn.setAttribute('y', '0px');
        pauseBtn.style.position = 'absolute';

        var pause_btn = document.createElementNS(xmlns, 'path');
        pause_btn.setAttribute('d', 'M6.212 4.312h2.56v6.993h-2.56zm5.12 0h2.56v6.993h-2.56z');
        pause_btn.setAttribute('fill', setCtrl.btnColUp);
        pause_btn.setAttribute('id', 'pause_btn');

        var pauseTop = document.createElement('div');
        pauseTop.id = 'pauseTop';
        pauseTop.style.height = btnH;
        pauseTop.style.width = btnW;
        pauseTop.style.position = 'absolute';
        pauseTop.style.zIndex = '15';
        pauseTop.style.top = '0px';
        pauseTop.style.left = '0px';
        pauseTop.style.padding = '5px';

        btnS.push(pause);

        pause.appendChild(pauseTop);
        pauseBtn.appendChild(pause_btn);
        pause.appendChild(pauseBtn);
        pause.appendChild(pause_bg);

        var seek = document.createElement('input');
        seek.id = 'seeker';
        seek.className = 'seekerC';
        seek.type = 'range';
        seek.min = 0;
        seek.max = 100;
        seek.value = 1;
        seek.step = 0.01;
        seek.style.position = 'absolute';
        seek.style.zIndex = 12;

        var time = document.createElement('div');
        time.style.position = 'absolute';
        time.style.textAlign = 'right';

        var timeTxt = document.createElement('span');
        timeTxt.id = 'timeTxt';
        timeTxt.className = 'timeC';
        timeTxt.innerHTML = '00:00 | ';

        var totalTxt = document.createElement('span');
        totalTxt.id = 'totalTxt';
        totalTxt.className = 'timeC';
        totalTxt.innerHTML = '00:00';

        var muteVid = document.createElement('div');
        muteVid.id = 'muteBtn';
        // muteVid.src = 'bgImgs/mute.svg';
        muteVid.style.width = btnW;
        muteVid.style.height = btnH;
        muteVid.style.position = 'absolute';
        muteVid.style.zIndex = '10';

        var mute_bg = play_bg.cloneNode(true);

        var muteBtn = document.createElementNS(xmlns, 'svg');
        muteBtn.setAttribute('xlmns', xmlns);
        muteBtn.setAttribute('viewBox', '0 0 20 15');
        muteBtn.setAttribute('preserveAspectRatio', 'none');
        muteBtn.setAttribute('height', parseInt(btnH));
        muteBtn.setAttribute('width', parseInt(btnW));
        muteBtn.setAttribute('x', '0px');
        muteBtn.setAttribute('y', '0px');
        muteBtn.style.position = 'absolute';

        var mute_btn = document.createElementNS(xmlns, 'path');
        mute_btn.setAttribute('d', 'M5.042 6.416v2.662h2.28l.038.023 2.81-2.25V4.634L7.324 6.416H5.042zm8.675-.48l-.913.73c.177.32.28.688.28 1.08 0 .72-.344 1.37-.857 1.76l.646.836c.873-.554 1.45-1.51 1.45-2.596 0-.676-.226-1.302-.606-1.81zM10.17 10.86V8.78L8.71 9.946l1.457.913zm4.418-6.886l.37.297-9.193 7.374-.37-.297');
        mute_btn.setAttribute('fill', setCtrl.btnColUp);
        mute_btn.setAttribute('id', 'mute_btn');
        btnS.push(muteVid);

        var muteTop = document.createElement('div');
        muteTop.id = 'muteTop';
        muteTop.style.height = btnH;
        muteTop.style.width = btnW;
        muteTop.style.position = 'absolute';
        muteTop.style.zIndex = '15';
        
        muteVid.appendChild(muteTop);
        muteBtn.appendChild(mute_btn);
        muteVid.appendChild(muteBtn);
        muteVid.appendChild(mute_bg);

        var unMuteVid = document.createElement('div');
        unMuteVid.id = 'unMuteBtn';
        //unMuteVid.src = 'bgImgs/unMute.svg';
        unMuteVid.style.width = btnW;
        unMuteVid.style.height = btnH;
        unMuteVid.style.position = 'absolute';
        unMuteVid.style.zIndex = '12';

        var unMute_bg = play_bg.cloneNode(true);

        var unMuteBtn = document.createElementNS(xmlns, 'svg');
        unMuteBtn.setAttribute('xlmns', xmlns);
        unMuteBtn.setAttribute('viewBox', '0 0 20 15');
        unMuteBtn.setAttribute('preserveAspectRatio', 'none');
        unMuteBtn.setAttribute('height', parseInt(btnH));
        unMuteBtn.setAttribute('width', parseInt(btnW));
        unMuteBtn.setAttribute('x', '0px');
        unMuteBtn.setAttribute('y', '0px');
        unMuteBtn.style.position = 'absolute';

        var unMute1_btn = document.createElementNS(xmlns, 'path');
        unMute1_btn.setAttribute('d', 'M5.042 6.613V9.01H7.05l2.502 1.604V5.01L7.05 6.612m5.524-2.637l-.588.76c1.07.38 1.883 1.634 1.883 3.077 0 1.43-.8 2.676-1.858 3.066l.574.764c1.385-.58 2.37-2.075 2.37-3.83 0-1.762-.993-3.262-2.384-3.837z');
        unMute1_btn.setAttribute('fill', setCtrl.btnColUp);
        unMute1_btn.setAttribute('id', 'unMute1_btn');
        //btnS.push(unMute1);

        var unMute2_btn = document.createElementNS(xmlns, 'path');
        unMute2_btn.setAttribute('d', 'M11.43 5.453l-.576.744c.474.347.794.947.794 1.614 0 .65-.3 1.235-.753 1.587l.568.753c.768-.5 1.275-1.36 1.275-2.338 0-.992-.52-1.863-1.308-2.357z');
        unMute2_btn.setAttribute('fill', setCtrl.btnColUp);
        unMute2_btn.setAttribute('id', 'unMute2_btn');
        btnS.push(unMuteVid);

        var unMuteTop = document.createElement('div');
        unMuteTop.id = 'unMuteTop';
        unMuteTop.style.height = btnH;
        unMuteTop.style.width = btnW;
        unMuteTop.style.position = 'absolute';
        unMuteTop.style.zIndex = '15';
        
        unMuteVid.appendChild(unMuteTop);
        unMuteBtn.appendChild(unMute1_btn);
        unMuteBtn.appendChild(unMute2_btn);
        unMuteVid.appendChild(unMuteBtn);
        unMuteVid.appendChild(unMute_bg);

        var vol = document.createElement('input');
        vol.id = 'volumeSlider';
        vol.className = 'sliderC';
        vol.type = 'range';
        vol.min = '0';
        vol.max = '100';
        vol.value = '75';
        vol.step = '1';
        vol.style.position = 'absolute';
        vol.style.transform = 'rotate(270deg)';
        vol.style.width = (playerH * 0.30) - btnH + 'px';

        var rSize = document.createElement('div');
        rSize.id = 'rezBtn';
        //rSize.src = 'bgImgs/rez.svg';
        rSize.style.width = btnW;
        rSize.style.height = btnH;
        rSize.style.position = 'absolute';

        var rSize_bg = play_bg.cloneNode(true);

        var rSizeBtn = document.createElementNS(xmlns, 'svg');
        rSizeBtn.setAttribute('xlmns', xmlns);
        rSizeBtn.setAttribute('viewBox', '0 0 20 15');
        rSizeBtn.setAttribute('preserveAspectRatio', 'none');
        rSizeBtn.setAttribute('height', parseInt(btnH));
        rSizeBtn.setAttribute('width', parseInt(btnW));
        rSizeBtn.setAttribute('x', '0px');
        rSizeBtn.setAttribute('y', '0px');
        rSizeBtn.style.position = 'absolute';

        var rSize_btn = document.createElementNS(xmlns, 'path');
        rSize_btn.setAttribute('d', 'M13.323 9.113c-.03 0-.06.003-.09.004l-3.09-2.39c-.216-.166-.53-.21-.804-.14l.48-1.093c.08-.18 0-.445-.18-.58l-1.01-.78c-.177-.135-.52-.197-.75-.136l-.608.16c-.1.026-.16.07-.178.124-.018.053.013.114.087.172l.65.5c.23.18.23.47 0 .647l-.694.54c-.11.09-.26.14-.417.14-.16 0-.31-.045-.42-.132l-.65-.5c-.075-.06-.153-.082-.22-.07-.07.013-.128.06-.16.138l-.21.47c-.08.18 0 .445.177.58l1.01.78c.175.135.518.197.75.136l1.42-.38c-.092.21-.034.455.183.62l3.09 2.39c0 .024-.003.047-.003.07 0 .698.73 1.265 1.634 1.265s1.637-.566 1.637-1.264c0-.697-.73-1.264-1.634-1.264zm0 1.927c-.473 0-.856-.297-.856-.662 0-.178.088-.344.25-.47.162-.124.377-.193.606-.193.472 0 .856.298.856.663s-.39.662-.86.662z');
        rSize_btn.setAttribute('fill', setCtrl.btnColUp);
        rSize_btn.setAttribute('id', 'rez_btn');
        btnS.push(rSize);
        
        var rezTop = document.createElement('div');
        rezTop.id = 'rezTop';
        rezTop.style.height = btnH;
        rezTop.style.width = btnW;
        rezTop.style.position = 'absolute';
        rezTop.style.zIndex = '15';

        rSize.appendChild(rezTop);
        rSizeBtn.appendChild(rSize_btn);
        rSize.appendChild(rSizeBtn);
        rSize.appendChild(rSize_bg);

        var fsb = document.createElement('div');
        fsb.id = 'fScreenBtn';
        //fsb.src = 'bgImgs/fScreen.svg';
        fsb.style.width = btnW;
        fsb.style.height = btnH;
        fsb.style.position = 'absolute';

        var fsb_bg = play_bg.cloneNode(true);

        var fsbBtn = document.createElementNS(xmlns, 'svg');
        fsbBtn.setAttribute('xlmns', xmlns);
        fsbBtn.setAttribute('viewBox', '0 0 20 15');
        fsbBtn.setAttribute('preserveAspectRatio', 'none');
        fsbBtn.setAttribute('height', parseInt(btnH));
        fsbBtn.setAttribute('width', parseInt(btnW));
        fsbBtn.setAttribute('x', '0px');
        fsbBtn.setAttribute('y', '0px');
        fsbBtn.style.position = 'absolute';

        var fsb_btn = document.createElementNS(xmlns, 'path');
        fsb_btn.setAttribute('d', 'M14.327 11.612h.63L14.95 4h-.63v.56h-.908v-.586H6.608v.004h-.02v.582h-.913v-.554h-.633l.006 7.61h.63v-.542h.915v.568h6.817v-.568h.914l.003.538zM7.54 10.576L7.538 5.04h4.923l.008 5.536H7.54zm-1.863-2.52h.915v1.03H5.68v-1.03zm.914-.48h-.91v-1.03h.915v1.03zm6.82-1.03h.916v1.03h-.913l-.002-1.03zm0 1.51h.917v1.03h-.914v-1.03zm.91-3.017l.005 1.03h-.915V5.04h.912zm-7.73 0v1.03h-.91V5.04h.913zm-.91 5.55V9.56h.916v1.03H5.68zm7.735 0v-.018l-.002-1.012h.916v1.03h-.914z');
        fsb_btn.setAttribute('fill', setCtrl.btnColUp);
        fsb_btn.setAttribute('id', 'fScreen_btn');
        btnS.push(fsb);

        var fScreenTop = document.createElement('div');
        fScreenTop.id = 'fScreenTop';
        fScreenTop.style.height = btnH;
        fScreenTop.style.width = btnW;
        fScreenTop.style.position = 'absolute';
        fScreenTop.style.zIndex = '15';

        fsb.appendChild(fScreenTop);
        fsbBtn.appendChild(fsb_btn);
        fsb.appendChild(fsbBtn);
        fsb.appendChild(fsb_bg);

        // holder divs for the controls
        var ctrls = document.createElement('div');
        ctrls.id = 'ctrls';
        ctrls.style.position = 'absolute';
        ctrls.style.zIndex = '12';
        ctrls.style.padding = padding + 'px';
        ctrls.style.display = 'inline-block';

        ctrls.appendChild(play);
        ctrls.appendChild(pause);
        ctrls.appendChild(seek);
        time.appendChild(timeTxt);
        time.appendChild(totalTxt);
        ctrls.appendChild(time);

        var sideCtrl = document.createElement('div');
        sideCtrl.id = 'sideCtrl';
        sideCtrl.style.position = 'absolute';
        sideCtrl.style.display = 'block';
        sideCtrl.style.zIndex = '12';
        sideCtrl.style.padding = '5px 0px 5px 5px';
        sideCtrl.style.width = parseInt(btnW) * 3 + 'px';
        sideCtrl.style.height = btnH;
        sideCtrl.style.margin = '5px';

        var volCtrl = document.createElement('div');
        volCtrl.id = 'volCtrl';
        volCtrl.style.position = 'absolute';
        volCtrl.style.overflow = 'hidden';
        volCtrl.style.zIndex = '12';
        volCtrl.style.display = 'block';
        volCtrl.style.height = btnH;
        volCtrl.style.width = btnW;

        var sizeCtrl = document.createElement('div');
        sizeCtrl.id = 'sizeCtrl';
        sizeCtrl.style.position = 'absolute';
        sizeCtrl.style.zIndex = '12';
        sizeCtrl.style.display = 'block';
        sizeCtrl.style.height = btnH;
        sizeCtrl.style.width = btnW;
        
        volCtrl.appendChild(muteVid);
        volCtrl.appendChild(unMuteVid);
        volCtrl.appendChild(vol);
        sideCtrl.appendChild(volCtrl);
        sizeCtrl.appendChild(rSize);
        sideCtrl.appendChild(sizeCtrl);
        sideCtrl.appendChild(fsb);
        ctrl.appendChild(ctrls);
        ctrl.appendChild(sideCtrl);

        var vidCtrlBG = document.createElement('div');
        vidCtrlBG.id = 'vidCtrlBG';
        vidCtrlBG.style.position = 'absolute';
        vidCtrlBG.style.width = '100%';
        vidCtrlBG.style.zIndex = '10';
        ctrl.appendChild(vidCtrlBG);

        var buffedOut = document.createElement('div');
        buffedOut.style.overflow = 'hidden';

        ctrls.appendChild(buffedOut);

        let seekThumbs = document.body.querySelector('#seeker');
        // seekThumbs = seekThumbs.querySelectorAll('::-moz-range-thumb');

        // console.log(seekThumbs);

        // position elements
        elmPosNorm();

        // create event listeners
        play.addEventListener('click', playPause);
        pause.addEventListener('click', playPause);
        muteVid.addEventListener('click', muteVideo);
        unMuteVid.addEventListener('click', muteVideo);
        fsb.addEventListener('click', fullScreen);
        volCtrl.addEventListener('mouseover', shoVol);
        volCtrl.addEventListener('mouseout', hideVol);
        seek.addEventListener('change', videoSeeker);
        vol.addEventListener('change', volumeSlider);
        video.addEventListener('timeupdate', timeTrack);
        video.addEventListener('mousemove', function(){
            shoVidCtrl();
            clearTimeout(mouseIdel);
            mouseIdel = setTimeout(hideVidCtrl, setCtrl.hideCtrlsTime * 1000);
        });

        mkLog('video.preload = '+video.preload);
        if(video.preload == 'auto'){
            if(setCtrl.progressFix !== false && setCtrl.progressFix.indexOf(userA) >= 0){
                var checkBuff;
                var killProgress;
                var compareBuff = 0;
                var killTimeout = false;
                clearInterval(checkBuff);
                checkBuff = setInterval(getProgress, 1 * 1000);
                seek.addEventListener('click', function(){
                    clearInterval(checkBuff);
                    checkBuff = setInterval(getProgress, 1 * 1000);
                });
            }else{
                video.addEventListener('progress', getProgress);
            }
        }
        
        video.addEventListener('mouseout', function(){
            video.removeEventListener('mousemove', shoVidCtrl);
            hideVidCtrl();
        });
        ctrl.addEventListener('mouseover', function(){
            shoVidCtrl();
            clearTimeout(mouseIdel);
        });

        ctrl.addEventListener('mouseout', hideVidCtrl);
        window.addEventListener('keydown', spacePP);

        // create the button mouseover:out listeners and events
        for(let btn in btnS){
            let btnDiv = btnS[btn];
            btnDiv.addEventListener('mouseover', btnOver);
            btnDiv.addEventListener('mouseout', btnOut);
        }
    }

    // AJAX Request for getting/setting video playlist and sizes
    var req = new XMLHttpRequest();
    var vars;
    if(loc.includes('.json')){
        req.open('GET', loc, true);
    }
    else{
        if(!setCtrl.playlist){
            vars = 'functToGet=mkH5Videos';
        }else{
            vars = 'functToGet=vidSort&playlist='+setCtrl.playlist.join(',');
        }
        req.open('POST', loc, true);
    }
    // req.open('POST', loc, true);
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function(){
        if(req.readyState == 4 && req.status == 200){
            var ret = req.responseText;
            var objct = JSON.parse(ret);
            var tLoc = getTopDir();

            // create the thumbs
            if(Object.keys(objct).length > 2){

                var pList = document.createElement('div');
                pList.id = 'playList';
                pList.style.position = 'absolute';

                var pScroll = document.createElement('div');
                pScroll.id = 'playListScroll';
                pScroll.style.overflow = 'auto';
                pScroll.style.position = 'absolute';

                var pListHolder = document.createElement('div');
                pListHolder.id = 'pListHolder';
                pListHolder.style.position = 'absolute';

                pScroll.appendChild(pList);
                pListHolder.appendChild(pScroll);
                player.appendChild(pListHolder);

                var pListW = 0;
                var obPos = 0;
                var v = 0;
                var len = Object.keys(objct).length;
                var vidObj, ob, nPos, order, objc;
                var addedArr = [];
                for(let i = 1; i <= len; i++){
                    ob = Object.keys(objct)[i];
                    if(ob != 'settings'){
                        if(!setCtrl.videoOrder){
                            vidObj = objct[ob];
                        }else{
                            if(setCtrl.videoOrder[v] !== undefined && (setCtrl.videoOrder[v].replace(' ', '_')) in objct && addedArr.indexOf(setCtrl.videoOrder[v].replace(' ', '_')) === -1){
                                vidObj = objct[setCtrl.videoOrder[v].replace(' ', '_')];
                                ob = setCtrl.videoOrder[v].replace(' ', '_');
                                addedArr.push(setCtrl.videoOrder[v].replace(' ', '_'));
                                i--;
                            }
                            else{
                                if(objct[ob] === undefined){
                                    break;
                                }
                                if(addedArr.indexOf(ob) === -1){
                                    vidObj = objct[ob];
                                    addedArr.push(ob);
                                }else{
                                    vidObj = false;
                                }
                            }
                        }
                        if(vidObj !== false && vidObj !== undefined){
                            if(vidObj.thumb.t_0.width > pListW){ 
                                pListW = vidObj.thumb.t_0.width;
                            }
                            var pStuff = document.createElement('div');
                            pStuff.id = ob+'_holder';
                            pStuff.className = 'playListVideo_thumb';
                            pStuff.style.position = 'absolute';
                            pStuff.style.display = 'block';
                            pStuff.style.overflow = 'hidden';
                            pStuff.style.width = vidObj.thumb.t_0.width +'px';
                            pStuff.style.height = vidObj.thumb.t_0.height +'px';
                            pStuff.style.top = obPos +'px';
                            pList.appendChild(pStuff); 
                             
                            var pThumb = document.createElement('img');
                            pThumb.id = ob+'_pic';
                            if(tLoc === ''){
                                pThumb.src = vidObj.thumb.t_0.loc;
                            }
                            else{
                                let pLoc = tLoc+'/';
                                pThumb.src = pLoc+vidObj.thumb.t_0.loc;
                            }
                            pThumb.style.width = pStuff.style.width;
                            pThumb.style.height = pStuff.style.height;
                            pThumb.style.zIndex = '13';

                            var plName = document.createElement('p');
                            plName.id = ob+'_title';
                            plName.className = 'thumbTitle';
                            plName.style.position = 'absolute';
                            plName.style.bottom = '0px';
                            plName.innerHTML = vidObj.name;
                            plName.style.zIndex = '15';
                            plName.style.overflow = 'visible';

                            var nameHold = document.createElement('div');
                            nameHold.id = ob+'_nameHold';
                            nameHold.className = 'thumbTitleHolder';
                            nameHold.style.position = 'absolute';
                            nameHold.style.bottom = '0px';
                            nameHold.style.left = '0px';
                            nameHold.style.width = pStuff.style.width;
                            nameHold.style.height = vidObj.thumb.t_0.height * .36 + 'px';
                            nameHold.style.zIndex = '14';
                            nameHold.style.backgroundColor = '#000000';
                            nameHold.style.opacity = 0.7;

                            nameHold.appendChild(plName);

                            var btnDiv = document.createElement('div');
                            btnDiv.id = ob+'_thumb';
                            btnDiv.style.position = 'absolute';
                            btnDiv.style.top = '0px';
                            btnDiv.style.width = pStuff.style.width;
                            btnDiv.style.height = pStuff.style.height;
                            btnDiv.style.zIndex = '16';

                            pStuff.appendChild(btnDiv);
                            pStuff.appendChild(pThumb);
                            pStuff.appendChild(nameHold);

                            let txtWidth = parseInt(window.getComputedStyle(plName, null).getPropertyValue("width"));
                            let txtHeight = parseInt(window.getComputedStyle(plName, null).getPropertyValue("height"));
                            let holdWidth = parseInt(nameHold.style.width);

                            plName.style.right = scrolTxtPos(holdWidth, txtWidth);
                            plName.style.width = txtWidth + 2 + 'px';
                            plName.style.height = txtHeight + 'px';
                            
                            document.getElementById(pStuff.id).addEventListener('mouseover', thumbOver, false);
                            document.getElementById(pStuff.id).addEventListener('mouseout', thumbOut, false);
                            document.getElementById(pStuff.id).addEventListener('click', thumbClick, false);

                            if(obPos === 0){
                                nowPlay = ob+'_title';
                                plName.style.color = setCtrl.nowPlayColor;
                                video.id = ob;
                                // if has top location make link absolute
                                if(tLoc === ''){
                                    video.poster = objct[ob].images.p_0.loc;
                                    video.innerHTML = "<source src="+objct[ob].loc+'/'+objct[ob].mp4[setCtrl.uSetRez]+" type='video/mp4'>\n<source src="+objct[ob].loc+'/'+objct[ob].webm[setCtrl.uSetRez]+" type='video/webm'>";
                                }
                                else{
                                    tLoc = tLoc+'/';
                                    video.poster = tLoc+objct[ob].images.p_0.loc;
                                    video.innerHTML = "<source src="+tLoc+objct[ob].loc+'/'+objct[ob].mp4[setCtrl.uSetRez]+" type='video/mp4'>\n<source src="+tLoc+objct[ob].loc+'/'+objct[ob].webm[setCtrl.uSetRez]+" type='video/webm'>";
                                }
                                video.load();
                            }

                            obPos = obPos + vidObj.thumb.t_0.height + 5;
                            v++;
                        }
                    }
                }

                pList.style.width = pListW + 'px';
                pList.style.zIndex = '12';

                pListHolder.style.height = player.style.height;
                pListHolder.style.width = pListW + 20 + 'px';
                pListHolder.style.zIndex = '10';
                pListHolder.style.top = 0 + 'px';
                pListHolder.style.left = parseInt(video.style.left) + parseInt(video.style.width) + 10 + 'px';

                pScroll.style.height = pListHolder.style.height;
                pScroll.style.width = pListW + 20 + 'px';
                pScroll.style.opacity = '1.0';
            }


            var aboutBG = document.createElement('div');
            aboutBG.id = 'aboutBG';
            aboutBG.style.position = 'absolute';
            aboutBG.style.display = 'block';
            player.appendChild(aboutBG);

            rSizeVid();
        }

        function getTopDir(){
            if(setCtrl.topDir === ''){
                if(objct.settings.topDir === ''){
                    return '';
                }else{
                    return objct.settings.topDir;
                }
            }else{
                return setCtrl.topDir;
            }
        }

        // These functions handle the thumbnails
        function thumbOver(e){
            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_thumb', '_pic');

            let txt = document.getElementById(tgt.id.replace('_thumb', '_title'));
            let txtHoldWidth = parseInt(document.getElementById(tgt.id.replace('_thumb', '_nameHold')).style.width);
            let txtWidth = parseInt(window.getComputedStyle(txt, null).getPropertyValue("width"));
            txtScroller(txt, txtWidth, txtHoldWidth);

            let pic = document.getElementById(tId);
            TweenMax.to(pic, 0.1, {scaleX: 1.15, scaleY: 1.15});
        }

        function thumbOut(e){
            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_thumb', '_pic');
            let pic = document.getElementById(tId);
            TweenMax.to(pic, 0.1, {scaleX: 1, scaleY: 1});
        }

        function thumbClick(e){
            seek.value = 1;
            if(!video.paused){
                isPlaying = false;
                video.pause();
                video.currentTime = 0;
            }

            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_thumb', '');
            video.id = tId;
            if(nowPlay !== undefined){
                document.getElementById(nowPlay).style.color = '#fff';
            }
            nowPlay = tId+'_title';
            document.getElementById(tId+'_title').style.color = setCtrl.nowPlayColor;
            let vObj = objct[tId];
            let tLoc = getTopDir();

            // remove elements for recreation of them with new data rezBtns
            sizeCtrl.removeChild(document.getElementById('rSizeRHolder'));
            // remove the about stuff
            document.getElementById('aboutBG').removeChild(document.getElementById('aboutScroller'));
            document.getElementById('aboutBG').removeChild(document.getElementById('aboutVidName'));

            // scroll thumb text
            let txt = document.getElementById(tgt.id.replace('_thumb', '_title'));
            let txtHoldWidth = parseInt(document.getElementById(tgt.id.replace('_thumb', '_nameHold')).style.width);
            let txtWidth = parseInt(window.getComputedStyle(txt, null).getPropertyValue("width"));
            txtScroller(txt, txtWidth, txtHoldWidth);

            // change video resolution settings
            rSizeVid(vObj);

            // if has top location make link absolute
            if(tLoc === ''){
                video.poster = vObj.images.p_0.loc;
                video.innerHTML = "<source src="+vObj.loc+'/'+vObj.mp4[setCtrl.uSetRez]+" type='video/mp4'>\n<source src="+vObj.loc+'/'+vObj.webm[setCtrl.uSetRez]+" type='video/webm'>";
            }
            else{
                tLoc = tLoc+'/';
                video.poster = tLoc+vObj.images.p_0.loc;
                video.innerHTML = "<source src="+tLoc+vObj.loc+'/'+vObj.mp4[setCtrl.uSetRez]+" type='video/mp4'>\n<source src="+tLoc+vObj.loc+'/'+vObj.webm[setCtrl.uSetRez]+" type='video/webm'>";
            }
            
            buffedOut.innerHTML = '';

            video.load();
            if(video.preload === 'auto'){
                if(setCtrl.progressFix !== false && setCtrl.progressFix.indexOf(userA) >= 0){
                    clearInterval(checkBuff);
                    checkBuff = setInterval(getProgress, 1 * 1000);
                }else{
                    video.addEventListener('progress', getProgress);
                }
            }
            seek.value = 1;
            shoVidCtrl();
            clearTimeout(mouseIdel);

        }

        function txtScroller(txt, txtWidth, txtHold){
            if(!TweenMax.isTweening(txt)){
                let time = txtWidth / txtHold * 5;
                // console.log(time);
                let timeStart = time * .75;
                let timeEnd = time * .25;

                let txTween = TweenMax.to(txt, timeStart, {
                    right: txtHold + 3 + 'px',
                    delay: 0.2,
                    ease: Power0.easeIn,
                    onComplete:function(){
                        TweenMax.fromTo(txt, timeEnd, {
                            right: -txtWidth
                        }, {
                            right: scrolTxtPos(txtHold, txtWidth),
                            ease: Sine.easeOut
                        });
                    }
                });
            }
        }

        function rSizeVid(vObj){
            // this function creates the about elements and resizes the video
            let i, j, obc;
            if(vObj === undefined || vObj === ''){
                j = 1;
                let key = false;
                for(obc in objct){
                    if(obc == video.id){
                        vObj = objct[obc];
                        key = true;
                        break;
                    }
                    j++;
                }
                if(!key){
                   mkLog('If you want the resolution changer to work initially you must give your video a id that has a key from the json file.');
                }
            }else{
                j = 1;
                for(obc in objct){
                    if(objct[obc] == vObj){
                        break;
                    }
                    j++;
                }
            }

            // create the about video section
            let aboutVidTxt = document.createElement('div');
            aboutVidTxt.id = 'aboutVidTxt';
            aboutVidTxt.style.display = 'block';

            let aboutHolder = document.createElement('div');
            aboutHolder.id = 'aboutHolder';
            aboutHolder.style.position = 'relative';
            aboutHolder.style.display = 'block';

            let aboutScroller = document.createElement('div');
            aboutScroller.id = 'aboutScroller';
            aboutScroller.style.position = 'relative';
            aboutScroller.style.display = 'block';

            let aboutBG = document.getElementById('aboutBG');

            let vidName = document.createElement('h3');
            vidName.id = 'aboutVidName';
            vidName.style.display = 'block';
            aboutBG.appendChild(vidName);
            
            // create the resize buttons
            let rSizeR = document.createElement('div');
            rSizeR.id = 'rSizeR';
            rSizeR.style.position = 'absolute';
            rSizeR.style.width = btnW;
            
            let rSizeRHolder = document.createElement('div');
            rSizeRHolder.id = 'rSizeRHolder';
            rSizeRHolder.style.position = 'absolute';
            rSizeRHolder.style.width = btnW;
            rSizeRHolder.style.overflow = 'hidden';

            rSizeRHolder.appendChild(rSizeR);
            sizeCtrl.appendChild(rSizeRHolder);

            nPos = 2;
            order = ['HD', 'HQ', 'SD', 'LQ', 'M'];
            let x = 1;
            for(objc in objct){
                if(x === j){
                    for(i = 0; i <= order.length - 1; i++){
                        for(ob in objct[objc].mp4){
                            if(order[i] == ob){
                                // build the sizers and eventListeners
                                let rezC = document.createElement('div');
                                rezC.id = ob;
                                rezC.className = 'rezCtrls';
                                rezC.style.width = btnW;
                                rezC.style.height = btnH;
                                rezC.style.display = 'block';
                                rezC.style.textAlign = 'center';
                                if(i === 0){
                                    rezC.style.padding = '10px 0px 0px 0px';
                                }
                                if(rezC.id == setCtrl.uSetRez){
                                    rezC.style.color = setCtrl.rezColorSelect;
                                }else{
                                    rezC.style.color = setCtrl.rezColorOver;
                                }
                                rezC.innerHTML = ob;
                                rSizeR.appendChild(rezC);
                                rezC.style.bottom = nPos + 'px';
                                nPos = nPos + parseInt(btnH);
                                document.getElementById(rSizeR.id).addEventListener('mouseover', rSizeOver, false);
                                document.getElementById(rSizeR.id).addEventListener('mouseout', rSizeOut, false);
                                document.getElementById(rSizeR.id).addEventListener('click', rSizeClick, false);
                            }
                        }
                    }

                    // build the about info
                    let inTxt = objct[objc].about.split('\n');
                    for(let tx in inTxt){
                        let pTag = document.createElement('p');
                        pTag.className = 'videoDesription_P';
                        pTag.innerHTML = inTxt[tx];
                        aboutVidTxt.appendChild(pTag);
                    }
                    vidName.innerHTML = objct[objc].name;
                    break;
                }
                x++;
            }

            // position the resize buttons
            fsb.style.zIndex = '13';
            rSizeR.style.height = nPos + 'px';
            rSizeR.style.left = '0px';
            rSizeR.style.bottom = '0px';
            rSizeR.style.zIndex = '12';
            rSizeRHolder.style.left = '0px';
            rSizeRHolder.style.bottom = parseInt(ctrl.style.bottom) + parseInt(ctrl.style.height) - padding + 'px';
            rSizeRHolder.style.height = '0px';
            rSizeRHolder.style.zIndex = '10';

            // position the about section
            aboutHolder.appendChild(aboutVidTxt);
            aboutScroller.appendChild(aboutHolder);
            aboutBG.appendChild(aboutScroller);

            aboutBG.style.left = video.style.left;
            aboutBG.style.top = parseInt(video.style.top) + parseInt(video.style.height) + 30 + 'px';
            aboutBG.style.width = parseInt(player.style.width) + parseInt(pListHolder.style.width) + 'px';
            aboutBG.style.zIndex = '11';

            aboutScroller.style.width = parseInt(player.style.width) + parseInt(pListHolder.style.width) + 'px';
            aboutScroller.style.zIndex = '11';
            aboutScroller.style.margin = '0px 10px 10px 0px';

            aboutHolder.style.left = '0px';
            aboutHolder.style.top = '0px';
            aboutHolder.style.width = parseInt(player.style.width) + parseInt(pListHolder.style.width) - 15 + 'px';
            aboutHolder.style.zIndex = '11';

            vidName.style.zIndex = '2';
            aboutVidTxt.style.zIndex = '12';
            aboutVidTxt.style.width = '98%';
            aboutVidTxt.style.margin = '0 auto';

            rSize.addEventListener('click', shoSize);
            let sizSho = true;

            // these functions handle the resize function
            function rSizeOver(e){
                let tgt = e.target || e.srcElement || window.event;
                tgt.style.color = setCtrl.rezColorUp;
                TweenMax.to(tgt, 0.1, {scaleX: 1.1, scaleY: 1.1});
            }

            function rSizeOut(e){
                let tgt = e.target || e.srcElement || window.event;
                TweenMax.to(tgt, 0.1, {scaleX: 1, scaleY: 1});
                if(tgt.id == setCtrl.uSetRez){
                    tgt.style.color = setCtrl.rezColorSelect;
                }else{
                    tgt.style.color = setCtrl.rezColorOver;
                }
            }

            function rSizeClick(e){
                buffedOut.innerHTML = '';
                let cTime = video.currentTime;
                let tgt = e.target || e.srcElement || window.event;
                let tLoc = getTopDir();
                document.getElementById(setCtrl.uSetRez).style.color = setCtrl.rezColorOver;
                setCtrl.uSetRez = tgt.id;
                tgt.style.color = setCtrl.rezColorUp;

                // if has top location make link absolute
                if(tLoc === ''){
                    video.poster = vObj.images.p_0.loc;
                    video.innerHTML = "<source src="+vObj.loc+'/'+vObj.mp4[tgt.id]+" type='video/mp4'>\n<source src="+vObj.loc+'/'+vObj.webm[tgt.id]+" type='video/webm'>";
                }
                else{
                    tLoc = tLoc+'/';
                    video.poster = tLoc+vObj.images.p_0.loc;
                    video.innerHTML = "<source src="+tLoc+vObj.loc+'/'+vObj.mp4[tgt.id]+" type='video/mp4'>\n<source src="+tLoc+vObj.loc+'/'+vObj.webm[tgt.id]+" type='video/webm'>";
                }

                seek.value = seek.value;
                video.load();
                video.currentTime = cTime;

                if(isPlaying){
                    video.play();
                }
            }
            function shoSize(){
                TweenMax.to(rSize_btn, 0, {fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0});
                TweenMax.to(rSize_btn, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
                if(sizSho){
                    TweenMax.to(rSizeRHolder, 0.3, {height: nPos});
                    rSize.style.zIndex = '10';
                    rSizeRHolder.style.zIndex = '12';
                    rSizeR.style.zIndex = '13';
                    sizSho = false;
                }else{
                    hideSize();
                    sizSho = true;
                }
            }
            function hideSize(){
                TweenMax.to(rSizeRHolder, 0.5, {height: 0});
                rSizeRHolder.style.zIndex = '10';
                rSizeR.style.zIndex = '10';
                rSize.style.zIndex = '12';
            }
        }
    };

    if(loc.includes('.json')){
        req.send(null);
    }
    else{
        req.send(vars);
    }

    // these functions are for the main buttons.
    function btnOver(e){
        let bt = e.target || e.srcElement || window.event;
        let btN, btN2;
        if(bt.id == 'unMuteTop'){
            btN = bt.id.replace('Top', '1_btn');
            btN2 = bt.id.replace('Top', '2_btn');
            btN = document.getElementById(btN);
            btN2 = document.getElementById(btN2);
            TweenMax.to(btN, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
            TweenMax.to(btN2, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
        }else{
            btN = bt.id.replace('Top', '_btn');
            btN = document.getElementById(btN);
            TweenMax.to(btN, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
        }
    }

    function btnOut(e){
        let bt = e.target || e.srcElement || window.event;
        let btN, btN2;
        if(bt.id == 'unMuteTop'){
            btN = bt.id.replace('Top', '1_btn');
            btN2 = bt.id.replace('Top', '2_btn');
            btN = document.getElementById(btN);
            btN2 = document.getElementById(btN2);
            TweenMax.to(btN, 0.5, {fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0});
            TweenMax.to(btN2, 0.5, {fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0});
        }else{
            btN = bt.id.replace('Top', '_btn');
            btN = document.getElementById(btN);
            TweenMax.to(btN, 0.5, {fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0});
        }
    }
    // end main button functions

    function chBtnBgColor(topColor, topAlpha, bottomColor, bottomAlpha){
        TweenMax.to(bottomFill, 0, {fill: topColor, fillOpacity: topAlpha});
        TweenMax.to(topFill, 0, {fill: bottomColor, fillOpacity: bottomAlpha});
    }

    function chBtnBgAlpha(topAlpha, bottomAlpha){
        TweenMax.to(bottomFill, 0, {fillOpacity: topAlpha});
        TweenMax.to(topFill, 0, {fillOpacity: bottomAlpha});
    }

    function spacePP(event){
        // console.log(event.keyCode);
        if(event.keyCode == 32){
            playPause();
        }
    }

    function playPause(){
        if(!video.paused){
            isPlaying = false;
            video.pause();
            video.volume = vol.value / 100;
            if(!ctrlHidden){
                play.style.zIndex = '12';
                play.style.visibility = 'visible';
                pause.style.zIndex = '10';
                pause.style.visibility = 'hidden';
            }

            // here is where you need to put the timeOut function!!!
            if(video.preload === 'none' || video.preload === 'auto'){
                if(setCtrl.progressFix !== false && setCtrl.progressFix.indexOf(userA) >= 0){
                    if(!killTimeout){
                        killTimeout = true;
                        // clearTimeout(killProgress);
                        killProgress = setTimeout(function(){
                            clearTimeout(killProgress);
                            clearInterval(checkBuff);
                            killTimeout = false;
                        }, 45 * 1000);
                    }
                }
            }
        }
        else{
            isPlaying = true;
            video.play();
            video.volume = vol.value / 100;
            if(!ctrlHidden){
                play.style.zIndex = '10';
                play.style.visibility = 'hidden';
                pause.style.zIndex = '12';
                pause.style.visibility = 'visible';
            }

            /*/ This is a firefox helper. As of FF-55 im having problems with FF dropping the video buffered progress /*/
            if(video.preload === 'none' || video.preload === 'auto'){
                if(setCtrl.progressFix !== false && setCtrl.progressFix.indexOf(userA) >= 0){
                    clearTimeout(killProgress);
                    clearInterval(checkBuff);
                    checkBuff = setInterval(getProgress, 1 * 1000);
                }else{
                    video.addEventListener('progress', getProgress);
                }
            }
        }
    }

    function muteVideo(){
        if(video.muted){
            vol.value = video.volume * 100;
            video.muted = false;
            muteVid.style.zIndex = '10';
            muteVid.style.visibility = 'hidden';
            unMuteVid.style.zIndex = '12';
            unMuteVid.style.visibility = 'visible';
            TweenMax.to(unMute1_btn, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
            TweenMax.to(unMute2_btn, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
        }else{
            video.muted = true;
            vol.value = 0;
            muteVid.style.zIndex = '12';
            muteVid.style.visibility = 'visible';
            unMuteVid.style.zIndex = '10';
            unMuteVid.style.visibility = 'hidden';
            TweenMax.to(mute_btn, 0.5, {fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1});
        }
    }

    function fullScreen(){
        fsb.removeEventListener('click', fullScreen, false);
        fsb.addEventListener('click', exitFullScreen, false);
        if(player.requestFullscreen){
          player.requestFullscreen();
        }else if(player.mozRequestFullScreen){
          player.mozRequestFullScreen();
        }else if(player.webkitRequestFullscreen){
          player.webkitRequestFullscreen();
        }else if(player.msRequestFullscreen){
            player.msRequestFullscreen();
        }else{
            mkLog('Your browser does not support full screen.');
        }
        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;

        if(full === undefined || Object.getPrototypeOf(full) !== Object.prototype){
            let aboutBG = document.getElementById('aboutBG');
            let pListHolder = document.getElementById('pListHolder');
            aboutBG.style.visibility = 'hidden';
            pListHolder.style.visibility = 'hidden';
            elmPosFS();
            escFsEvts('mk');
        }
    }

    function exitFullScreen(){
        fsb.removeEventListener('click', exitFullScreen, false);
        fsb.addEventListener('click', fullScreen, false);
        if(document.exitFullscreen){
            document.exitFullscreen();
        }else if(document.webkitExitFullscreen){
            document.webkitExitFullscreen();
        }else if(document.mozCancelFullScreen){
            document.mozCancelFullScreen();
        }else if(document.msExitFullscreen){
            document.msExitFullscreen();
        }else{
            mkLog('Your browser does not support full screen.');
        }
        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        
        if(full === undefined || Object.getPrototypeOf(full) === Object.prototype){
            let aboutBG = document.getElementById('aboutBG');
            let pListHolder = document.getElementById('pListHolder');
            aboutBG.style.visibility = 'visible';
            pListHolder.style.visibility = 'visible';
            elmPosNorm();
            document.getElementById(player.id).scrollIntoView();
        }
    }
    
    function escFsEvts(doWhat){
        let fSh = ['fullscreenchange', 'mozfullscreenchange', 'MSFullscreenChange', 'webkitfullscreenchange'];
        
        if(doWhat == 'mk'){
            for(let fH in fSh){
                document.addEventListener(fSh[fH], escBtn);
            }
        }else{
            for(let fH in fSh){
                document.removeEventListener(fSh[fH], escBtn);
            }
        }
    }

    function escBtn(){
        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        if(full === null || full === undefined || Object.getPrototypeOf(full) === Object.prototype){
            if(player.style.height == '100%' || video.style.width == '100%'){
                exitFullScreen();
                escFsEvts('rm');
                document.getElementById(player.id).scrollIntoView();
            }
        }
    }

    function videoSeeker(){
        let uMoveSeek = video.duration * (seek.value / 100);
        video.currentTime = uMoveSeek;
    }

    function getProgress(){
        let bufferedEnd;
        let totalBuff = 0;
        let duration = video.duration;
        if(video.buffered.length - 1 >= 0){
            bufferedEnd = video.buffered.end(video.buffered.length - 1);
            timeTrack();
        }

        for(let i = 0; i <= video.buffered.length - 1; i++){
            let bufLeft = Math.round(video.buffered.start(i) / duration * 100);
            let endLessStart = video.buffered.end(i) - video.buffered.start(i);
            let bufWidth = Math.round(endLessStart / duration * 100);

            totalBuff = totalBuff + endLessStart;

            if(document.getElementById('bufProg_' + i) === null){
                let buffedProg = document.createElement('div');
                buffedProg.id = 'bufProg_' + i;
                buffedProg.className = 'buffProg';
                buffedOut.appendChild(buffedProg);
                buffedProg.style.position = 'absolute';

                if(Object.prototype.toString.call(setCtrl.bufferColor) === '[object Array]'){
                    buffedProg.style.backgroundColor = randClor(setCtrl.bufferColor);
                }else if(setCtrl.bufferColor == 'rainbow'){
                    buffedProg.style.backgroundColor = randClor();
                }else{
                    buffedProg.style.backgroundColor = setCtrl.bufferColor;
                }

                mkLog('Progress Buffer backgroundColor = '+buffedProg.style.backgroundColor);
                
                buffedProg.style.height = buffedOut.style.height;
                buffedProg.style.alpha = 0.75;
                buffedProg.style.top = '0px';
                buffedProg.style.display = 'inline-block';
                buffedProg.style.left = bufLeft + '%';
                buffedProg.style.width = bufWidth + '%';
            }
            else{
                let buffedProg = document.getElementById('bufProg_' + i);
                buffedProg.style.left = bufLeft + '%';
                buffedProg.style.width = bufWidth + '%';
            }
        }

        // This is starting to work right
        if(userA == 'firefox'){
            if(Math.floor(totalBuff) == Math.floor(duration)){
                clearInterval(checkBuff);
                clearTimeout(killProgress);
                killTimeout = false;
            }else{
                if(compareBuff < totalBuff && isPlaying){
                    clearTimeout(killProgress);
                    killTimeout = false;
                    compareBuff = totalBuff;                    
                }else{
                    if(!killTimeout && video.paused){
                        killTimeout = true;
                        killProgress = setTimeout(function(){
                            clearTimeout(killProgress);
                            clearInterval(checkBuff);
                            killTimeout = false;
                        }, 45 * 1000);
                    }
                }
            }
        }
    }

    function randClor(colArr){
        if(colArr === undefined || Object.prototype.toString.call(colArr) !== '[object Array]'){
            let leterColor = ['A', 'B', 'C', 'D', 'E', 'F'];
            let newColor = '';
            for(let i = 6; i >= 1; i--){
                if((Math.floor(Math.random() * (1+498-1)) + 1) %2 === 0){
                    newColor = newColor + leterColor[Math.floor(Math.random() * 6)];
                }else{
                    newColor = newColor + Math.floor(Math.random() * 10);
                }
            }
            return '#' + newColor;
        }
        else{
            let newArr = Array();
            for(let color in colArr){
                if(colArr[color].match(/^#?[A-Fa-f0-9]{6}[\s]{0,1}$/g)){
                    if(colArr[color].substring(0, 1) !== '#'){
                        newArr.push('#'+colArr[color].toUpperCase());
                    }else{
                        newArr.push(colArr[color].toUpperCase());
                    }
                }
            }
            return newArr[randNum(0, newArr.length - 1)];
        }
    }

    function randNum(low, high){
        return Math.floor(Math.random() * (1+high-low)) + low;
    }

    function volumeSlider(){
        video.volume = vol.value / 100;
    }

    function timeTrack(){
        if(video.ended){
            video.currentTime = 0;
            isPlaying = false;
        }

        let now = video.currentTime * (100 / video.duration);
        let nowMin = Math.floor(video.currentTime / 60);
        let nowSec = Math.floor(video.currentTime - nowMin * 60);
        let nowHr = Math.floor(nowMin / 60);
        let totalMin = Math.floor(video.duration / 60);
        let totalSec = Math.round(video.duration - totalMin * 60);
        let totalHr = Math.floor(totalMin / 60);
        if(!isNaN(totalSec)){
            seek.value = now;

            if(nowSec < 10){
                nowSec = '0'+nowSec;
            }
            if(totalSec < 10){
                totalSec = '0'+totalSec;
            }
            if(nowMin < 10){
                nowMin = '0'+nowMin;
            }
            if(totalMin < 10){
                totalMin = '0'+totalMin;
            }
            if(totalHr >= 1){
                if(nowMin >= 60){
                    nowMin = nowMin - (60 * totalHr);
                    if(nowMin < 10){
                        nowMin = '0'+nowMin;
                    }
                }
                
                totalMin = totalMin - (60 * totalHr);
                timeTxt.innerHTML = nowHr+':'+nowMin+':'+nowSec+' | ';
                totalTxt.innerHTML = totalHr+':'+totalMin+':'+totalSec;
            }else{
                timeTxt.innerHTML = nowMin+':'+nowSec+' | ';
                totalTxt.innerHTML = totalMin+':'+totalSec;
            }
        }
        else{
            seek.value = 1;
        }
        
    }

    function shoVidCtrl(){
        ctrlHidden = false;
        if(!video.paused){
            play.style.zIndex = '10';
            play.style.visibility = 'hidden';
            pause.style.zIndex = '12';
            pause.style.visibility = 'visible';
        }else{
            play.style.zIndex = '12';
            play.style.visibility = 'visible';
            pause.style.zIndex = '10';
            pause.style.visibility = 'hidden';
        }

        if(!video.muted){
            muteVid.style.zIndex = '10';
            muteVid.style.visibility = 'hidden';
            unMuteVid.style.zIndex = '12';
            unMuteVid.style.visibility = 'visible';
        }else{
            muteVid.style.zIndex = '12';
            muteVid.style.visibility = 'visible';
            unMuteVid.style.zIndex = '10';
            unMuteVid.style.visibility = 'hidden';
        }
        ctrl.style.visibility = 'visible';
        TweenMax.to(ctrl, 0.5, {opacity:1});
        video.style.cursor = 'default';      
    }

    function hideVidCtrl(){
        ctrlHidden = true;
        TweenMax.to(ctrl, 0.5, {opacity:0, onComplete:function(){
            ctrl.style.visibility = 'hidden';
            play.style.visibility = 'hidden';
            pause.style.visibility = 'hidden';
            muteVid.style.visibility = 'hidden';
            unMuteVid.style.visibility = 'hidden';
            video.style.cursor = 'none';
        }});
    }

    function shoVol(){
        TweenMax.to(volCtrl, 0.5, {height:(parseInt(playerH) * 0.3)});
    }

    function hideVol(){
        TweenMax.to(volCtrl, 0.5, {height:parseInt(btnH)});
    }

    function parseInt(str){
        //console.log(str);
        str = str.replace("px", "").replace("%", ""); 
        return Number(str);
    }

    function mkLog(txt){
        if(verbose){
            console.log(txt);
        }
    }

    function getUserAgent(listAvailableUAs){
        if(listAvailableUAs === undefined || listAvailableUAs === null || listAvailableUAs !== true){
            listAvailableUAs = false;
        }
        let userArr = {
            chrome: /Chrome/.test(navigator.userAgent),
            safari: /Safari/.test(navigator.userAgent),
            iPad: /iPad/.test(navigator.userAgent),
            iPhone: /iPhone/.test(navigator.userAgent),
            droid: /Android/.test(navigator.userAgent),
            firefox: /Firefox/.test(navigator.userAgent)
        };
        let uaList = '';
        let noUserAgent;
        for(let obj in userArr){
            if(listAvailableUAs){
                if(uaList === ''){
                    uaList = obj;
                }else{
                    uaList = uaList + ', ' + obj;
                }
            }else{
                if(userArr[obj]){
                    return obj;
                }else{
                    noUserAgent = true;
                }
            }
        }
        if(listAvailableUAs){
            return uaList;
        }
        if(noUserAgent){
            return 'The User-Agent your looking for is not in the list of User-Agents to search for. You can add it manually in the getUserAgent.userArr Object.';
        }
    }

    function scrolTxtPos(holdWidth, txtWidth){
        if(holdWidth < txtWidth){
            let txtRight = txtWidth - holdWidth;
            let txtPad = holdWidth * 0.25;
            return -(txtRight + txtPad)+ 'px';
        }else{
            return '3px';
        }
    }

    function elmPosNorm(){
        // video (html video element)
        video.style.height = playerH;
        video.style.width = playerW;

        // player (div), holds the video and the controls
        player.style.height = playerH;
        player.style.width = playerW;
        player.style.top = playerT;
        player.style.left = playerL;
        
        // ctrl (div), the complete controls div
        ctrl.style.width = parseInt(playerW) + 'px';
        ctrl.style.height = parseInt(btnH) + 10 + 'px';
        ctrl.style.position = 'absolute';
        ctrl.style.bottom = '0px';
        ctrl.style.left = '0px';

        // ctrls (div), the left controls, play, seek, time
        ctrls.style.width = playerW;
        ctrls.style.height = ctrl.style.height;

        // vidCtrlBG (div), the background for the controls
        vidCtrlBG.style.width = ctrl.style.width;
        vidCtrlBG.style.height = ctrl.style.height;

        // sideCtrl (div), holds -> volCtrl, sizeCtrl, fsb
        sideCtrl.style.bottom = '0px';
        sideCtrl.style.right = '0px';
        sideCtrl.style.height = btnH;

        // volCtrl (div), holds -> mute, un-mute, volume slider
        volCtrl.style.bottom = '0px';

        // sizeCtrl (div), holds -> resize btn and size controls.
        // size controls are added from ajax 
        sizeCtrl.style.left = parseInt(muteVid.style.width) + 5 +'px';
        sizeCtrl.style.bottom = volCtrl.style.bottom;

        // play (img:button)
        play.style.top = '0px';
        play.style.left = '0px';

        // pause (img:button)
        pause.style.top = '0px';
        pause.style.left = '0px';

        // fsb:FullScreen (img:button)
        fsb.style.left = parseInt(sizeCtrl.style.left) + parseInt(sizeCtrl.style.width) + 'px';
        fsb.style.bottom = '0px';

        muteVid.style.bottom = '0px';
        unMuteVid.style.bottom = '0px';

        // vol (input:range), controls the volume
        vol.style.bottom = parseInt(muteVid.style.height) + parseInt(volCtrl.style.height) - parseInt(btnH) + setCtrl.volOffset + 'px';
        vol.style.left = parseInt(muteVid.style.width) / 50 + 'px';

        // seek (input:range), controls the video scrubber
        seek.style.left = parseInt(play.style.left) + parseInt(play.style.width) + 8 + 'px';
        seek.style.top = parseInt(play.style.height) / 2 + parseInt(seek.style.height) +'px';
        seek.style.width = parseInt(ctrl.style.width) - parseInt(sideCtrl.style.width) - parseInt(play.style.width) - 20 + 'px';

        // buffedOut (div:container). Holds the buffer objects
        buffedOut.style.position = seek.style.position;
        buffedOut.style.top = parseInt(seek.style.top) + 1 + 'px';
        buffedOut.style.left = parseInt(seek.style.left) + 2 + 'px';
        buffedOut.style.zIndex = seek.style.zIndex - 1;
        buffedOut.style.height = '10px';
        buffedOut.style.width = seek.style.width;
        buffedOut.style.margin = '5px 0px 5px 0px';

        // Minor fix for webkit browsers
        if(userA === 'chrome' || userA === 'safari' || userA === 'opera'){
            seek.style.top = '18px';
            buffedOut.style.top = parseInt(seek.style.top) - 4 + 'px';
        }

        // time (div), holds the time spans [0:25:32 : 1:27:49]
        time.style.top = '-2px';
        time.style.right = parseInt(sideCtrl.style.right) + parseInt(sideCtrl.style.width) + (parseInt(btnW) * 1.25) + 'px';
    }

    function elmPosFS(){
        player.style.top = '0px';
        player.style.left = '0px';
        player.style.width = '100%';
        player.style.height = '100%';
        video.style.width = '100%';
        video.style.height = '100%';
        ctrl.style.width = window.innerWidth + 'px';
        ctrl.style.bottom = '0px';
        ctrls.style.width = '100%';
        vidCtrlBG.style.width = '100%';
        seek.style.width = wW - parseInt(sideCtrl.style.width) + parseInt(seek.style.left) - (parseInt(btnW) * 3) + 'px';
        time.style.top = '-2px';
        buffedOut.style.width = seek.style.width;
    }
}'''

        #print jsFile
        if getJS is True:
             return jsFile
        else:
            jsPath = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.js')
            if not os.path.exists(jsPath):
                jFile = open(jsPath, 'w')
                jFile.write(jsFile)
                jFile.close()

    def mkPHP(self):
        phpFile = '''<?php
if(@$_POST['functToGet'] == 'mkH5Videos'){
    $jFile = file_get_contents('mkH5videos.json');
    echo $jFile;
}
else{
    die();
}
?>'''
        #print phpFile
        phpPath = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.php')
        pFile = open(phpPath, 'w')
        pFile.write(phpFile)
        pFile.close()

    def mkCSS(self):
        cssFile = R'''

/*------------------- Important notes ------------------*/
/* This file was generated by the mkH5videos.py program.
The program will do its best to take most of the styling
off your hands. !important the colors for the range inputs
in this file come from the mkH5videos.json settings object.
The thumbnail and track for the volume and time scrubber 
will be set to the main button colors by default. 

** I have given most of the elements created by javaScript
an id or a class name. If you need to style something that
isn't in this css firebug is your friend. ** */

/* The background div for the thumbnails */
#pListHolder{
    position: absolute;
    left: 900px;
    height: 900px;
    background: #333;
    padding: 10px;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px;
    opacity: 0.90;
}
/* The video selector thumbnail scroller */
#playListScroll{
    height: 900px;
}

/* The video selector thumbnail text */
.thumbTitle{
    color: #fff;
    text-align: right;
    text-indent: 10px;
    font-size: 10px;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
    padding: 5px 5px 5px 10px;
    background: #000;;
    border: solid;
    border-width: 0px;
    border-color: #333;
    opacity: 0.80;
}

/* The background div for the about video text */
#aboutBG{
    width: 600px;
    background: #333;
    padding: 10px;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px;
    opacity: 0.90;
    z-index: -1;
    padding: 10px;
}
/* the about video paragraphs */
#aboutVidTxt p{
    color: #fff;
    text-indent: 15px;
    font-size: 12px;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
}
/* the about video header */
#aboutBG h3{
    color: thumbColor;
    text-indent: 10px;
    font-size: 14px;
    font-weight: bold;
    font-family: "Franklin Gothic Medium", "Franklin Gothic", "ITC Franklin Gothic", Arial, sans-serif;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: default;
}
/* The about holder scroller. Uncomment to allow scrolling
 of fixed height content. Comment out to allow dynamically
 changing height of content! */
/*#aboutScroller{
    position: absolute;
    height: 150px;
    overflow: auto;
}*/
/* the background for the video controls */
#vidCtrlBG{
    background: #333;
    padding: 0px;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px;
    opacity: 0.75;
}
/* the background for the resolution controls */
#rSizeR{
    background: #333;
    border: solid;
    border-width: 0px;
    border-color: #333;
    border-radius: 10px 10px 0px 0px;
    opacity: 0.75;
}
/* the class for the resolution control buttons */
.rezCtrls{
    font-size: 14px;
    font-family: 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif';
    font-weight: bolder;
    font-style: italic;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: pointer;
}
/* The div that holds the video player and controls*/
#player{
    background: #000;
}

/*-------- The button controls. --------*/
/*-------- Needs CSS Mobile Love --------*/
#playBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#playBtn:hover{
    opacity: 1;
}

#pauseBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#pauseBtn:hover{
    opacity: 1;
}

#muteBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#muteBtn:hover{
    opacity: 1;
}

#unMuteBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#unMuteBtn:hover{
    opacity: 1;
}

#rezBtn{
    border: none;
    cursor: pointer;
    opacity: 0.75;
}

#rezBtn:hover{
    opacity: 1.0;
}

#fScreenBtn{
    border: none;
    cursor: pointer;
    opacity: 0.72;
}

#fScreenBtn:hover{
    opacity: 1;
}
/* the time tracker text ( 0:24:19 | 1:25:37 )*/
.timeC{
    font-size: 11px;
    font-family: 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif';
    color: #fff;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: default;
}

/* enable this if you want the background to not have 
white dots in the corners. only an issue in webkit but
looks funny in fireFox */
input[type='range']{
    /*background-color: #000;*/
    border: 0px;
}

/*------------------- Volume Range Input ------------------*/

/* This is where you may need to find the volume thumbnail
and background track colors. I will make a comment above them
so you can find them easily.  */

.sliderC {
    -webkit-appearance: none;
    width: 100%;
    margin: 5.65px 0;
    cursor:  pointer;
}
.sliderC:focus {
    outline: none;
}
/* Chrome, Opera */
.sliderC::-webkit-slider-runnable-track {
    width: 100%;
    height: 12px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the volume 1 */
    background: TrackColor;
    border-radius: 4.9px;
    border: 1.8px solid #010101;
}
.sliderC::-webkit-slider-thumb {
    position: relative;
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 15px;
    width: 10px;
    border-radius: 9px;
    top: 2.5px;
    /* the thumbnail for the volume 1 */
    background: thumbColor;
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -7.45px;
}
.sliderC:focus::-webkit-slider-runnable-track {
    background: #949393;
}
/* Fire Fox */
.sliderC::-moz-range-track {
    width: 100%;
    height: 3.7px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the volume 2 */
    background: TrackColor;
    border-radius: 4.9px;
    border: 1.8px solid #010101;
}
.sliderC::-moz-range-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 15px;
    width: 10px;
    border-radius: 9px;
    /* the thumbnail for the volume 2 */
    background: thumbColor;
    cursor: pointer;
}
/* Satan */
.sliderC::-ms-track {
    width: 100%;
    height: 3.7px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
}
.sliderC::-ms-fill-lower {
    background: #7a7979;
    border: 1.8px solid #010101;
    border-radius: 9.8px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}
.sliderC::-ms-fill-upper {
    /* The track color for the volume 3 */
    background: TrackColor;
    border: 1.8px solid #010101;
    border-radius: 9.8px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}
.sliderC::-ms-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 15px;
    width: 10px;
    border-radius: 9px;
    /* the thumbnail for the volume 3 */
    background: thumbColor;
    cursor: pointer;
    height: 3.7px;
}
.sliderC:focus::-ms-fill-lower {
    background: TrackColor;
}
.sliderC:focus::-ms-fill-upper {
    /* The track color for the volume 4 */
    background: #949393;
}

/*_________________ End Volume Range Input ________________*/

/*--------------------Time Range Input---------------------*/

/* This is where you may need to find the video time thumbnail
and background track colors. I will make a comment above them
so you can find them easily.  */

/* Chrome, Opera */
.seekerC {
    -webkit-appearance: none;
    width: 150px;
    margin: 0.05px 0;
}
.seekerC:focus {
    outline: none;
}
.seekerC::-webkit-slider-runnable-track {
    width: 100%;
    height: 12px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the video 1 */
    background: TrackColor;
    border-radius: 4.7px;
    border: 2.2px solid #010101;
}
.seekerC::-webkit-slider-thumb {
    position: relative;
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 10px;
    width: 8px;
    top: 1px;
    border-radius: 9px;
    /* the thumbnail for the video 1 */
    background: thumbColor;
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -2.25px;
}
.seekerC:focus::-webkit-slider-runnable-track {
    background: #949393;
}


/* Fire Fox */
.seekerC::-moz-range-track {
    width: 100%;
    height: 9.9px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    /* The track color for the video 2 */
    background: TrackColor;
    border-radius: 4.7px;
    border: 2.2px solid #010101;
}
.seekerC::-moz-range-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 10px;
    width: 8px;
    border-radius: 9px;
    /* the thumbnail for the video 2 */
    background: thumbColor;
    cursor: pointer;
}
.seekerC::-moz-range-progress {
    /* the done track color for the video 1 */
    background-color: TrackDoneColor;
    height: 9.9px;
    cursor: pointer;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
    border-radius: 4.7px;
    border: 2.2px solid #010101; 
}

/* Satan */
.seekerC::-ms-track {
    width: 100%;
    height: 9.9px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
}
.seekerC::-ms-fill-lower {
    /* the done track color for the video 2 */
    background: TrackDoneColor;
    border: 2.2px solid #010101;
    border-radius: 9.4px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}
.seekerC::-ms-fill-upper {
    /* The track color for the video 3 */
    background: TrackColor;
    border: 2.2px solid #010101;
    border-radius: 9.4px;
    box-shadow: 1.1px 1.1px 3.1px #000000, 0px 0px 1.1px #0d0d0d;
}
.seekerC::-ms-thumb {
    box-shadow: 1.3px 1.3px 1.6px #000000, 0px 0px 1.3px #0d0d0d;
    border: 1px solid #000000;
    height: 10px;
    width: 8px;
    border-radius: 9px;
    /* the thumbnail for the video 3 */
    background: thumbColor;
    cursor: pointer;
    height: 9.9px;
}
.seekerC:focus::-ms-fill-lower {
    /* the done track color for the video 3 */
    background: TrackColor;
}
.seekerC:focus::-ms-fill-upper {
    /* The track color for the video 4 */
    background: #949393;
}

/*__________________ End Time Range Input _________________*/


'''
        jsonFile = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.json')
        # print jsonFile
        if os.path.exists(jsonFile):
            jFexists = True
            try:
                jsData = open(jsonFile)
                jData = json.load(jsData)
                jsData.close()
            except ValueError:
                jData = json.loads(jsonFile)
        else:
            jFexists = False

        if jFexists:
            cssFile = cssFile.replace('thumbColor', jData['settings']['thumbColor'])
            cssFile = cssFile.replace('TrackColor', jData['settings']['trackColor'])
            cssFile = cssFile.replace('TrackDoneColor', jData['settings']['trackColorDone'])
            cssPath = os.path.join(self.baseDir, 'mkH5videos', 'scripts', 'mkH5videos.css')
            cFile = open(cssPath, 'w')
            cFile.write(cssFile)
            cFile.close()
            print cssFile
        else:
            print 'To make the css file the json file must exist.'


    def mkHelp(self):
        h = '''

mkH5videos was created by Austin Byron dev@3ch0peak.com for personal use and has
released the source code under the GPL license for others to enjoy. Please feel
free to modify redistribute at your convenience but I do hope that my work herein
always remains free of charge. It is without warranty and I am not responsible 
for damages should any occur.

This script creates HTML5 encoded videos in webm and H264(mp4) formats in
multiple sizes.

This file depends on the following programs being installed:
    1.) FFmpeg 2.8.6
        Codecs:
            (a): libx264
            (b): libvpx
    2.) FFprobe
    3.) qt-faststart

This program also uses GSAP for javaScript animations. If you are going to be 
using this player commercially you need to check out their terms of service.
http://greensock.com/licensing/

Austin Byron and/or 3ch0peak are in no way affiliated with FFmpeg other than a
profound respect for the hard work they have done to make the best encoders 
for video, audio.

This script does not include FFmpeg. You will have to install and set up on 
your system.

Debian:
  check if available:
    apt-cache search
  if available:
    sudo apt-get install ffmpeg

If you are running Windows, Mac or need other installation instructions:
  https://ffmpeg.org/download.html

-------------------------------------------------------------------------

mkH5video usage:

$ python mkH5videos.py [*args] {**kwargs}

example:

$ python mkH5videos.py myvideo1.mp4 myvideo2.webm widescreen=true

$ python mkH5videos.py myvideo.avi setsize=1920x1080 \\
> bitrate=1900000 maxbit=2100000 buffersize=4200000 json=true

-------------------------------------------------------------------------

Options:
  General:

    -h, --help               Prints this help menu and exit.

    -v, --version            Prints the program version and exit.

    -y                       Forces the answer yes to all questions.

    -n                       Sets FFmepg to default and may need answers before
                               tasks are completed. Handy for testing video size.   
  Advanced:
    html=                    Takes true, True or false, False. Defaults to False
                               Makes html5 file.

    json=                    Takes true, True or false, False. Defaults to False.
                               Makes json file to be used by JSfile. The function
                               reads all files from the folders created by this
                               program and makes valid json accordingly. You can 
                               make and remake the json file as needed.

    jScript=                 Takes true, True or false, False. Defaults to False.
                               Makes javaScript file.

    php=                     Takes true, True or false, False. Defaults to False.
                               Makes PHP file.

    css=                     Takes true, True or false, False. Defaults to False.
                               Makes CSS file. **Needs the JSON file**

    alWeb=                   Takes true, True or false, False. Defaults to False
                               Makes html5, css, php and javaScript files.

    playersize=              Takes numbers separated by 'x' : 1920x1080
                               width then height of video player in your HTML.
                               Default is set to 16:9 = 400x240 4:3 = 400x300.

    thumbsize=               Takes numbers separated by 'x' : 320x180
                               width then height of video player in your HTML.
                               Default is set to 16:9 = 120x72 4:3 = 120x90.

    widescreen=              Takes true, True or false, False.

    threads=                 Takes number. The number of threads or CPU to use.
                               Defaults to all but 1 of your CPU's.

    setsize=                 Takes numbers separated by 'x' : 1920x1080
                               width then height of video. If set the following
                               options are required.

    bitrate=                 Takes number. The bitrate of the video. Without
                               setsize this will be ignored.

    maxbit=                  Takes number. The maximum bitrate for the video.
                               Without setsize this will be ignored.

    buffersize=              Takes number. The buffersize for the video.
                               Without setsize this will be ignored.


Notes:
  ** These are likely to change quite a bit as there is much testing to be done. **
  Default 16:9 Screen - size, bitrate, maxbit, buffersize:
    {'width': 1920, 'height': 1080, 'bit_rate': 2200000, 'max_bit': 2400000, 'buffer_size': 4800000},
    {'width': 1280, 'height': 720, 'bit_rate': 1800000, 'max_bit': 2000000, 'buffer_size': 4000000},
    {'width': 720, 'height': 406, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
    {'width': 480, 'height': 270, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
    {'width': 320, 'height': 180, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}

  Default 4:3 Screen - size, bitrate, maxbit, buffersize:
    {'width': 1440, 'height': 1080, 'bit_rate': 2200000, 'max_bit': 2400000, 'buffer_size': 4800000},
    {'width': 1024, 'height': 768, 'bit_rate':1800000, 'max_bit': 2000000, 'buffer_size': 4000000},
    {'width': 640, 'height': 480, 'bit_rate': 900000, 'max_bit': 1100000, 'buffer_size': 2200000},
    {'width': 400, 'height': 300, 'bit_rate': 500000, 'max_bit': 700000, 'buffer_size': 1400000},
    {'width': 272, 'height': 204, 'bit_rate': 300000, 'max_bit': 500000, 'buffer_size': 1000000}
  
  When setting your own sizes the width and height must be divisible by 2. The 
  bitrate has to be >= 30000. The maxbit must be > bitrate and the buffersize 
  should be > maxbit.

  This program does a two pass encoding for both formats. On mp4 it uses 
  qt-faststart to move all the meta data to the beginning for streaming.

  It also creates 5 video plaice holder images at different times so you
  have more choices of place holders. It does the same for thumbnails.

  Reading:
    DuckDuckGo FFmpeg and read everything you can.

    The commands used to write the videos were taken and adapted from:
    https://github.com/adexin-team/refinerycms-videojs/wiki/Encoding-files-to-.webm-(VP8)-and-.mp4-(h.264)-using-ffmpeg

  Tips:
    Make this script run from anywhere in the terminal:Bash, so you
      don't have to type /home/$USER/my/path/to/mkH5videos.py
      https://ubuntuforums.org/showthread.php?t=1074736

    Use the \\ to break up the arguments for better readability.
      $ mkH5videos.py ~/Desktop/myVid1.mp4 \\
      > ~/Videos/AwesomeVideos/myVid2.webm \\
      > widescreen=true setsize=1920x1080 \\
      > json=true html=true

    Make the json file and edit the settings before making the html and css
    as this program will read the json settings and set colors accordingly.
'''
        print h

if __name__ == '__main__':
    ## build *args and **kwargs from sys.argv
    lt, dt = [], {}
    # dt = {}
    for ar in argv[1:]:
        if '=' in ar:
            a = ar.split('=')
            dt[a[0]] = a[1]
        else:
            lt.append(ar)

    mkVideo(*lt, **dt)
