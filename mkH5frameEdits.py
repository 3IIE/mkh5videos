#!/usr/bin/python3.7
import sys
import psutil as ps
import time
import re
import subprocess

# from threading import Thread
# from Queue import Queue, Empty

import wx
import wx.xrc
import wx.richtext
from wx import html2 as h2

from bs4 import BeautifulSoup as BS

from mkH5video3 import mkH5Video, mkJS

def create(parent, args):
    return mkHframe(parent, args)

class mkHframe(wx.Frame):
    def __init__(self, parent, *args, **kwargs):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"mkH5videos", 
                          pos=wx.DefaultPosition, size=wx.Size(1024, 786),
                          style=wx.DEFAULT_FRAME_STYLE|wx.DOUBLE_BORDER|wx.TAB_TRAVERSAL, 
                          name=u"mkH5videos")

        if getattr(sys, 'frozen', False):
            # we are running in a |PyInstaller| bundle
            self.baseDir = sys._MEIPASS
            self.sDataDir = ps.os.getcwd()
        else:
            # we are running in a normal Python environment
            self.baseDir = ps.os.getcwd()
            self.sDataDir = ps.os.path.join(ps.os.getcwd(), 'mkH5videos')

        print(self.baseDir)
        winSize = self.Size.Get()

        self.serverOutPut = ''

        self.localUrlXre = re.compile(r'^http:\/\/localhost:[0-8]{4}\/?')
        self.webUrlXre = re.compile(r'[^http:\/\/www\.]|[^www\.]')
        self.localFileXre = re.compile(r'^file:\/\/')
        self.mkH5Xre = re.compile(r'^http:\/\/localhost:[0-8]{4}\/index\.html$')

        ## set color scheme
        spliterColorFG = wx.Colour(64, 64, 64)
        spliterColorBG = wx.Colour(128, 128, 128)

        panelColorFG = wx.Colour(255, 255, 255)
        panelColorBG = wx.Colour(64, 64, 64)

        notebookColorFG = wx.Colour(255, 255, 255)
        notebookColorBG = wx.Colour(88, 88, 88)

        menuColorFG = wx.Colour(255, 255, 255)
        nemuColorBG = wx.Colour(64, 64, 64)

        browserSize = wx.Size(winSize[0] - 300, winSize[1] * .75)
        consoleSize = wx.Size(winSize[0], winSize[1] * .25)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.SetExtraStyle(wx.FRAME_EX_METAL)
        self.SetBackgroundColour(wx.Colour(64, 64, 64))

        # ----------- Make Main Menue Bar and Menue Items ----------- #

        self.main_menubar = wx.MenuBar(0)
        self.main_menubar.SetForegroundColour(menuColorFG)
        self.main_menubar.SetBackgroundColour(nemuColorBG)

        self.file_menu = wx.Menu()

        self.openFile_menuItem = wx.MenuItem(self.file_menu,
                                             wx.ID_ANY,
                                             u"Open File",
                                             wx.EmptyString,
                                             wx.ITEM_NORMAL)

        self.file_menu.Append(self.openFile_menuItem)

        self.openProject_menuItem = wx.MenuItem(self.file_menu,
                                                wx.ID_ANY,
                                                u"Open mkH5 Project",
                                                wx.EmptyString,
                                                wx.ITEM_NORMAL)

        self.file_menu.Append(self.openProject_menuItem)

        self.file_menu.AppendSeparator()

        self.saveStyle_menuItem = wx.MenuItem(self.file_menu,
                                              wx.ID_ANY,
                                              u"Save Player Style",
                                              wx.EmptyString,
                                              wx.ITEM_NORMAL)

        self.file_menu.Append(self.saveStyle_menuItem)

        self.saveProject_menuItem = wx.MenuItem(self.file_menu,
                                                wx.ID_ANY,
                                                u"Save mkH5 Project",
                                                wx.EmptyString,
                                                wx.ITEM_NORMAL)

        self.file_menu.Append(self.saveProject_menuItem)

        self.file_menu.AppendSeparator()

        self.exit_menuItem = wx.MenuItem(self.file_menu,
                                         wx.ID_ANY,
                                         u"Exit",
                                         wx.EmptyString,
                                         wx.ITEM_NORMAL)

        self.file_menu.Append(self.exit_menuItem)

        self.main_menubar.Append(self.file_menu, u"File")

        self.help_menu = wx.Menu()

        self.about_menuItem = wx.MenuItem(self.help_menu,
                                          wx.ID_ANY,
                                          u"About mkH5videos",
                                          wx.EmptyString,
                                          wx.ITEM_NORMAL)

        self.help_menu.Append(self.about_menuItem)

        self.main_menubar.Append(self.help_menu, u"Help")

        self.SetMenuBar(self.main_menubar)

        # ___________ END Main Menue Bar and Menue Items ___________ #


        mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.topBottom_splitter = wx.SplitterWindow(self,
                                                    wx.ID_ANY,
                                                    wx.DefaultPosition,
                                                    wx.DefaultSize,
                                                    wx.SP_3D)

        self.topBottom_splitter.Bind(wx.EVT_IDLE, self.topBottom_splitterOnIdle)

        self.topBottom_splitter.Bind(wx.EVT_SPLITTER_SASH_POS_CHANGED,
                                     self.leftRightSplitterChanged)

        # self.topBottom_splitter.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
        self.topBottom_splitter.SetForegroundColour(spliterColorFG)
        self.topBottom_splitter.SetBackgroundColour(spliterColorBG)

        ## ==============** Start Top Panel **============== ##

        self.top_panel = wx.Panel(self.topBottom_splitter,
                                  wx.ID_ANY, wx.DefaultPosition,
                                  wx.DefaultSize,
                                  wx.TAB_TRAVERSAL)

        self.top_panel.SetBackgroundColour(panelColorBG)

        topSizer = wx.BoxSizer(wx.VERTICAL)

        self.leftRight_splitter = wx.SplitterWindow(self.top_panel, wx.ID_ANY, 
                                                    wx.DefaultPosition, wx.DefaultSize,
                                                    wx.SP_3D|wx.SP_3DBORDER)

        self.leftRight_splitter.Bind(wx.EVT_IDLE, self.leftRight_splitterOnIdle)

        self.leftRight_splitter.SetForegroundColour(spliterColorFG)
        self.leftRight_splitter.SetBackgroundColour(spliterColorBG)

        ## ----------** Start Browser Panel **-------------- ##

        self.browser_panel = wx.Panel(self.leftRight_splitter, wx.ID_ANY,
                                      wx.DefaultPosition, wx.Size(winSize[0] - 300, winSize[0] - 250),
                                      wx.TAB_TRAVERSAL)

        self.browser_panel.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
        self.browser_panel.SetBackgroundColour(panelColorBG)

        browserSizer = wx.BoxSizer(wx.VERTICAL)

        # ----------- Start Browser Toolbar ----------- #

        self.BrowserToolBar = wx.ToolBar(self.browser_panel, wx.ID_ANY,
                                         wx.DefaultPosition, wx.DefaultSize,
                                         wx.TB_HORIZONTAL)

        # self.back_Btn_Image = wx.Image()
        # wx.Image.AddHandler(wx.XPMHandler())
        # self.back_Btn_Image.LoadFile('/home/my3al/Desktop/winShare/btnSvgs/scrolArow.svg')
        # # self.back_Btn_Image.LoadFile('scrolArro.svg')
        # self.back_Btn_Image = wx.Image(u"/home/my3al/Desktop/winShare/weatherShow/sunnyHot.jpg", wx.BITMAP_TYPE_ANY).ConvertToBitmap()

        imgPath = ps.os.path.join(self.baseDir, 'btnSvgs', 'scrolArow.svg')
        print(imgPath)
        if not ps.os.path.exists(imgPath):
            print('not exist')
        self.back_Btn_Image = wx.Bitmap(imgPath, type=wx.BITMAP_TYPE_XPM_DATA)

        # self.back_Btn_Image = wx.Bitmap(ps.os.path.join(u"/home/my3al/Desktop/winShare/btnSvgs/scrolArow.svg"), type=wx.BITMAP_TYPE_XPM_DATA)
        # self.back_Btn_Image.Scale(100, 100, wx.IMAGE_QUALITY_HIGH)

        self.back_Btn_Image.SetSize(wx.Size(100, 100))

        self.Back_btn = self.BrowserToolBar.AddTool(wx.ID_ANY, u"Back", 
                                                    self.back_Btn_Image, self.back_Btn_Image, 
                                                    wx.ITEM_NORMAL, u"Back a Page", wx.EmptyString, 
                                                    None)

        self.Forward_btn = self.BrowserToolBar.AddTool(wx.ID_ANY, u"Forward",
                                                       self.back_Btn_Image, self.back_Btn_Image,
                                                       wx.ITEM_NORMAL, u"Forward a Page", wx.EmptyString,
                                                       None)

        self.URL_Location_Bar = wx.TextCtrl(self.BrowserToolBar, wx.ID_ANY,
                                            u"http://localhost:8088/index.html", wx.DefaultPosition,
                                            wx.DefaultSize,
                                            wx.TE_LEFT|wx.ALWAYS_SHOW_SB|wx.SUNKEN_BORDER|wx.TE_PROCESS_ENTER)

        print(self.browser_panel.GetSize()[0])
        self.URL_Location_Bar.SetSize(wx.Size(self.browser_panel.GetSize()[0] - (self.back_Btn_Image.GetSize()[0] * 3 + 15), -1))

        self.BrowserToolBar.AddControl(self.URL_Location_Bar)
        self.BrowserToolBar.Realize()

        # ___________ END Browser Toolbar ___________ #


        # ----------- Start Browser Tabs ------------ #

        browserSizer.Add(self.BrowserToolBar, 0, wx.EXPAND, 5)
        ## make notebook
        self.browser_notebook = wx.Notebook(self.browser_panel, wx.ID_ANY,
                                            wx.DefaultPosition, wx.DefaultSize, 0)

        self.browser_notebook.SetForegroundColour(notebookColorFG)
        self.browser_notebook.SetBackgroundColour(notebookColorBG)

        ## initial page. Web Localhost
        self.Web = wx.Panel(self.browser_notebook, wx.ID_ANY,
                            wx.DefaultPosition, wx.DefaultSize,
                            wx.FULL_REPAINT_ON_RESIZE|wx.TAB_TRAVERSAL)

        self.Web.SetForegroundColour(panelColorFG)
        self.Web.SetBackgroundColour(panelColorBG)

        webSizer = wx.BoxSizer(wx.VERTICAL)

        # ----------- Start the Server to serve http://localhost:8088 ----------- #

        # self.sDataDir = ps.os.path.join(self.sDataDir, 'mkH5videos')
        print(self.sDataDir)
        ps.os.chdir(self.sDataDir)

        if '-s' in args:

            self.homePage = 'http://localhost:8080/mkH5videos.html'

            self.sServer = mkServer(serverDir=self.sDataDir)

            self.sServer.start()

        else:
            self.homePage = ps.os.path.join(self.sDataDir, 'mkH5videos.html')
            self.homePage = 'file://' + self.homePage


        self.htmlWin = h2.WebView.New(self.Web, wx.ID_ANY,
                                      self.homePage, wx.DefaultPosition,
                                      wx.Size(self.browser_panel.GetSize()[0],
                                      self.browser_panel.GetSize()[1]))

        self.htmlWin.SetZoom(h2.WEBVIEW_ZOOM_MEDIUM)
        self.htmlWin.EnableHistory(enable=True)
        self.clickTag = self.htmlWin.GetSelectedSource()

        webSizer.Add(self.htmlWin, 0, wx.ALL, 5)

        jsData = mkJS('', True)
        # print(f'here = {jsData}')
        self.jFile = jsData.replace('let ', 'var ')
        self.jFile = self.jFile.replace(u"'use strict';", '')

        self.URL_Location_Bar.SetValue(self.homePage)
        self.loadPage()

        self.Web.SetSizer(webSizer)
        self.Web.Layout()
        webSizer.Fit(self.Web)

        self.browser_notebook.AddPage(self.Web, u"mkH5videos", True)

        # ____________ END the Server to serve http://localhost:8088 ____________ #

        ##
        self.HTML_panel = wx.Panel(self.browser_notebook, wx.ID_ANY,
                                   wx.DefaultPosition, wx.DefaultSize,
                                   wx.FULL_REPAINT_ON_RESIZE|wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)

        self.HTML_panel.SetForegroundColour(panelColorFG)
        self.HTML_panel.SetBackgroundColour(panelColorBG)

        htmlSizer = wx.BoxSizer(wx.VERTICAL)

        self.html_richText = wx.richtext.RichTextCtrl(self.HTML_panel, wx.ID_ANY, wx.EmptyString,
                                                      wx.DefaultPosition, wx.DefaultSize,
                                                      wx.TE_READONLY|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS)

        self.html_richText.SetBackgroundColour(wx.Colour(64, 64, 64))

        htmlSizer.Add(self.html_richText, 1, wx.EXPAND |wx.ALL, 5)

        self.HTML_panel.SetSizer(htmlSizer)
        self.HTML_panel.Layout()

        htmlSizer.Fit(self.HTML_panel)

        self.browser_notebook.AddPage(self.HTML_panel, u"HTML", False)

        browserSizer.Add(self.browser_notebook, 1, wx.EXPAND |wx.ALL, 5)

        self.browser_panel.SetSizer(browserSizer)
        self.browser_panel.Layout()
        browserSizer.Fit(self.browser_panel)

        ## ______________** END Browser Panel **______________ ##


        ## --------------** Start Settings Panel **--------------- ##

        self.right_panel = wx.Panel(self.leftRight_splitter, wx.ID_ANY,
                                    wx.DefaultPosition, wx.DefaultSize,
                                    wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)

        self.right_panel.SetBackgroundColour(panelColorBG)

        self.leftRight_splitter.SplitVertically(self.browser_panel, self.right_panel, 710)
        topSizer.Add(self.leftRight_splitter, 1, wx.EXPAND, 5)

        ## ______________** END Settings Panel **______________ ##


        self.top_panel.SetSizer(topSizer)
        self.top_panel.Layout()
        topSizer.Fit(self.top_panel)

        ## ==============** END Top Panel **============== ##


        ## ==============** Start Bottom Panel **============== ##

        self.bottom_panel = wx.Panel(self.topBottom_splitter, wx.ID_ANY, 
                                     wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)

        self.bottom_panel.SetBackgroundColour(panelColorBG)

        boittomSizer = wx.BoxSizer(wx.VERTICAL)

        self.encoderConsole_notebook = wx.Notebook(self.bottom_panel, wx.ID_ANY,
                                                   wx.DefaultPosition, wx.DefaultSize, 0)

        self.encoderConsole_notebook.SetForegroundColour(notebookColorFG)
        self.encoderConsole_notebook.SetBackgroundColour(notebookColorBG)

        self.encoder_panel = wx.Panel(self.encoderConsole_notebook, wx.ID_ANY,
                                      wx.DefaultPosition, wx.DefaultSize,
                                      wx.FULL_REPAINT_ON_RESIZE|wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)

        self.encoder_panel.SetBackgroundColour(panelColorBG)

        self.encoderConsole_notebook.AddPage(self.encoder_panel, u"Video Encoder", False)

        ## ------------- ** Start Console  ** -------------- ##

        self.console_panel = wx.Panel(self.encoderConsole_notebook, wx.ID_ANY,
                                      wx.DefaultPosition, wx.DefaultSize,
                                      wx.FULL_REPAINT_ON_RESIZE|wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL)

        self.console_panel.SetBackgroundColour(panelColorBG)

        consoleSizer = wx.BoxSizer(wx.VERTICAL)

        self.console_textCtrl = wx.TextCtrl(self.console_panel, wx.ID_ANY, 
                                            wx.EmptyString, wx.DefaultPosition, 
                                            wx.Size(winSize[0], self.bottom_panel.GetSize()[1]),
                                            wx.TE_LEFT|wx.TE_MULTILINE|wx.TE_NO_VSCROLL|wx.TE_READONLY|wx.TE_WORDWRAP|wx.FULL_REPAINT_ON_RESIZE)

        # if '-s' in args:
        #     console_txt = self.sServer.stdout.read()
        #     self.console_textCtrl.SetValue(console_txt)
        # else:
        #     self.console_textCtrl.SetValue('Server not running.')

        consoleSizer.Add(self.console_textCtrl, 0, wx.ALL, 5)


        self.console_panel.SetSizer(consoleSizer)
        self.console_panel.Layout()

        consoleSizer.Fit(self.console_panel)

        ## ______________ END Console ______________ ##

        self.encoderConsole_notebook.AddPage(self.console_panel, u"Console", False)

        boittomSizer.Add(self.encoderConsole_notebook, 1, wx.EXPAND |wx.ALL, 5)


        self.bottom_panel.SetSizer(boittomSizer)
        self.bottom_panel.Layout()
        boittomSizer.Fit(self.bottom_panel)

        ## ============== END Bottom Panel ============== ##

        self.topBottom_splitter.SplitHorizontally(self.top_panel, self.bottom_panel, 604)
        mainSizer.Add(self.topBottom_splitter, 1, wx.EXPAND, 5)


        self.SetSizer(mainSizer)
        self.Layout()
        self.mkH5_statusBar = self.CreateStatusBar(2, wx.ALWAYS_SHOW_SB, wx.ID_ANY)
        self.mkH5_statusBar.SetBackgroundColour(wx.Colour(64, 64, 64))


        # ----------- Start Content Events ----------- #

        self.Bind(wx.EVT_SIZE, self.mainFrameOnSize)

        self.Bind(wx.EVT_MENU, self.openOptOnMenuSelection, id=self.openFile_menuItem.GetId())

        self.Bind(wx.EVT_MENU, self.SaveOptOnMenuSelection, id=self.saveStyle_menuItem.GetId())

        self.Bind(wx.EVT_MENU, self.ExitOptOnMenuSelection, id=self.exit_menuItem.GetId())

        self.Bind(wx.EVT_TOOL, self.Back_btnOnToolClicked, id=self.Back_btn.GetId())

        self.Bind(wx.EVT_TOOL, self.Forward_btnOnToolClicked, id=self.Forward_btn.GetId())

        self.URL_Location_Bar.Bind(wx.EVT_TEXT_ENTER, self.URL_Location_BarOnKeyDown)
        # ____________ END Content Events ____________ #


        self.Centre(wx.BOTH)



    def __del__(self):
        pass

    def topBottom_splitterOnIdle(self, event):
        self.topBottom_splitter.SetSashPosition(604)
        self.topBottom_splitter.Unbind(wx.EVT_IDLE)

    def leftRight_splitterOnIdle(self, event):
        self.leftRight_splitter.SetSashPosition(710)
        self.leftRight_splitter.Unbind(wx.EVT_IDLE)

    def leftRightSplitterChanged(self, event):
        print(event.GetSashPosition())
        self.htmlWin.SetSize(event.GetSashPosition(), self.htmlWin.GetSize()[1])
        self.browser_panel.SetSize(event.GetSashPosition(), self.htmlWin.GetSize()[1])


    # ----------- Event Functions ----------- #

    def mainFrameOnSize(self, event):
        pass
        # self.htmlWin.SetSize(event.Size.Get()[0] - 300, event.Size.Get()[1] - 250)

    def openOptOnMenuSelection(self, event):
        event.Skip()

    def SaveOptOnMenuSelection(self, event):
        event.Skip()

    def ExitOptOnMenuSelection(self, event):
        if '-s' not in args:
            sys.exit()
        
        proc = self.sServer.stop()

        print(proc)

        if isinstance(proc, list):
            print(proc)
        else:
            sys.exit()


    def Back_btnOnToolClicked(self, event):
        if self.htmlWin.CanGoBack():
            self.htmlWin.GoBack()

    def Forward_btnOnToolClicked(self, event):
        if self.htmlWin.CanGoForward():
            self.htmlWin.GoForward()

    def URL_Location_BarOnKeyDown(self, event):
        # print(event)
        self.loadPage()

    def onSoupLoad(self, event, retScrpt):
        self.htmlWin.RunScript(self.jFile)
        self.htmlWin.RunScript(retScrpt)
        self.htmlWin.Reload()
        # print(self.htmlWin.GetPageSource())
        self.Bind(h2.EVT_WEBVIEW_LOADED, None, self.htmlWin)
        self.Bind(h2.EVT_WEBVIEW_ERROR, None, self.htmlWin)

    def onSoupError(self, event):
        print('ERROR: onSoupLoadError was raised for -> \n{}\n'.format(event))
        self.Bind(h2.EVT_WEBVIEW_ERROR, None, self.htmlWin)

    def onPageLoad(self, event):
        # self.htmlWin.RunScript(self.jFile)
        # self.htmlWin.RunScript(retScrpt)
        self.Bind(h2.EVT_WEBVIEW_LOADED, None, self.htmlWin)

    # ___________ END Event Functions ___________ #


    # ----------- Start Web Helper Functions ----------- #

    def ioWatcher(self, identifier, stream):
        print(identifier)
        print(stream)

        # oFile = open(stream, 'r')

        for line in stream:
            # print('here is the line: {}'.format(line))
            self.ioHandler.put((identifier, line))
            print(line)

        if not stream.closed:
            stream.close()

        print('here is what we want')

    def printer(self):
        while True:
            try:
                # Block for 1 second.
                item = self.ioHandler.get(True, 1)
                print('try: <-------- ! {}'.format(*item))
            except Empty:
                # No output in either streams for a second. Are we done?
                print('except: <-------- !')
                if self.sServer.poll() is not None:
                    print('ohSnap; ahhNaBruh_uDonFuctUPnow!')
                    break
            else:
                print(list(item))
                if item != None:
                    identifier, line = item
                    if identifier == str(identifier) and identifier != '' and line == str(line) and line != '':
                        self.serverOutPut = '{}{}{}: {}'.format(self.serverOutPut, '\n', identifier, line)
                        print(self.serverOutPut)
                        self.console_textCtrl.SetValue(self.serverOutPut)
                        print('try ^^^ Harder!')


    def loadPage(self):
        ## this function helps to set the correct page load functionality
        if self.mkH5Xre.search(self.URL_Location_Bar.GetLineText(0)):
            ## soup creates a bs4 html object and loads in current directory as index.html
            print('Mixing.')
            vidHtmlPath = ps.os.path.join(self.sDataDir, 'mkH5videos.html')
            vidHtml, rScrpt = self.getSoup(vidHtmlPath)
            vidHtml = str(vidHtml.prettify())
            self.htmlWin.SetPage(vidHtml, 'http://localhost:8088/index.html')
            self.URL_Location_Bar.SetValue('http://localhost:8088/index.html')
            self.Bind(h2.EVT_WEBVIEW_LOADED, lambda event: self.onSoupLoad(event, rScrpt), self.htmlWin)
            self.Bind(h2.EVT_WEBVIEW_ERROR, self.onSoupError, self.htmlWin)

        elif self.localUrlXre.search(self.URL_Location_Bar.GetLineText(0)):
            ## localURL loads a url relative to the currrent directory
            ## for use with the sServer http://localhost:8088
            self.htmlWin.LoadURL(self.URL_Location_Bar.GetLineText(0))
            self.URL_Location_Bar.SetValue(self.URL_Location_Bar.GetLineText(0))
            self.Bind(h2.EVT_WEBVIEW_LOADED, lambda event: self.onSoupLoad(event, 'rScrpt'), self.htmlWin)

        elif self.webUrlXre.search(self.URL_Location_Bar.GetLineText(0)):
            ## web loads from an external server location (* Basic Web Function *)
            self.htmlWin.LoadURL(self.URL_Location_Bar.GetLineText(0))
            self.URL_Location_Bar.SetValue(self.URL_Location_Bar.GetLineText(0))

        elif self.localFileXre.search(self.URL_Location_Bar.GetLineText(0)):
            ## loads a file from local destination nelative or not to this document
            ## without Simple Server opperation (* Static Local File Location *)
            self.htmlWin.LoadURL(self.URL_Location_Bar.GetLineText(0))
            self.URL_Location_Bar.SetValue(self.htmlWin.GetCurrentURL())
            self.Bind(h2.EVT_WEBVIEW_LOADED, self.onPageLoad, self.htmlWin)


    def getSoup(self, url):
        if ps.os.path.exists(str(url)):
            iOpen = open(url, 'r')
            iFile = iOpen.read()
            iOpen.close()
            regX = re.compile(r'window.*\{\s(.*)\s\}\);', re.DOTALL)
            # remove comments from JS file regex
            regX2 = re.compile(r'\s//.*')
            # remove spaces if greater than 2 and all tab and newline
            regX3 = re.compile(r'[ ]{2,20}|[\t|\n]')
            mkh5 = BS(iFile, 'lxml')

            for scrpt in mkh5.find_all('script'):
                if scrpt.has_attr('src'):
                    if 'mkH5videos.js' in scrpt['src']:
                        scrpt.extract()
                else:
                    retScr = re.findall(regX, scrpt.text)
                    retScrip = re.sub(regX2, '', retScr[0])
                    retScrip = re.sub(regX3, '', retScrip)
                    scrpt.extract()

            return (mkh5, retScrip)
        else:
            sys.exit()
            return False
    # ____________ End Web Helper Functions ____________ #


### Supporting Classes

class mkServer(object):
    def __init__(self, *args, **kargs):
        self.pid = None
        if 'serverDir' in kargs.keys():
            self.serverDir = ps.os.path.split(kargs['serverDir'])[0]
        else:
            sys.exit()


    def start(self):
        print(f"server directory = {ps.os.path.join(self.serverDir, 'mkH5videos.html')}")

        with open(ps.os.devnull, 'r+b', 0) as DEVNULL:
            p = ps.subprocess.Popen(['php', '-S', 'localhost:8066', '-t', str(self.serverDir)], bufsize=-1, stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL, close_fds=True, shell=True)

        # with open(ps.os.devnull, 'r+b', 0) as DEVNULL:
        #     p = ps.subprocess.Popen(' '.join(['php', '-m', 'SimpleHTTPServer', '8066', str(self.serverDir)]), bufsize=-1, stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL, close_fds=True, shell=True)

        time.sleep(3)
        self.pid = p.pid
        p.kill()

        print(self.pid)

    def getPid(self):
        proc = chkProc(['python'])


    def stop(self):
        proc = chkProc(['python'], kill=['python', '-m', 'SimpleHTTPServer', '8066', str(self.serverDir)])
        return proc


### Supporting Functions

def chkProc(proc=[], kill=False):
    q = [x for x in ps.process_iter() if x.name() in proc or x.pid in proc]
    if isinstance(kill, [list, str]) and not q == []:
        for p in q:
            if isinstance(kill, str):
                kill = kill.split(' ')
            if kill == p.cmdline():
                print(p.cmdline())
                p.kill()
        time.sleep(3)
        q = [x for x in ps.process_iter() if x.name() in proc or x.pid in proc]
        return True if q == [] else q
    return q


def mkArgs(dlArgs):
    if len(dlArgs) > 0:
        lt, dt = [], {}
        for ar in dlArgs[1:]:
            if '=' in ar:
                a = ar.split('=')
                dt[a[0]] = a[1]
            else:
                lt.append(ar)
        return (lt, dt)
    else:
        return None


if __name__ == '__main__':

    app = wx.App()
    args = mkArgs(sys.argv)
    if args is None:
        frame = mkHframe(None)
    else:
        frame = mkHframe(None, *args[0], **args[1])
    frame.Show()
    app.MainLoop()

