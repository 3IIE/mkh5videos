import sys
import os
import re
import subprocess
import wx
import wx.xrc
from wx import html2 as h2

from bs4 import BeautifulSoup as BS

import mkH5videos

def create(parent):
    return mainFrame(parent)

class mainFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(1024, 768), style=wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL)

        if getattr(sys, u'frozen', False):
            # we are running in a |PyInstaller| bundle
            self.baseDir = sys._MEIPASS
            self.sDataDir = os.getcwd()
        else:
            # we are running in a normal Python environment
            self.baseDir = os.getcwd()
            self.sDataDir = os.getcwd()

        print self.baseDir
        winSize = self.Size.Get()
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        self.localUrlXre = re.compile(r'^^http:\/\/localhost:[0-9]{4}\/?')
        self.webUrlXre = re.compile(r'[^http:\/\/www\.]|[^www\.]')
        self.localFileXre = re.compile(r'^file:\\\\')
        self.mkH5Xre = re.compile(r'^http:\/\/localhost:[0-8]{4}\/(index|mkH5videos)\.html?$')

        # ----------- Make Main Menue Bar and Menue Items ----------- #

        self.MainMenuBar = wx.MenuBar(0)

        if sys.platform != 'darwin':
            ## OSX does not like MenueBar size assignment.
            self.MainMenuBar.SetClientSize(winSize)

        self.fileMenu = wx.Menu()

        self.openOpt = wx.MenuItem(self.fileMenu, wx.ID_ANY, u"Open", wx.EmptyString, wx.ITEM_NORMAL)
        self.fileMenu.Append(self.openOpt)

        self.SaveOpt = wx.MenuItem(self.fileMenu, wx.ID_ANY, u"Save", wx.EmptyString, wx.ITEM_NORMAL)
        self.fileMenu.Append(self.SaveOpt)

        self.fileMenu.AppendSeparator()

        self.ExitOpt = wx.MenuItem(self.fileMenu, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL)
        self.fileMenu.Append(self.ExitOpt)

        self.MainMenuBar.Append(self.fileMenu, u"File")

        self.editMenue = wx.Menu()
        self.MainMenuBar.Append(self.editMenue, u"Edit")

        self.SetMenuBar(self.MainMenuBar)
        # ___________ END Main Menue Bar and Menue Items ___________ #


        # ----------- Start Browser Toolbar ----------- #

        self.BrowserToolBar = self.CreateToolBar(wx.TB_HORIZONTAL, wx.ID_ANY)

        # self.back_Btn_Image = wx.Image()
        # wx.Image.AddHandler(wx.XPMHandler())
        # self.back_Btn_Image.LoadFile(u'/home/my3al/Desktop/winShare/btnSvgs/scrolArow.svg')
        # # self.back_Btn_Image.LoadFile(u'scrolArro.svg')
        # self.back_Btn_Image = wx.Image(u"/home/my3al/Desktop/winShare/weatherShow/sunnyHot.jpg", wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.back_Btn_Image = wx.Bitmap(os.path.join(u"/home/my3al/Desktop/winShare/btnSvgs/scrolArow.svg"), type=wx.BITMAP_TYPE_XPM_DATA)
        # self.back_Btn_Image.Scale(100, 100, wx.IMAGE_QUALITY_HIGH)
        # self.back_Btn_Image.SetSize((100, 100))


        self.Back_btn = self.BrowserToolBar.AddTool(wx.ID_ANY, u"Back", self.back_Btn_Image, self.back_Btn_Image, wx.ITEM_NORMAL, u"Back a Page", wx.EmptyString, None)

        self.Forward_btn = self.BrowserToolBar.AddTool(wx.ID_ANY, u"Forward", self.back_Btn_Image, self.back_Btn_Image, wx.ITEM_NORMAL, u"Forward a Page", wx.EmptyString, None)

        self.URL_Location_Bar = wx.TextCtrl(self.BrowserToolBar, wx.ID_ANY, u"http://localhost:8088/index.html", wx.DefaultPosition, wx.DefaultSize, wx.TE_LEFT|wx.ALWAYS_SHOW_SB|wx.SUNKEN_BORDER|wx.TE_PROCESS_ENTER)

        self.URL_Location_Bar.SetMaxLength(500)
        self.URL_Location_Bar.SetSize((winSize[0] - 150, -1))

        self.BrowserToolBar.AddControl(self.URL_Location_Bar)
        self.BrowserToolBar.Realize()

        htmlSizer = wx.BoxSizer(wx.VERTICAL)
        # ___________ END Browser Toolbar ___________ #


        # ----------- Start the Server to serve http://localhost:8088 ----------- #

        self.sDataDir = os.path.join(self.sDataDir, u'mkH5videos')
        print self.sDataDir

        os.chdir(self.sDataDir)


        # self.sServer = subprocess.Popen(u'python -m SimpleHTTPServer 8088', stderr=subprocess.STDOUT, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
        self.sServer = subprocess.Popen(u'python -m SimpleHTTPServer 8088', shell=True)

        # print self.sServer.communicate()[0]

        self.htmlWin = h2.WebView.New(self, wx.ID_ANY, u'', wx.DefaultPosition, winSize)

        htmlSizer.Add(self.htmlWin, 0, wx.ALL, 5)

        jsData = mkH5videos.mkVideo(getJS=True)
        self.jFile = str(jsData.jsFile).replace(u'let ', u'var ')
        self.jFile = self.jFile.replace(u"'use strict';", '')

        self.loadPage()

        self.htmlWin.SetZoom(h2.WEBVIEW_ZOOM_MEDIUM)

        self.SetSizer(htmlSizer)
        # ____________ END the Server to serve http://localhost:8088 ____________ #


        # ------------ Start Frame Positions ------------ #

        self.Layout()
        self.Centre(wx.BOTH)
        # ______________ End Frame Position _____________ #


        # ----------- Start Content Events ----------- #

        self.Bind(wx.EVT_SIZE, self.mainFrameOnSize)
        self.Bind(wx.EVT_MENU, self.openOptOnMenuSelection, id=self.openOpt.GetId())
        self.Bind(wx.EVT_MENU, self.SaveOptOnMenuSelection, id=self.SaveOpt.GetId())
        self.Bind(wx.EVT_MENU, self.ExitOptOnMenuSelection, id=self.ExitOpt.GetId())
        self.Bind(wx.EVT_TOOL, self.Back_btnOnToolClicked, id=self.Back_btn.GetId())
        self.Bind(wx.EVT_TOOL, self.Forward_btnOnToolClicked, id=self.Forward_btn.GetId())
        self.URL_Location_Bar.Bind(wx.EVT_TEXT_ENTER, self.URL_Location_BarOnKeyDown)
        # ____________ END Content Events ____________ #


    def __del__(self):
        pass


    # ----------- Event Functions ----------- #

    def mainFrameOnSize(self, event):
        self.htmlWin.SetSize(event.Size.Get())

    def openOptOnMenuSelection(self, event):
        event.Skip()

    def SaveOptOnMenuSelection(self, event):
        event.Skip()

    def ExitOptOnMenuSelection(self, event):
        # self.sServer.communicate()
        self.sServer.kill()
        self.sServer.wait()
        # print self.sServer.poll()
        # self.sServer.wait()
        while True:
            if self.sServer.poll() is None:
                print 'Poll: {}'.foramt(self.sServer.poll())
                self.sServer.kill()
            else:
                sys.exit()

    def Back_btnOnToolClicked(self, event):
        event.Skip()

    def Forward_btnOnToolClicked(self, event):
        event.Skip()

    def URL_Location_BarOnKeyDown(self, event):
        print event
        self.loadPage()

    def onSoupLoad(self, event, retScrpt):
        self.htmlWin.RunScript(self.jFile)
        self.htmlWin.RunScript(retScrpt)
        # print self.htmlWin.GetPageSource()
        self.Bind(h2.EVT_WEBVIEW_LOADED, None, self.htmlWin)
        self.Bind(h2.EVT_WEBVIEW_ERROR, None, self.htmlWin)

    def onSoupError(self, event):
        print 'ERROR: onSoupLoadError was raised for -> {}'.format(event)
        self.Bind(h2.EVT_WEBVIEW_ERROR, None, self.htmlWin)

    def onPageLoad(self, event):
        # self.htmlWin.RunScript(self.jFile)
        # self.htmlWin.RunScript(retScrpt)
        self.Bind(h2.EVT_WEBVIEW_LOADED, None, self.htmlWin)

    # ___________ END Event Functions ___________ #


    # ----------- Start Web Helper Functions ----------- #

    def loadPage(self):
        ## this function helps to set the correct page load functionality
        if self.mkH5Xre.search(self.URL_Location_Bar.GetLineText(0)):
            ## soup creates a bs4 html object and loads in current directory as index.html
            print 'Mixing.'
            vidHtmlPath = os.path.join(self.sDataDir, u'mkH5videos.html')
            vidHtml, rScrpt = self.getSoup(vidHtmlPath)
            vidHtml = str(vidHtml.prettify())
            self.htmlWin.SetPage(vidHtml, u'http://localhost:8088/index.html')
            self.URL_Location_Bar.SetValue(u'http://localhost:8088/index.html')
            self.Bind(h2.EVT_WEBVIEW_LOADED, lambda event: self.onSoupLoad(event, rScrpt), self.htmlWin)
            self.Bind(h2.EVT_WEBVIEW_ERROR, self.onSoupError, self.htmlWin)
        elif self.localUrlXre.search(self.URL_Location_Bar.GetLineText(0)):
            ## localURL loads a url relative to the currrent directory
            ## for use with the sServer http://localhost:8088
            self.htmlWin.LoadURL(self.URL_Location_Bar.GetLineText(0))
            self.URL_Location_Bar.SetValue(self.URL_Location_Bar.GetLineText(0))
            self.Bind(h2.EVT_WEBVIEW_LOADED, lambda event: self.onSoupLoad(event, 'rScrpt'), self.htmlWin)
        elif self.webUrlXre.search(self.URL_Location_Bar.GetLineText(0)):
            ## web loads from an external server location (* Basic Web Function *)
            self.htmlWin.LoadURL(self.URL_Location_Bar.GetLineText(0))
            self.URL_Location_Bar.SetValue(self.URL_Location_Bar.GetLineText(0))
        elif self.webUrlXre.search(self.URL_Location_Bar.GetLineText(0)):
            ## loads a file from local destination nelative or not to this document
            ## without Simple Server opperation (* Static Local File Location *)
            self.htmlWin.LoadURL(self.URL_Location_Bar.GetLineText(0))
            self.URL_Location_Bar.SetValue(self.htmlWin.GetCurrentURL())
            self.Bind(h2.EVT_WEBVIEW_LOADED, self.onPageLoad, self.htmlWin)

    def getSoup(self, url):
        if os.path.exists(str(url)):
            iOpen = open(url, 'r')
            iFile = iOpen.read()
            iOpen.close()
            regX = re.compile(r'window.*\{\s(.*)\s\}\);', re.DOTALL)
            mkh5 = BS(iFile, 'lxml')

            for scrpt in mkh5.find_all('script'):
                if scrpt.has_attr('src'):
                    if 'mkH5videos.js' in scrpt['src']:
                        scrpt.extract()
                else:
                    retScr = re.findall(regX, scrpt.text)
                    retScrip = re.sub(r'\s//.*', '', retScr[0])
                    retScrip = re.sub(r'[ ]{2,20}|[\t|\n]', '', retScrip)
                    scrpt.extract()


            return (mkh5, retScrip)
        else:
            return False

    # ____________ End Web Helper Functions ____________ #


if __name__ == '__main__':
    app = wx.App()
    frame = create(None)
    frame.Show()
    app.MainLoop()
