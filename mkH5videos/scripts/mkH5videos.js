/*jshint esversion: 6 */
// for best minification.
// npm install -g babel-minify --save-dev
// # bash command to minify our javascript
// $ minify --mangle --simplify --evaluate --deadcode scripts/mkH5videos.js > scripts/mkH5videos.min.js
'use strict';
if(!window.gsap){
    let script2 = document.createElement("script");
    script2.type = "text/javascript";
    script2.src = "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js";
    document.getElementsByTagName("head")[0].appendChild(script2);
}

function mkH5video(player, video, configFile, setCtrl, verbose){
    // gsap.defaults({overwrite: "auto"});
    var loc, i;
    if(player === undefined || player === null || player === ""){
        player = document.getElementById('player');
    }
    if(video === undefined || video === null || video === ""){
        video = document.getElementsByTagName('video')[0];
    }
    if(configFile === undefined || configFile === null || configFile === ""){
        loc = 'scripts/mkH5videos.json';
    }else{
        loc = configFile;
    }
    if(verbose === undefined || verbose === null || verbose !== true){
        verbose = false;
    }

    var metaScale = document.querySelector('meta[name="viewport"]').content;

    var mouseIdel, nowPlay, play_bg, btnH, btnW, cTween, showSplash=false;

    var xmlns = 'http://www.w3.org/2000/svg';

    if(!video.controls){
        
        // ** -Start- Build user costomizations -Start- ** //

        // maybe add these again if we want to figure out some crazy button effects.
        // 'btnBgTopOver': '#404249', 'btnBgBottomOver': '#2C365C',
        if(setCtrl === undefined || setCtrl === null || Object.getPrototypeOf(setCtrl) !== Object.prototype){
            console.log('No settings.');
            setCtrl = {'topDir': '',
                       'videoOrder': false,
                       'playlist': false,
                       'uSetRez': 'SD',
                       'rezColorUp': '#FD5600',
                       'rezColorSelect': '#458CCB',
                       'rezColorOver': '#ffffff',
                       'shoCtrlOnStart': 'visible',
                       'buttonW': mkInt(player.style.height) * 0.10 + 10 + 'px',
                       'buttonH': mkInt(player.style.height) * 0.10 + 'px',
                       'btnColUp': '#458CCB',
                       'btnColOver': '#72ABDD',
                       'btnBgTop': '#040202',
                       'btnBgTopAlpha': 1.0,
                       'btnBgBottom': '#10100A',
                       'btnBgBottomAlpha': 1.0,
                       'nowPlayColor': '#91C7F6',
                       'bufferColor': '#FFFFFF',
                       'volOffset': 10,
                       'hideCtrlsTime': 3,
                       'progressFix': false};
        }else{
            console.log('settings');
            if(setCtrl.topDir === undefined || typeof setCtrl.topDir !== 'string'){
                setCtrl.topDir = '';
                mkLog('Setting-topDir ( * Default * )');
            }
            else{
                mkLog('Setting-topDir (Type expected, String:URL): '+setCtrl.topDir);
            }
            if(setCtrl.videoOrder === undefined || Object.prototype.toString.call(setCtrl.videoOrder) !== '[object Array]'){
                setCtrl.videoOrder = false;
                mkLog('Setting-videoOrder ( * Default * )');
            }else{
                mkLog('Setting-videoOrder (Type expected, Array): '+setCtrl.videoOrder);
            }
            if(setCtrl.playlist === undefined || Object.prototype.toString.call(setCtrl.playlist) !== '[object Array]'){
                setCtrl.playlist = false;
                mkLog('Setting-playlist ( * Default * )');
            }else{
                mkLog('Setting-playlist (Type expected, Array): '+setCtrl.playlist);
            }
            if(setCtrl.uSetRez === undefined || typeof setCtrl.uSetRez !== 'string'){
                setCtrl.uSetRez = 'SD';
                mkLog('Setting-uSetRez ( * Default * ):');
            }else{
                mkLog('Setting-uSetRez (Default video resolution, XD | HD | HQ | SD | LQ | M): '+setCtrl.uSetRez);
            }
            if(setCtrl.rezColorUp === undefined || typeof setCtrl.rezColorUp !== 'string'){
                setCtrl.rezColorUp = '#FD5600';
                mkLog('Setting-rezColorUp ( * Default * )');
            }else{
                mkLog('Setting-rezColorUp (Type expected, Hexadecimal Color String): '+setCtrl.rezColorUp);
            }
            if(setCtrl.rezColorSelect === undefined || typeof setCtrl.rezColorSelect !== 'string'){
                setCtrl.rezColorSelect = '#458CCB';
                mkLog('Setting-rezColorSelect ( * Default * )');
            }else{
                mkLog('Setting-rezColorSelect (Type expected, Hexadecimal Color String): '+setCtrl.rezColorSelect);
            }
            if(setCtrl.rezColorOver === undefined || typeof setCtrl.rezColorOver !== 'string'){
                setCtrl.rezColorOver = '#ffffff';
                mkLog('Setting-rezColorOver ( * Default * )');
            }else{
                mkLog('Setting-rezColorOver (Type expected, Hexadecimal Color String): '+setCtrl.rezColorOver);
            }
            if(setCtrl.shoCtrlOnStart === undefined || typeof setCtrl.shoCtrlOnStart !== 'string'){
                setCtrl.shoCtrlOnStart = 'visible';
                mkLog('Setting-shoCtrlOnStart ( * Default * )');
            }else{
                mkLog('Setting-shoCtrlOnStart (Type expected, hidden | visible): '+setCtrl.shoCtrlOnStart);
            }
            if(setCtrl.buttonH === undefined || typeof setCtrl.buttonH !== 'string'){
                setCtrl.buttonH = mkInt(player.style.height) * 0.10 + 'px';
                mkLog('Setting-buttonH ( * Default * )');
            }else{
                mkLog('Setting-buttonH (Type expected, String-number in pixels): '+setCtrl.buttonH);
            }
            if(setCtrl.buttonW === undefined || typeof setCtrl.buttonW !== 'string'){
                setCtrl.buttonW = mkInt(player.style.height) * 0.10 + 10 + 'px';
                mkLog('Setting-buttonW ( * Default * )');
            }else{
                mkLog('Setting-buttonW (Type expected, String-number in pixels): '+setCtrl.buttonW);
            }
            if(setCtrl.btnColUp === undefined || typeof setCtrl.btnColUp !== 'string'){
                setCtrl.btnColUp = '#458CCB';
                mkLog('Setting-btnColUp ( * Default * )');
            }else{
                mkLog('Setting-btnColUp (Type expected, Hexadecimal Color String): '+setCtrl.btnColUp);
            }
            if(setCtrl.btnColOver === undefined || typeof setCtrl.btnColOver !== 'string'){
                setCtrl.btnColOver = '#72ABDD';
                mkLog('Setting-btnColOver ( * Default * )');
            }else{
                mkLog('Setting-btnColOver (Type expected, Hexadecimal Color String): '+setCtrl.btnColOver);
            }
            if(setCtrl.btnBgTop === undefined || typeof setCtrl.btnBgTop !== 'string'){
                setCtrl.btnBgTop = '#040202';
                mkLog('Setting-btnBgTop ( * Default * )');
            }else{
                mkLog('Setting-btnBgTop (Type expected, Hexadecimal Color String): '+setCtrl.btnBgTop);
            }
            // if(setCtrl.btnBgTopOver === undefined || typeof setCtrl.btnBgTopOver !== 'string'){
            //     setCtrl.btnBgTopOver = '#404249'
            //     mkLog('Setting-btnBgTopOver ( * Default * )');
            // }else{
            //     mkLog('Setting-btnBgTopOver (Type expected, Hexadecimal Color String): '+setCtrl.btnBgTopOver);
            // }
            if(setCtrl.btnBgTopAlpha === undefined ||  setCtrl.btnBgTopAlpha === setCtrl.btnBgTopAlpha && setCtrl.btnBgTopAlpha !== (setCtrl.btnBgTopAlpha|0)){
                setCtrl.btnBgTopAlpha = 1.0;
                mkLog('Setting-btnBgTopAlpha ( * Default * )');
            }else{
                mkLog('Setting-btnBgTopAlpha (Type expected, Intager 0.0-1.0): '+setCtrl.btnBgTopAlpha);
            }
            if(setCtrl.btnBgBottom === undefined || typeof setCtrl.btnBgBottom !== 'string'){
                setCtrl.btnBgBottom = '#10100A';
                mkLog('Setting-btnBgBottom ( * Default * )');
            }else{
                mkLog('Setting-btnBgBottom (Type expected, Hexadecimal Color String): '+setCtrl.btnBgBottom);
            }
            // if(setCtrl.btnBgBottomOver === undefined || typeof setCtrl.btnBgBottomOver !== 'string'){
            //     setCtrl.btnBgBottomOver = '#2C365C';
            //     mkLog('Setting-btnBgBottomOver ( * Default * )');
            // }else{
            //     mkLog('Setting-btnBgBottomOver (Type expected, Hexadecimal Color String): '+setCtrl.btnBgBottomOver);
            // }
            if(setCtrl.btnBgBottomAlpha === undefined || setCtrl.btnBgBottomAlpha === setCtrl.btnBgBottomAlpha && setCtrl.btnBgBottomAlpha !== (setCtrl.btnBgBottomAlpha|0)){

                // console.log(setCtrl.btnBgBottomAlpha);
                // setCtrl.btnBgBottomAlpha = setCtrl.btnBgBottomAlpha;

                // setCtrl.btnBgBottomAlpha = 1.0;
                mkLog('Setting-btnBgBottomAlpha ( * Default * )');
            }else{
                mkLog('Setting-btnBgBottomAlpha (Type expected, Float 0.0-1.0): '+setCtrl.btnBgBottomAlpha);
            }
            if(setCtrl.nowPlayColor === undefined || typeof setCtrl.nowPlayColor !== 'string'){
                // Now playing thumbnail text`
                setCtrl.nowPlayColor = '#458CCB';
                mkLog('Setting-nowPlayColor ( * Default * )');
            }else{
                mkLog('Setting-nowPlayColor (Type expected, Hexadecimal Color String): '+setCtrl.nowPlayColor);
            }

            // console.log(setCtrl.bufferColor);
            // console.log(Object.prototype.toString.call(setCtrl.bufferColor));


            if(setCtrl.bufferColor === undefined || typeof setCtrl.bufferColor !== 'string' && Object.prototype.toString.call(setCtrl.bufferColor) !== '[object Array]'){
                setCtrl.bufferColor = '#FFFFFF';
                mkLog('Setting-bufferColor ( * Default * )');
            }else{
                mkLog('Setting-bufferColor (Type expected, Hexadecimal Color String | Array | rainbow): '+setCtrl.bufferColor);
            }
            if(setCtrl.volOffset === undefined || typeof setCtrl.volOffset !== 'number'){
                setCtrl.volOffset = 0;
                mkLog('Setting-volOffset ( * Default * )');
            }else{
                mkLog('Setting-volOffset (Type expected, Number): '+setCtrl.volOffset);
            }
            if(setCtrl.hideCtrlsTime === undefined || typeof setCtrl.hideCtrlsTime !== 'number'){
                setCtrl.hideCtrlsTime = 3;
                mkLog('Setting-hideCtrlsTime ( * Default * )');
            }else{
                mkLog('Setting-hideCtrlsTime (Type expected, Number): '+setCtrl.hideCtrlsTime);
            }
        }
        // ** -END- Build user costomizations -END- ** //


        // **  Build our controlls  ** //

        var ctrl = document.createElement('div');
        player.appendChild(ctrl);
        ctrl.id = 'vidCtrl';
        ctrl.style.visibility = setCtrl.shoCtrlOnStart;
        ctrl.style.zIndex = '50';

        btnH = setCtrl.buttonH;
        btnW = setCtrl.buttonW;
        
        // Video Player size variables
        var playerW = player.style.width;
        var playerH = player.style.height;
        var playerT = player.style.top;
        var playerL = player.style.left;

        video.style.height = playerH;
        video.style.width = playerW;

        // var mouseIdel, nowPlay;  
        var ctrlHidden = false;
        var padding = 5;
        var isPlaying = false;
        var btnS = [];

        var wW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        // this (wH) may be usefull in the future? As of right now it does nothing!
        var wH = window.screen.height || document.documentElement.clientHeight || document.body.clientHeight;

        var userA = getUserAgent();

        var font_family = window.getComputedStyle(document.body,null).getPropertyValue("font-family");

        /*/ create buttons and SVG's /*/
        var play = document.createElement('div');
        play.id = 'playBtn';
        play.style.width = btnW;
        play.style.height = btnH;
        play.style.position = 'absolute';
        play.style.visibility = 'visible';
        play.style.zIndex = '12';
        play.style.padding = '5px';

        play_bg = document.createElementNS(xmlns, 'svg');
        play_bg.setAttribute('baseProfile', 'tiny');
        play_bg.setAttribute('xlmns', xmlns);
        play_bg.setAttribute('viewBox', '0 0 20 15');
        play_bg.setAttribute('preserveAspectRatio', 'none');
        play_bg.setAttribute('height', mkInt(btnH));
        play_bg.setAttribute('width', mkInt(btnW));
        play_bg.setAttribute('x', '0');
        play_bg.setAttribute('y', '0');
        play_bg.style.display = 'block';

        var playG = document.createElementNS(xmlns, 'g');
        playG.setAttribute('fill', setCtrl.btnBgBottom);
        play_bg.appendChild(playG);

        var bottomFill = document.createElementNS(xmlns, 'path');
        bottomFill.setAttribute('d', 'M4.283 14.637c-2.14 0-3.88-1.294-3.88-2.883V3.256c0-1.59 1.74-2.883 3.88-2.883h11.433c2.14 0 3.88 1.293 3.88 2.883v8.498c0 1.59-1.74 2.883-3.88 2.883H4.283z');
        playG.appendChild(bottomFill);

        var outLine = document.createElementNS(xmlns, 'path');
        outLine.setAttribute('d', 'M15.716.524c2.026 0 3.676 1.226 3.676 2.73v8.5c0 1.505-1.65 2.73-3.676 2.73H4.283c-2.026 0-3.675-1.226-3.675-2.73v-8.5c0-1.505 1.648-2.73 3.675-2.73h11.433m0-.304H4.283C2.037.22.2 1.587.2 3.257v8.498c0 1.67 1.837 3.034 4.083 3.034h11.433c2.246 0 4.084-1.37 4.084-3.04v-8.5c0-1.67-1.838-3.035-4.084-3.035z');
        playG.appendChild(outLine);

        var linGrad = document.createElementNS(xmlns, 'linearGradient');
        linGrad.setAttribute('id', 'a');
        linGrad.setAttribute('gradientUnits', 'userSpaceOnUse');
        linGrad.setAttribute('x1', '9.999');
        linGrad.setAttribute('y1', '4.301');
        linGrad.setAttribute('x2', '9.999');
        linGrad.setAttribute('y2', '11.851');

        var stopTop = document.createElementNS(xmlns, "stop");
        stopTop.setAttribute('offset', '0');
        stopTop.setAttribute('stop-color', '#fff');
        linGrad.appendChild(stopTop);

        var stopBottom = document.createElementNS(xmlns, "stop");
        stopBottom.setAttribute('offset', '1');
        linGrad.appendChild(stopBottom);

        play_bg.appendChild(linGrad);

        var highlightLine = document.createElementNS(xmlns, 'path');
        highlightLine.setAttribute('d', 'M15.716.828H4.282c-1.8 0-3.265 1.09-3.265 2.428v8.498c0 1.34 1.464 2.427 3.265 2.427h11.434c1.8 0 3.267-1.08 3.267-2.42v-8.5c0-1.34-1.466-2.426-3.267-2.426zm2.858 10.926c0 1.17-1.283 2.124-2.858 2.124H4.282c-1.575 0-2.857-.953-2.857-2.124V3.256c0-1.17 1.282-2.125 2.857-2.125h11.434c1.575 0 2.858.96 2.858 2.13v8.5z');
        highlightLine.setAttribute('fill', 'url(#a)');
        play_bg.appendChild(highlightLine);

        var topFill = document.createElementNS(xmlns, 'path');
        topFill.setAttribute('d', 'M15.716.828H4.283c-1.8 0-3.267 1.09-3.267 2.428V7.65c2.72.522 5.766.815 8.984.815s6.263-.293 8.983-.814V3.26c0-1.34-1.465-2.428-3.267-2.428z');
        topFill.setAttribute('fill', setCtrl.btnBgTop);
        play_bg.appendChild(topFill);

        var playBtn = document.createElementNS(xmlns, 'svg');
        playBtn.setAttribute('xlmns', xmlns);
        playBtn.setAttribute('viewBox', '0 0 20 15');
        playBtn.setAttribute('preserveAspectRatio', 'none');
        playBtn.setAttribute('height', mkInt(btnH));
        playBtn.setAttribute('width', mkInt(btnW));
        playBtn.setAttribute('x', '0px');
        playBtn.setAttribute('y', '0px');
        playBtn.style.position = 'absolute';

        var play_btn = document.createElementNS(xmlns, 'path');
        play_btn.setAttribute('d', 'M6.212 3.974l7.913 3.834-7.913 3.834');
        play_btn.setAttribute('fill', setCtrl.btnColUp);
        play_btn.setAttribute('id', 'play_btn');

        var playTop = document.createElement('div');
        playTop.id = 'playTop';
        playTop.style.height = btnH;
        playTop.style.width = btnW;
        playTop.style.position = 'absolute';
        playTop.style.zIndex = '15';
        playTop.style.top = '0px';
        playTop.style.left = '0px';
        playTop.style.padding = '5px';
        btnS.push(play);
        
        play.appendChild(playTop);
        playBtn.appendChild(play_btn);
        play.appendChild(playBtn);
        play.appendChild(play_bg);

        // create slider thumbnail svg
        // maybe one day?

        // sets the background alpha on the buttons top and bottom
        chBtnBgAlpha(setCtrl.btnBgTopAlpha, setCtrl.btnBgBottomAlpha);
        
        var pause = document.createElement('div');
        pause.id = 'pauseBtn';
        pause.style.width = btnW;
        pause.style.height = btnH;
        pause.style.position = 'absolute';
        pause.style.visibility = 'hidden';
        pause.style.zIndex = '10';
        pause.style.padding = '5px';

        var pause_bg = play_bg.cloneNode(true);

        var pauseBtn = document.createElementNS(xmlns, 'svg');
        pauseBtn.setAttribute('xlmns', xmlns);
        pauseBtn.setAttribute('viewBox', '0 0 20 15');
        pauseBtn.setAttribute('preserveAspectRatio', 'none');
        pauseBtn.setAttribute('height', mkInt(btnH));
        pauseBtn.setAttribute('width', mkInt(btnW));
        pauseBtn.setAttribute('x', '0px');
        pauseBtn.setAttribute('y', '0px');
        pauseBtn.style.position = 'absolute';

        var pause_btn = document.createElementNS(xmlns, 'path');
        pause_btn.setAttribute('d', 'M6.212 4.312h2.56v6.993h-2.56zm5.12 0h2.56v6.993h-2.56z');
        pause_btn.setAttribute('fill', setCtrl.btnColUp);
        pause_btn.setAttribute('id', 'pause_btn');

        var pauseTop = document.createElement('div');
        pauseTop.id = 'pauseTop';
        pauseTop.style.height = btnH;
        pauseTop.style.width = btnW;
        pauseTop.style.position = 'absolute';
        pauseTop.style.zIndex = '15';
        pauseTop.style.top = '0px';
        pauseTop.style.left = '0px';
        pauseTop.style.padding = '5px';

        btnS.push(pause);

        pause.appendChild(pauseTop);
        pauseBtn.appendChild(pause_btn);
        pause.appendChild(pauseBtn);
        pause.appendChild(pause_bg);

        var seek = document.createElement('input');
        seek.id = 'seeker';
        seek.className = 'seekerC';
        seek.type = 'range';
        seek.min = 0;
        seek.max = 100;
        seek.value = 1;
        seek.step = 0.01;
        seek.style.position = 'absolute';
        seek.style.zIndex = 12;

        var time = document.createElement('div');
        time.style.position = 'absolute';
        time.style.textAlign = 'right';

        var timeTxt = document.createElement('span');
        timeTxt.id = 'timeTxt';
        timeTxt.className = 'timeC';
        timeTxt.innerHTML = '00:00 | ';

        var totalTxt = document.createElement('span');
        totalTxt.id = 'totalTxt';
        totalTxt.className = 'timeC';
        totalTxt.innerHTML = '00:00';

        var muteVid = document.createElement('div');
        muteVid.id = 'muteBtn';
        muteVid.style.width = btnW;
        muteVid.style.height = btnH;
        muteVid.style.position = 'absolute';
        muteVid.style.zIndex = '10';

        var mute_bg = play_bg.cloneNode(true);

        var muteBtn = document.createElementNS(xmlns, 'svg');
        muteBtn.setAttribute('xlmns', xmlns);
        muteBtn.setAttribute('viewBox', '0 0 20 15');
        muteBtn.setAttribute('preserveAspectRatio', 'none');
        muteBtn.setAttribute('height', mkInt(btnH));
        muteBtn.setAttribute('width', mkInt(btnW));
        muteBtn.setAttribute('x', '0px');
        muteBtn.setAttribute('y', '0px');
        muteBtn.style.position = 'absolute';

        var mute_btn = document.createElementNS(xmlns, 'path');
        mute_btn.setAttribute('d', 'M5.042 6.416v2.662h2.28l.038.023 2.81-2.25V4.634L7.324 6.416H5.042zm8.675-.48l-.913.73c.177.32.28.688.28 1.08 0 .72-.344 1.37-.857 1.76l.646.836c.873-.554 1.45-1.51 1.45-2.596 0-.676-.226-1.302-.606-1.81zM10.17 10.86V8.78L8.71 9.946l1.457.913zm4.418-6.886l.37.297-9.193 7.374-.37-.297');
        mute_btn.setAttribute('fill', setCtrl.btnColUp);
        mute_btn.setAttribute('id', 'mute_btn');
        btnS.push(muteVid);

        var muteTop = document.createElement('div');
        muteTop.id = 'muteTop';
        muteTop.style.height = btnH;
        muteTop.style.width = btnW;
        muteTop.style.position = 'absolute';
        muteTop.style.zIndex = '15';
        
        muteVid.appendChild(muteTop);
        muteBtn.appendChild(mute_btn);
        muteVid.appendChild(muteBtn);
        muteVid.appendChild(mute_bg);

        var unMuteVid = document.createElement('div');
        unMuteVid.id = 'unMuteBtn';
        unMuteVid.style.width = btnW;
        unMuteVid.style.height = btnH;
        unMuteVid.style.position = 'absolute';
        unMuteVid.style.zIndex = '12';

        var unMute_bg = play_bg.cloneNode(true);

        var unMuteBtn = document.createElementNS(xmlns, 'svg');
        unMuteBtn.setAttribute('xlmns', xmlns);
        unMuteBtn.setAttribute('viewBox', '0 0 20 15');
        unMuteBtn.setAttribute('preserveAspectRatio', 'none');
        unMuteBtn.setAttribute('height', mkInt(btnH));
        unMuteBtn.setAttribute('width', mkInt(btnW));
        unMuteBtn.setAttribute('x', '0px');
        unMuteBtn.setAttribute('y', '0px');
        unMuteBtn.style.position = 'absolute';

        var unMute1_btn = document.createElementNS(xmlns, 'path');
        unMute1_btn.setAttribute('d', 'M5.042 6.613V9.01H7.05l2.502 1.604V5.01L7.05 6.612m5.524-2.637l-.588.76c1.07.38 1.883 1.634 1.883 3.077 0 1.43-.8 2.676-1.858 3.066l.574.764c1.385-.58 2.37-2.075 2.37-3.83 0-1.762-.993-3.262-2.384-3.837z');
        unMute1_btn.setAttribute('fill', setCtrl.btnColUp);
        unMute1_btn.setAttribute('id', 'unMute1_btn');
        //btnS.push(unMute1);

        var unMute2_btn = document.createElementNS(xmlns, 'path');
        unMute2_btn.setAttribute('d', 'M11.43 5.453l-.576.744c.474.347.794.947.794 1.614 0 .65-.3 1.235-.753 1.587l.568.753c.768-.5 1.275-1.36 1.275-2.338 0-.992-.52-1.863-1.308-2.357z');
        unMute2_btn.setAttribute('fill', setCtrl.btnColUp);
        unMute2_btn.setAttribute('id', 'unMute2_btn');
        btnS.push(unMuteVid);

        var unMuteTop = document.createElement('div');
        unMuteTop.id = 'unMuteTop';
        unMuteTop.style.height = btnH;
        unMuteTop.style.width = btnW;
        unMuteTop.style.position = 'absolute';
        unMuteTop.style.zIndex = '15';
        
        unMuteVid.appendChild(unMuteTop);
        unMuteBtn.appendChild(unMute1_btn);
        unMuteBtn.appendChild(unMute2_btn);
        unMuteVid.appendChild(unMuteBtn);
        unMuteVid.appendChild(unMute_bg);

        var vol = document.createElement('input');
        vol.id = 'volumeSlider';
        vol.className = 'sliderC';
        vol.type = 'range';
        vol.min = '0';
        vol.max = '100';
        vol.value = '75';
        vol.step = '1';
        vol.style.position = 'absolute';
        vol.style.transform = 'rotate(270deg)';
        vol.style.width = (playerH * 0.30) - btnH + 'px';

        var rSize = document.createElement('div');
        rSize.id = 'rezBtn';
        //rSize.src = 'bgImgs/rez.svg';
        rSize.style.width = btnW;
        rSize.style.height = btnH;
        rSize.style.position = 'absolute';

        var rSize_bg = play_bg.cloneNode(true);

        var rSizeBtn = document.createElementNS(xmlns, 'svg');
        rSizeBtn.setAttribute('xlmns', xmlns);
        rSizeBtn.setAttribute('viewBox', '0 0 20 15');
        rSizeBtn.setAttribute('preserveAspectRatio', 'none');
        rSizeBtn.setAttribute('height', mkInt(btnH));
        rSizeBtn.setAttribute('width', mkInt(btnW));
        rSizeBtn.setAttribute('x', '0px');
        rSizeBtn.setAttribute('y', '0px');
        rSizeBtn.style.position = 'absolute';

        var rSize_btn = document.createElementNS(xmlns, 'path');
        rSize_btn.setAttribute('d', 'M13.323 9.113c-.03 0-.06.003-.09.004l-3.09-2.39c-.216-.166-.53-.21-.804-.14l.48-1.093c.08-.18 0-.445-.18-.58l-1.01-.78c-.177-.135-.52-.197-.75-.136l-.608.16c-.1.026-.16.07-.178.124-.018.053.013.114.087.172l.65.5c.23.18.23.47 0 .647l-.694.54c-.11.09-.26.14-.417.14-.16 0-.31-.045-.42-.132l-.65-.5c-.075-.06-.153-.082-.22-.07-.07.013-.128.06-.16.138l-.21.47c-.08.18 0 .445.177.58l1.01.78c.175.135.518.197.75.136l1.42-.38c-.092.21-.034.455.183.62l3.09 2.39c0 .024-.003.047-.003.07 0 .698.73 1.265 1.634 1.265s1.637-.566 1.637-1.264c0-.697-.73-1.264-1.634-1.264zm0 1.927c-.473 0-.856-.297-.856-.662 0-.178.088-.344.25-.47.162-.124.377-.193.606-.193.472 0 .856.298.856.663s-.39.662-.86.662z');
        rSize_btn.setAttribute('fill', setCtrl.btnColUp);
        rSize_btn.setAttribute('id', 'rez_btn');
        btnS.push(rSize);
        
        var rezTop = document.createElement('div');
        rezTop.id = 'rezTop';
        rezTop.style.height = btnH;
        rezTop.style.width = btnW;
        rezTop.style.position = 'absolute';
        rezTop.style.zIndex = '15';

        rSize.appendChild(rezTop);
        rSizeBtn.appendChild(rSize_btn);
        rSize.appendChild(rSizeBtn);
        rSize.appendChild(rSize_bg);

        var fsb = document.createElement('div');
        fsb.id = 'fScreenBtn';
        fsb.style.width = btnW;
        fsb.style.height = btnH;
        fsb.style.position = 'absolute';

        var fsb_bg = play_bg.cloneNode(true);

        var fsbBtn = document.createElementNS(xmlns, 'svg');
        fsbBtn.setAttribute('xlmns', xmlns);
        fsbBtn.setAttribute('viewBox', '0 0 20 15');
        fsbBtn.setAttribute('preserveAspectRatio', 'none');
        fsbBtn.setAttribute('height', mkInt(btnH));
        fsbBtn.setAttribute('width', mkInt(btnW));
        fsbBtn.setAttribute('x', '0px');
        fsbBtn.setAttribute('y', '0px');
        fsbBtn.style.position = 'absolute';

        var fsb_btn = document.createElementNS(xmlns, 'path');
        fsb_btn.setAttribute('d', 'M14.327 11.612h.63L14.95 4h-.63v.56h-.908v-.586H6.608v.004h-.02v.582h-.913v-.554h-.633l.006 7.61h.63v-.542h.915v.568h6.817v-.568h.914l.003.538zM7.54 10.576L7.538 5.04h4.923l.008 5.536H7.54zm-1.863-2.52h.915v1.03H5.68v-1.03zm.914-.48h-.91v-1.03h.915v1.03zm6.82-1.03h.916v1.03h-.913l-.002-1.03zm0 1.51h.917v1.03h-.914v-1.03zm.91-3.017l.005 1.03h-.915V5.04h.912zm-7.73 0v1.03h-.91V5.04h.913zm-.91 5.55V9.56h.916v1.03H5.68zm7.735 0v-.018l-.002-1.012h.916v1.03h-.914z');
        fsb_btn.setAttribute('fill', setCtrl.btnColUp);
        fsb_btn.setAttribute('id', 'fScreen_btn');
        btnS.push(fsb);

        var fScreenTop = document.createElement('div');
        fScreenTop.id = 'fScreenTop';
        fScreenTop.style.height = btnH;
        fScreenTop.style.width = btnW;
        fScreenTop.style.position = 'absolute';
        fScreenTop.style.zIndex = '15';

        fsb.appendChild(fScreenTop);
        fsbBtn.appendChild(fsb_btn);
        fsb.appendChild(fsbBtn);
        fsb.appendChild(fsb_bg);

        // this is for the fullscreen info button svg
        var infoBG = document.createElementNS(xmlns, 'svg');
        infoBG.setAttribute('baseProfile', 'tiny');
        infoBG.setAttribute('xlmns', xmlns);
        infoBG.setAttribute('viewBox', '0 0 20 15');
        infoBG.setAttribute('preserveAspectRatio', 'none');
        infoBG.setAttribute('height', mkInt(btnH));
        infoBG.setAttribute('width', mkInt(btnW));
        infoBG.setAttribute('x', '0');
        infoBG.setAttribute('y', '0');

        var ellipse = document.createElementNS(xmlns, 'ellipse');
        ellipse.setAttribute('cx', '9.7908363');
        ellipse.setAttribute('cy', '7.3207169');
        ellipse.setAttribute('rx', '5.9063745');
        ellipse.setAttribute('ry', '5.9262953');
        ellipse.setAttribute('fill', setCtrl.btnColOver);

        gsap.to(ellipse, {duration: 0.1, scaleX: 0.0, scaleY: 0.0,
                          x: mkInt(btnH)*0.1, y: mkInt(btnW)*0.1, overwrite: true});

        let circleBG = document.createElementNS(xmlns, 'g');
        // circleBG.setAttribute('fill', setCtrl.btnBgBottom);
        infoBG.appendChild(circleBG);
        circleBG.appendChild(ellipse);

        let circleBottomFill = document.createElementNS(xmlns, 'path');
        circleBottomFill.setAttribute('d', 'm 11.281,8.145 v 2.633 c 0.125,0.072 0.377,0.23 0.377,0.49 0,0.418 -0.598,0.678 -1.415,0.678 -0.974,0 -2.294,-0.217 -2.294,-0.707 0,-0.173 0.251,-0.346 0.408,-0.403 V 8.158 C 6.77,8.102 5.235,7.968 3.766,7.774 c 0.291,3.312 2.933,5.902 6.151,5.902 3.214,0.004 5.852,-2.578 6.15,-5.883 -1.524,0.198 -3.135,0.306 -4.786,0.352 z');
        circleBottomFill.setAttribute('fill', setCtrl.btnBgBottom);
        circleBG.appendChild(circleBottomFill);

        let circleTopFill = document.createElementNS(xmlns, 'path');
        circleTopFill.setAttribute('d', 'm 9.896,1.318 c -3.402,0 -6.16,2.759 -6.16,6.165 0,0.194 0.012,0.386 0.029,0.574 1.465,0.184 3.012,0.286 4.593,0.338 V 6.711 C 8.295,6.625 8.043,6.48 8.043,6.235 8.012,5.932 8.86,5.557 9.583,5.557 c 1.918,0 1.981,0.433 1.981,0.836 0,0.13 -0.094,0.289 -0.283,0.375 V 8.406 C 12.918,8.361 14.514,8.26 16.027,8.074 16.047,7.88 16.058,7.683 16.058,7.483 16.059,4.078 13.299,1.318 9.896,1.318 Z m -0.029,3.49 c -0.975,0 -1.886,-0.49 -1.886,-1.341 0,-0.418 0.503,-0.663 1.414,-0.663 1.195,0 1.886,0.577 1.886,1.312 0,0.403 -0.408,0.692 -1.414,0.692 z');
        circleTopFill.setAttribute('fill', setCtrl.btnBgTop);
        circleBG.appendChild(circleTopFill);

        let circleBorder = document.createElementNS(xmlns, 'g');
        // circleBorder.setAttribute('fill', setCtrl.btnBgBottom);
        infoBG.appendChild(circleBorder);

        // I might want to rethink this path. The complexxity is lost on it's small size. Simplicity might be nice as well?
        let borderPath = document.createElementNS(xmlns, 'path');
        borderPath.setAttribute('d', 'M16.021,8.484c0,0-0.041,0.142-0.117,0.405    c-0.038,0.132-0.086,0.293-0.144,0.484c-0.038,0.188-0.138,0.415-0.278,0.674c-0.135,0.253-0.271,0.547-0.45,0.833    c-0.208,0.261-0.435,0.54-0.675,0.841c-0.267,0.275-0.597,0.521-0.92,0.804c-0.352,0.246-0.762,0.444-1.168,0.679    c-0.879,0.321-1.889,0.605-2.962,0.485c-0.28-0.032-0.509-0.021-0.84-0.096c-0.248-0.072-0.499-0.146-0.752-0.223    c-0.252-0.054-0.513-0.188-0.774-0.329c-0.255-0.145-0.539-0.26-0.77-0.438C5.214,11.93,4.461,10.96,3.96,9.86    C3.504,8.73,3.403,7.48,3.579,6.271c0.263-1.162,0.817-2.318,1.638-3.189C6.071,2.249,7.101,1.634,8.203,1.36    C9.3,1.15,10.43,1.118,11.398,1.479c0.492,0.102,0.93,0.38,1.357,0.603c0.428,0.231,0.748,0.58,1.104,0.847    c0.281,0.347,0.574,0.66,0.799,0.995c0.188,0.359,0.394,0.687,0.531,1.017c0.107,0.343,0.211,0.663,0.305,0.96    c0.059,0.304,0.074,0.591,0.107,0.846c0.099,0.51-0.027,0.915-0.024,1.188c-0.022,0.274-0.036,0.42-0.036,0.42L16.021,8.484z     M15.541,8.352c0,0,0.13-1.099,0.02-1.604c-0.041-0.252-0.063-0.536-0.129-0.834c-0.102-0.291-0.209-0.604-0.326-0.94    c-0.146-0.321-0.356-0.637-0.549-0.982c-0.229-0.322-0.523-0.621-0.805-0.951c-0.675-0.552-1.429-1.104-2.417-1.337    c-0.943-0.325-2.02-0.258-3.054-0.035C7.245,1.954,6.292,2.563,5.514,3.36C4.769,4.208,4.309,5.224,4.072,6.357    C3.944,7.477,4.066,8.613,4.504,9.628c0.484,0.983,1.184,1.836,2.05,2.413c0.208,0.152,0.443,0.23,0.658,0.352    c0.213,0.116,0.427,0.229,0.7,0.281c0.255,0.067,0.508,0.135,0.758,0.202c0.173,0.031,0.462,0.029,0.682,0.047    c0.946,0.086,1.815-0.196,2.571-0.487c0.347-0.217,0.697-0.395,0.995-0.612c0.271-0.256,0.553-0.472,0.773-0.715    c0.199-0.263,0.387-0.509,0.56-0.734c0.142-0.239,0.233-0.471,0.34-0.669c0.052-0.102,0.101-0.194,0.146-0.284    c0.039-0.1,0.061-0.211,0.089-0.305c0.051-0.192,0.094-0.356,0.129-0.489c0.067-0.267,0.106-0.407,0.106-0.407L15.541,8.352z');
        borderPath.setAttribute('fill', setCtrl.btnBgBottom);
        circleBorder.appendChild(borderPath);

        // holder divs for the controls
        var ctrls = document.createElement('div');
        ctrls.id = 'ctrls';
        ctrls.style.position = 'absolute';
        ctrls.style.zIndex = '12';
        ctrls.style.padding = padding + 'px';
        ctrls.style.display = 'inline-block';

        ctrls.appendChild(play);
        ctrls.appendChild(pause);
        ctrls.appendChild(seek);
        time.appendChild(timeTxt);
        time.appendChild(totalTxt);
        ctrls.appendChild(time);

        var sideCtrl = document.createElement('div');
        sideCtrl.id = 'sideCtrl';
        sideCtrl.style.position = 'absolute';
        sideCtrl.style.display = 'block';
        sideCtrl.style.zIndex = '12';
        sideCtrl.style.padding = '5px 0px 5px 5px';
        sideCtrl.style.width = mkInt(btnW) * 3 + 'px';
        sideCtrl.style.height = btnH;
        sideCtrl.style.margin = '5px';

        var volCtrl = document.createElement('div');
        volCtrl.id = 'volCtrl';
        volCtrl.style.position = 'absolute';
        volCtrl.style.overflow = 'hidden';
        volCtrl.style.zIndex = '12';
        volCtrl.style.display = 'block';
        volCtrl.style.height = btnH;
        volCtrl.style.width = btnW;

        var sizeCtrl = document.createElement('div');
        sizeCtrl.id = 'sizeCtrl';
        sizeCtrl.style.position = 'absolute';
        sizeCtrl.style.zIndex = '12';
        sizeCtrl.style.display = 'block';
        sizeCtrl.style.height = btnH;
        sizeCtrl.style.width = btnW;
        
        volCtrl.appendChild(muteVid);
        volCtrl.appendChild(unMuteVid);
        volCtrl.appendChild(vol);
        sideCtrl.appendChild(volCtrl);
        sizeCtrl.appendChild(rSize);
        sideCtrl.appendChild(sizeCtrl);
        sideCtrl.appendChild(fsb);
        ctrl.appendChild(ctrls);
        ctrl.appendChild(sideCtrl);

        var vidCtrlBG = document.createElement('div');
        vidCtrlBG.className = 'vidCtrlBG';
        vidCtrlBG.style.position = 'absolute';
        vidCtrlBG.style.width = '100%';
        vidCtrlBG.style.zIndex = '10';
        ctrl.appendChild(vidCtrlBG);

        var buffedOut = document.createElement('div');
        buffedOut.style.overflow = 'hidden';

        ctrls.appendChild(buffedOut);

        // position elements 
        elmPosNorm();


        // **  create event listeners  ** //

        // screen.orientation.addEventListener("change", function(e) {
        //     alert(screen.orientation.type + " " + screen.orientation.angle);
        //     }, false);

        // screen.orientation.addEventListener('change', rotate, false);

        screen.orientation.addEventListener('change', function() {
            console.log('Current orientation is ' + screen.orientation.type);
        });



        
        play.addEventListener('click', playPause, false);
        pause.addEventListener('click', playPause, false);
        muteVid.addEventListener('click', muteVideo, false);
        unMuteVid.addEventListener('click', muteVideo, false);
        fsb.addEventListener('click', fullScreen, false);
        volCtrl.addEventListener('mouseover', shoVol, false);
        volCtrl.addEventListener('mouseout', hideVol, false);
        seek.addEventListener('change', videoSeeker, false);
        vol.addEventListener('change', volumeSlider, false);
        video.addEventListener('timeupdate', timeTrack, false);

        video.addEventListener('mousemove', function(){
            shoVidCtrl();
            clearTimeout(mouseIdel);
            mouseIdel = setTimeout(hideVidCtrl, setCtrl.hideCtrlsTime * 1000);
        }, false);

        mkLog('video.preload = '+video.preload);

        video.addEventListener('progress', getProgress, false);

        video.addEventListener('mouseover', function(){
            window.addEventListener('keydown', spacePP, false);
            window.addEventListener('keydown', seekFWD, false);
            window.addEventListener('keydown', seekBAK, false);
        }, false);
        
        video.addEventListener('mouseout', function(){
            video.removeEventListener('mousemove', shoVidCtrl);
            window.removeEventListener('keydown', spacePP, false);
            window.removeEventListener('keydown', seekFWD, false);
            window.removeEventListener('keydown', seekBAK, false);
            hideVidCtrl();
        }, false);

        ctrl.addEventListener('mouseover', function(){
            shoVidCtrl();
            clearTimeout(mouseIdel);
        }, false);

        ctrl.addEventListener('mouseout', hideVidCtrl, false);


        // create the button mouseover:out listeners and events
        for(let btn in btnS){
            let btnDiv = btnS[btn];
            btnDiv.addEventListener('mouseover', btnOver, false);
            btnDiv.addEventListener('mouseout', btnOut, false);
        }

        // Create our show hide controlls animation
        var ctrlFade_tween = gsap.fromTo(ctrl,
                                        {duration: 0.5, opacity:0, onComplete:function(){

                                                ctrl.style.visibility = 'hidden';
                                                play.style.visibility = 'hidden';
                                                pause.style.visibility = 'hidden';
                                                muteVid.style.visibility = 'hidden';
                                                unMuteVid.style.visibility = 'hidden';
                                                video.style.cursor = 'none';
                                            }
                                        },
                                        {opacity: 1, duration: 0.5, onComplete: function(){
                                                if(!video.paused){
                                                    play.style.zIndex = '10';
                                                    play.style.visibility = 'hidden';
                                                    pause.style.zIndex = '12';
                                                    pause.style.visibility = 'visible';
                                                }else{
                                                    play.style.zIndex = '12';
                                                    play.style.visibility = 'visible';
                                                    pause.style.zIndex = '10';
                                                    pause.style.visibility = 'hidden';
                                                }

                                                if(!video.muted){
                                                    muteVid.style.zIndex = '10';
                                                    muteVid.style.visibility = 'hidden';
                                                    unMuteVid.style.zIndex = '12';
                                                    unMuteVid.style.visibility = 'visible';
                                                }else{
                                                    muteVid.style.zIndex = '12';
                                                    muteVid.style.visibility = 'visible';
                                                    unMuteVid.style.zIndex = '10';
                                                    unMuteVid.style.visibility = 'hidden';
                                                }

                                                ctrl.style.visibility = 'visible';
                                                video.style.cursor = 'default';
                                            }
                                        });

        // a quick hack to fix the video seaker width correct after user zooms in
        // with ctrl + "+" ... Might need mouse wheel if I cant find solution.
        window.addEventListener('keydown', onZoom, false);

    }


    //  ** Start building our playlist **  //

    // AJAX Request for getting/setting video playlist and sizes
    var req = new XMLHttpRequest();
    var vars;
    if(loc.includes('.json')){
        req.open('GET', loc, true);
    }
    else{
        if(!setCtrl.playlist){
            vars = 'functToGet=mkH5Videos';
        }else{
            vars = 'functToGet=vidSort&playlist='+setCtrl.playlist.join(',');
        }
        req.open('POST', loc, true);
    }

    var objct = null;
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function(){
        if(req.readyState == 4 && req.status == 200){
            var ret = req.responseText;
            objct = JSON.parse(ret);
            var tLoc = getTopDir();

            // create the thumbs
            if(Object.keys(objct).length > 2){

                var pList = document.createElement('div');
                pList.id = 'playList';
                pList.style.position = 'absolute';

                var pScroll = document.createElement('div');
                pScroll.id = 'playListScroll';
                pScroll.style.overflow = 'auto';
                pScroll.style.position = 'absolute';

                var pListHolder = document.createElement('div');
                pListHolder.id = 'pListHolder';
                pListHolder.style.position = 'absolute';

                pScroll.appendChild(pList);
                pListHolder.appendChild(pScroll);
                player.appendChild(pListHolder);

                var pListW = 0;
                var obPos = 0;
                var v = 0;
                var len = Object.keys(objct).length;
                var vidObj, ob, nPos, order, objc;
                var addedArr = [];
                for(i = 1; i <= len; i++){
                    ob = Object.keys(objct)[i];
                    if(ob != 'settings'){
                        if(!setCtrl.videoOrder){
                            vidObj = objct[ob];
                        }else{
                            if(setCtrl.videoOrder[v] !== undefined &&
                                (strUnFix(setCtrl.videoOrder[v])) in objct &&
                                addedArr.indexOf(strUnFix(setCtrl.videoOrder[v])) === -1){

                                vidObj = objct[strUnFix(setCtrl.videoOrder[v])];
                                ob = strUnFix(setCtrl.videoOrder[v]);
                                addedArr.push(strUnFix(setCtrl.videoOrder[v]));
                                i--;
                            }
                            else{
                                if(objct[ob] === undefined){
                                    break;
                                }
                                if(addedArr.indexOf(ob) === -1){
                                    vidObj = objct[ob];
                                    addedArr.push(ob);
                                }else{
                                    vidObj = false;
                                }
                            }
                        }
                        if(vidObj !== false && vidObj !== undefined){

                            let tmpObj = vidObj.thumb[Object.keys(vidObj.thumb)[0]]
                            // console.log('Here man  >  '+tmpObj.width)

                            if(tmpObj.width > pListW){ 
                                pListW = tmpObj.width;
                            }
                            var pStuff = document.createElement('div');
                            pStuff.id = ob+'_holder';
                            pStuff.className = 'playListVideo_thumb';
                            pStuff.style.position = 'absolute';
                            pStuff.style.display = 'block';
                            pStuff.style.overflow = 'hidden';
                            pStuff.style.width = tmpObj.width +'px';
                            pStuff.style.height = tmpObj.height +'px';
                            pStuff.style.top = obPos +'px';
                            pList.appendChild(pStuff); 
                             
                            var pThumb = document.createElement('img');
                            pThumb.id = ob+'_pic';
                            if(tLoc === ''){
                                pThumb.src = tmpObj.loc;
                            }
                            else{
                                let pLoc = tLoc+'/';
                                pThumb.src = pLoc+tmpObj.loc;
                            }

                            pThumb.style.width = pStuff.style.width;
                            pThumb.style.height = pStuff.style.height;
                            pThumb.style.zIndex = '13';

                            var plName = document.createElement('p');
                            plName.id = ob+'_title';
                            plName.className = 'thumbTitle';
                            plName.style.position = 'absolute';
                            plName.style.bottom = '0px';
                            plName.innerHTML = vidObj.name;
                            plName.style.zIndex = '15';
                            plName.style.overflow = 'visible';
                            plName.style.fontSize = 12;
                            // let font_family = window.getComputedStyle(document.body,null).getPropertyValue("font-family");
                            plName.style.width = getTextWidth(vidObj.name, plName.style.fontSize + 'pt '+font_family) + 'px';

                            var nameHold = document.createElement('div');
                            nameHold.id = ob+'_nameHold';
                            nameHold.className = 'thumbTitleHolder';
                            nameHold.style.position = 'absolute';
                            nameHold.style.bottom = '0px';
                            nameHold.style.left = '0px';
                            nameHold.style.width = mkInt(pStuff.style.width) + 'px';
                            nameHold.style.height = tmpObj.height * .36 + 'px';
                            nameHold.style.zIndex = '14';
                            nameHold.style.backgroundColor = '#000000';
                            nameHold.style.opacity = 0.7;

                            nameHold.appendChild(plName);

                            var btnDiv = document.createElement('div');
                            btnDiv.id = ob+'_thumb';
                            btnDiv.style.position = 'absolute';
                            btnDiv.style.top = '0px';
                            btnDiv.style.width = pStuff.style.width;
                            btnDiv.style.height = pStuff.style.height;
                            btnDiv.style.zIndex = '16';

                            pStuff.appendChild(btnDiv);
                            pStuff.appendChild(pThumb);
                            pStuff.appendChild(nameHold);

                            let txtWidth = mkInt(window.getComputedStyle(plName, null).getPropertyValue("width"));
                            let txtHeight = mkInt(window.getComputedStyle(plName, null).getPropertyValue("height"));
                            let holdWidth = mkInt(nameHold.style.width);

                            plName.style.right = scrolTxtPos(holdWidth, txtWidth);
                            plName.style.width = txtWidth + 2 + 'px';
                            plName.style.height = txtHeight + 'px';
                            
                            document.getElementById(pStuff.id).addEventListener('mouseover', thumbOver, false);
                            document.getElementById(pStuff.id).addEventListener('mouseout', thumbOut, false);
                            document.getElementById(pStuff.id).addEventListener('click', thumbClick, false);

                            if(obPos === 0){
                                nowPlay = ob+'_title';
                                plName.style.color = setCtrl.nowPlayColor;
                                video.id = ob;

                                // Swich the video source
                                vidSwich(tLoc, objct[ob]);
                            }

                            obPos = obPos + tmpObj.height + 5;
                            v++;
                        }
                    }
                }

                pList.style.width = pListW + 'px';
                pList.style.zIndex = '12';

                pListHolder.style.height = player.style.height;
                pListHolder.style.width = pListW + 20 + 'px';
                pListHolder.style.zIndex = '10';
                pListHolder.style.top = 0 + 'px';
                pListHolder.style.left = mkInt(video.style.left) + mkInt(video.style.width) + 10 + 'px';

                pScroll.style.height = pListHolder.style.height;
                pScroll.style.width = pListW + 20 + 'px';
                pScroll.style.opacity = '1.0';
            }


            var aboutBG = document.createElement('div');
            aboutBG.id = 'aboutBG';
            aboutBG.style.position = 'absolute';
            aboutBG.style.display = 'block';
            aboutBG.className = 'aboutBG';
            player.appendChild(aboutBG);

            rSizeVid();
        }

        // These functions handle the thumbnails
        function thumbOver(e){
            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_thumb', '_pic');
            let txt = document.getElementById(tgt.id.replace('_thumb', '_title'));
            let txtHoldWidth = mkInt(txt.style.width);
            let theWidth = mkInt(document.getElementById(tgt.id.replace('_thumb', '_holder')).style.width);
            txtScroller(txt, txtHoldWidth, theWidth);
            let pic = document.getElementById(tId);
            gsap.to(pic, {duration: 0.1, scaleX: 1.15, scaleY: 1.15, overwrite: true});
        }

        function thumbOut(e){
            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_thumb', '_pic');
            let pic = document.getElementById(tId);
            gsap.to(pic, {duration: 0.1, scaleX: 1, scaleY: 1, overwrite: true});
        }

        function thumbClick(e){
            seek.value = 1;
            if(!video.paused){
                isPlaying = false;
                video.pause();
                video.currentTime = 0;
            }

            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_thumb', '');
            video.id = tId;

            if(nowPlay !== null || nowPlay !== undefined){
                document.getElementById(strUnFix(nowPlay)).style.color = '#fff';
            }

            nowPlay = tId+'_title';
            document.getElementById(nowPlay).style.color = setCtrl.nowPlayColor;
            let vObj = objct[tId];
            let tLoc = getTopDir();

            // remove elements for recreation of them with new data rezBtns
            sizeCtrl.removeChild(document.getElementById('rSizeRHolder'));
            // remove the about stuff
            document.getElementById('aboutBG').removeChild(document.getElementById('aboutScroller'));
            document.getElementById('aboutBG').removeChild(document.getElementById('aboutVidName'));

            // scroll thumb text
            let txt = document.getElementById(tgt.id.replace('_thumb', '_title'));
            let txtHoldWidth = mkInt(document.getElementById(tgt.id.replace('_thumb', '_title')).style.width);
            let theWidth = mkInt(document.getElementById(tgt.id.replace('_thumb', '_holder')).style.width);
            txtScroller(txt, txtHoldWidth, theWidth);

            // change video resolution settings
            rSizeVid(vObj);

            // swich the video source
            vidSwich(tLoc, vObj);

            shoVidCtrl();
            clearTimeout(mouseIdel);

        }

        function rSizeVid(vObj){
            // this function creates the about elements and resizes the video
            let i, j, obc;
            if(vObj === undefined || vObj === ''){
                j = 1;
                let key = false;
                for(obc in objct){
                    if(obc == video.id){
                        vObj = objct[obc];
                        key = true;
                        break;
                    }
                    j++;
                }
                if(!key){
                   mkLog('If you want the resolution changer to work initially you must give your video a id that has a key from the json file.');
                }
            }else{
                j = 1;
                for(obc in objct){
                    if(objct[obc] == vObj){
                        break;
                    }
                    j++;
                }
            }

            // create the about video section
            let aboutVidTxt = document.createElement('div');
            aboutVidTxt.id = 'aboutVidTxt';
            aboutVidTxt.style.display = 'block';

            let aboutHolder = document.createElement('div');
            aboutHolder.id = 'aboutHolder';
            aboutHolder.style.position = 'relative';
            aboutHolder.style.display = 'block';

            let aboutScroller = document.createElement('div');
            aboutScroller.id = 'aboutScroller';
            aboutScroller.style.position = 'relative';
            aboutScroller.style.display = 'block';

            let aboutBG = document.getElementById('aboutBG');

            let vidName = document.createElement('h3');
            vidName.id = 'aboutVidName';
            vidName.style.display = 'block';
            vidName.style.fontSize = '14px';
            aboutBG.appendChild(vidName);
            
            // create the resize buttons
            let rSizeR = document.createElement('div');
            rSizeR.id = 'rSizeR';
            rSizeR.className = 'rSizeRC';
            rSizeR.style.position = 'absolute';
            rSizeR.style.width = btnW;
            
            let rSizeRHolder = document.createElement('div');
            rSizeRHolder.id = 'rSizeRHolder';
            rSizeRHolder.style.position = 'absolute';
            rSizeRHolder.style.width = btnW;
            rSizeRHolder.style.overflow = 'hidden';

            rSizeRHolder.appendChild(rSizeR);
            sizeCtrl.appendChild(rSizeRHolder);

            nPos = 2;
            order = ['XD', 'HD', 'HQ', 'SD', 'LQ', 'M'];
            let x = 1;
            for(objc in objct){
                if(x === j){
                    for(i = 0; i <= order.length - 1; i++){
                        for(ob in objct[objc].mp4){
                            if(order[i] == ob){
                                // build the sizers and eventListeners
                                let rezC = document.createElement('div');
                                rezC.id = ob;
                                rezC.className = 'rezCtrls';
                                rezC.style.width = btnW;
                                rezC.style.height = btnH;
                                rezC.style.display = 'block';
                                rezC.style.textAlign = 'center';
                                if(i === 0){
                                    rezC.style.padding = '10px 0px 0px 0px';
                                }
                                if(rezC.id == setCtrl.uSetRez){
                                    rezC.style.color = setCtrl.rezColorSelect;
                                }else{
                                    rezC.style.color = setCtrl.rezColorOver;
                                }
                                rezC.innerHTML = ob;
                                rSizeR.appendChild(rezC);
                                rezC.style.bottom = nPos + 'px';
                                nPos = nPos + mkInt(btnH);
                            }
                        }
                    }

                    mkAbout(objct[objc].about, aboutVidTxt);

                    vidName.innerHTML = objct[objc].name;
                    break;
                }
                x++;
            }

            // position the resize buttons
            fsb.style.zIndex = '13';
            rSizeR.style.height = nPos + 'px';
            rSizeR.style.left = '0px';
            rSizeR.style.bottom = '0px';
            rSizeR.style.zIndex = '12';

            rSizeRHolder.style.left = '0px';
            rSizeRHolder.style.bottom = mkInt(ctrl.style.bottom) + mkInt(ctrl.style.height) - padding + 'px';
            rSizeRHolder.style.height = '0px';
            rSizeRHolder.style.zIndex = '10';

            let rezSizR_tween = gsap.fromTo(rSizeRHolder, {duration: 0.5, height: nPos, ease: Power4.easeOut},
                                                          {duration: 0.35, height: 0, ease: Power2.easeIn});
            rezSizR_tween.pause(rezSizR_tween.endTime());
            rSizeR.addEventListener('click', rSizeClick, false);
            rSizeR.addEventListener('mouseover', rSizeOver, false);
            rSizeR.addEventListener('mouseout', rSizeOut, false);

            // position the about section
            aboutHolder.appendChild(aboutVidTxt);
            aboutScroller.appendChild(aboutHolder);
            aboutBG.appendChild(aboutScroller);

            aboutBG.style.left = video.style.left;
            aboutBG.style.top = mkInt(video.style.top) + mkInt(video.style.height) + 30 + 'px';

            if(pListHolder === undefined || pListHolder === null){
                // if there is only one video the player will not have a pListHolder
                aboutBG.style.width = mkInt(player.style.width) + 50 +'px';
            }else{
                aboutBG.style.width = mkInt(player.style.width) + mkInt(pListHolder.style.width) + 'px';
            }
            aboutBG.style.zIndex = '11';

            if(pListHolder === undefined || pListHolder === null){
                // if there is only one video the player will not have a pListHolder
                aboutScroller.style.width = mkInt(aboutBG.style.width) + 'px';
            }else{
                aboutScroller.style.width = mkInt(player.style.width) + mkInt(pListHolder.style.width) + 'px';
                
            }
            aboutScroller.style.zIndex = '11';
            aboutScroller.style.margin = '0px 10px 10px 0px';

            aboutHolder.style.left = '0px';
            aboutHolder.style.top = '0px';

            if(pListHolder === undefined || pListHolder === null){
                // if there is only one video the player will not have a pListHolder
                aboutHolder.style.width = mkInt(aboutBG.style.width) - 15 + 'px';
            }else{
                aboutHolder.style.width = mkInt(player.style.width) + mkInt(pListHolder.style.width) - 15 + 'px';
            }

            aboutHolder.style.zIndex = '11';

            vidName.style.zIndex = '2';
            aboutVidTxt.style.zIndex = '12';
            aboutVidTxt.style.width = '98%';
            aboutVidTxt.style.margin = '0 auto';

            rSize.addEventListener('click', shoSize, false);
            let sizSho = false;

            // these functions handle the resize function
            function rSizeOver(e){
                let tgt = e.target || e.srcElement || window.event;
                tgt.style.color = setCtrl.rezColorUp;
                gsap.to(tgt, {duration: 0.1, scaleX: 1.1, scaleY: 1.1, overwrite: true});
            }

            function rSizeOut(e){
                let tgt = e.target || e.srcElement || window.event;
                gsap.to(tgt, {duration: 0.1, scaleX: 1, scaleY: 1, overwrite: true});
                if(tgt.id == setCtrl.uSetRez){
                    tgt.style.color = setCtrl.rezColorSelect;
                }else{
                    tgt.style.color = setCtrl.rezColorOver;
                }
            }

            function rSizeClick(e){
                buffedOut.innerHTML = '';
                let cTime = video.currentTime;
                let tgt = e.target || e.srcElement || window.event;
                let tLoc = getTopDir();

                let qRez = ['XD', 'HD', 'HQ', 'SD', 'LQ', 'M'];

                if(document.getElementById(setCtrl.uSetRez) === null){
                    for(let val in qRez.reverse()){
                        // console.log(document.getElementById(setCtrl.uSetRez));
                        if(document.getElementById(setCtrl.uSetRez !== null)){
                            document.getElementById(setCtrl.uSetRez).style.color = setCtrl.rezColorOver;
                            break;
                        }

                    }
                }else{
                    document.getElementById(setCtrl.uSetRez).style.color = setCtrl.rezColorOver;
                }
                

                setCtrl.uSetRez = tgt.id;
                tgt.style.color = setCtrl.rezColorUp;

                // Swich the video object source
                vidSwich(tLoc, objct[video.id]);

                // seek.value = seek.value;
                video.load();
                video.currentTime = cTime;

                if(isPlaying){
                    video.play();
                }
            }

            function shoSize(){
                gsap.to(rSize_btn, {duration: 0, fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0, overwrite: true});
                gsap.to(rSize_btn, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
                if(sizSho){
                    rezSizR_tween.play();
                    rSize.style.zIndex = '10';
                    rSizeRHolder.style.zIndex = '12';
                    rSizeR.style.zIndex = '13';
                    sizSho = false;
                }else{
                    hideSize();
                }
            }

            function hideSize(){
                rezSizR_tween.reverse();
                rSizeRHolder.style.zIndex = '10';
                rSizeR.style.zIndex = '10';
                rSize.style.zIndex = '12';
                sizSho = true;
            }
        }
    };

    if(loc.includes('.json')){
        req.send(null);
    }
    else{
        req.send(vars);
    }

    // these functions are for the main buttons.
    function btnOver(e){
        let bt = e.target || e.srcElement || window.event;
        let btN, btN2;
        if(bt.id == 'unMuteTop'){
            btN = bt.id.replace('Top', '1_btn');
            btN2 = bt.id.replace('Top', '2_btn');
            btN = document.getElementById(btN);
            btN2 = document.getElementById(btN2);
            gsap.to(btN, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
            gsap.to(btN2, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
        }else{
            btN = bt.id.replace('Top', '_btn');
            btN = document.getElementById(btN);
            gsap.to(btN, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
        }
    }

    function btnOut(e){
        let bt = e.target || e.srcElement || window.event;
        let btN, btN2;
        if(bt.id == 'unMuteTop'){
            btN = bt.id.replace('Top', '1_btn');
            btN2 = bt.id.replace('Top', '2_btn');
            btN = document.getElementById(btN);
            btN2 = document.getElementById(btN2);
            gsap.to(btN, {duration: 0.5, fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0, overwrite: true});
            gsap.to(btN2, {duration: 0.5, fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0, overwrite: true});
        }else{
            btN = bt.id.replace('Top', '_btn');
            btN = document.getElementById(btN);
            gsap.to(btN, {duration: 0.5, fill: setCtrl.btnColUp, scale: 1, x: 0, y: 0, overwrite: true});
        }
    }
    // end main button functions

    function chBtnBgColor(topColor, topAlpha, bottomColor, bottomAlpha){
        gsap.to(bottomFill, {duration: 0, fill: topColor, fillOpacity: topAlpha, overwrite: true});
        gsap.to(topFill, {duration: 0, fill: bottomColor, fillOpacity: bottomAlpha, overwrite: true});
    }

    function chBtnBgAlpha(topAlpha, bottomAlpha){
        gsap.to(bottomFill, {duration: 0, fillOpacity: topAlpha, overwrite: true});
        gsap.to(topFill, {duration: 0, fillOpacity: bottomAlpha, overwrite: true});
    }

    function spacePP(event){
        // console.log(event.keyCode);
        if(event.keyCode == 32){
            playPause();
        }
    }

    function seekFWD(event){
        // console.log(event.keyCode);
        if(event.keyCode == 39){
            let time = 10;
            if(event.ctrlKey){
                time = time * 2;
            }
            videoSeeker('forward', time);
        }
    }

    function seekBAK(event){
        // console.log(event.keyCode);
        if(event.keyCode == 37){
            let time = 10;
            if(event.ctrlKey){
                time = time * 2;
            }
            videoSeeker('back', time);
        }
    }

    function playPause(){
        noSplash();

        if(document.fullscreenElement || document.mozFullScreenElement){
            video.addEventListener('ended', fullScreenSplash, false);
        }

        if(!video.paused){
            isPlaying = false;
            video.pause();
            video.volume = vol.value / 100;
            if(!ctrlHidden){
                play.style.zIndex = '12';
                play.style.visibility = 'visible';
                pause.style.zIndex = '10';
                pause.style.visibility = 'hidden';
            }
        }
        else{
            isPlaying = true;
            video.play();
            video.volume = vol.value / 100;
            if(!ctrlHidden){
                play.style.zIndex = '10';
                play.style.visibility = 'hidden';
                pause.style.zIndex = '12';
                pause.style.visibility = 'visible';
            }
        }
    }

    function muteVideo(){
        if(video.muted){
            vol.value = video.volume * 100;
            video.muted = false;
            muteVid.style.zIndex = '10';
            muteVid.style.visibility = 'hidden';
            unMuteVid.style.zIndex = '12';
            unMuteVid.style.visibility = 'visible';
            gsap.to(unMute1_btn, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
            gsap.to(unMute2_btn, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
        }else{
            video.muted = true;
            vol.value = 0;
            muteVid.style.zIndex = '12';
            muteVid.style.visibility = 'visible';
            unMuteVid.style.zIndex = '10';
            unMuteVid.style.visibility = 'hidden';
            gsap.to(mute_btn, {duration: 0.5, fill: setCtrl.btnColOver, scale: 1.2, x: -1, y: -1, overwrite: true});
        }
    }

    function fullScreen(){
        
        // console.log(document.querySelector('meta[name="viewport"]').content);
        // screen.orientation.lock("landscape-primary");
        document.querySelector('meta[name="viewport"]').content = "width=device-width, height=device-height, initial-scale=1";
        fsb.removeEventListener('click', fullScreen, false);
        fsb.addEventListener('click', pExitFullScreen, false);
        if(player.requestFullscreen){
          player.requestFullscreen();
        }else if(player.mozRequestFullScreen){
          player.mozRequestFullScreen();
        }else if(player.webkitRequestFullscreen){
          player.webkitRequestFullscreen();
        }else if(player.msRequestFullscreen){
            player.msRequestFullscreen();
        }else{
            mkLog('Your browser does not support full screen.');
        }
        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;

        if(full === undefined || Object.getPrototypeOf(full) !== Object.prototype){
            let aboutBG = document.getElementById('aboutBG');
            let pListHolder = document.getElementById('pListHolder');

            // There are twp elements that might not exist so test for their existence before using
            if(pListHolder !== null){
                pListHolder.style.visibility = 'hidden';
            }
            if(aboutBG !== null){
                aboutBG.style.visibility = 'hidden';
            }

            elmPosFS();
            escFsEvts('mk');
        }
    }


    function rotate(){
        let fsElement = document.fullscreenElement || document.mozFullScreenElement ||
document.webkitFullscreenElement || document.msFullscreenElement;
        console.log(fsElement);
        console.log(screen.orientation.type);
        if(fsElement !== null){
            elmPosFS();
        }
    }


    function pExitFullScreen(){
        document.querySelector('meta[name="viewport"]').content = metaScale;
        console.log(document.querySelector('meta[name="viewport"]').content);
        fsb.removeEventListener('click', pExitFullScreen, false);
        fsb.addEventListener('click', fullScreen, false);

        if(document.exitFullscreen){
            document.exitFullscreen();
        }else if(document.webkitExitFullscreen){
            document.webkitExitFullscreen();
        }else if(document.mozCancelFullScreen){
            document.mozCancelFullScreen();
        }else if(document.msExitFullscreen){
            document.msExitFullscreen();
        }else{
            mkLog('Your browser does not support full screen.');
        }

        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        
        if(full === undefined || Object.getPrototypeOf(full) === Object.prototype){
            let aboutBG = document.getElementById('aboutBG');
            let pListHolder = document.getElementById('pListHolder');
            aboutBG.style.visibility = 'visible';
            if(pListHolder !== null){
                // pListholder will not exist if there is only one video.
                pListHolder.style.visibility = 'visible';
            }
            elmPosNorm();
            document.getElementById(player.id).scrollIntoView();
        }
    }
    
    function escFsEvts(doWhat){
        let fSh = ['fullscreenchange', 'mozfullscreenchange', 'MSFullscreenChange', 'webkitfullscreenchange'];
        
        if(doWhat == 'mk'){
            for(let fH in fSh){
                document.addEventListener(fSh[fH], escBtn, false);
            }
        }else{
            for(let fH in fSh){
                document.removeEventListener(fSh[fH], escBtn, false);
            }
        }
    }

    function escBtn(){
        let full = document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
        if(full === null || full === undefined || Object.getPrototypeOf(full) === Object.prototype){
            if(player.style.height == wH + 'px' || video.style.width == wW + 'px'){
                pExitFullScreen();
                escFsEvts('rm');
                document.getElementById(player.id).scrollIntoView();
            }
        }
    }

    function videoSeeker(direction, time){

        let uMoveSeek = video.duration * (seek.value / 100);

        if(direction != undefined && ['back', 'forward'].indexOf(direction) != -1){
            if(time != undefined && time %1 === 0){
                if(direction == 'forward'){
                    uMoveSeek = video.currentTime + time;
                }else{
                    uMoveSeek = video.currentTime - time;
                }
            }
        }

        video.currentTime = uMoveSeek;
    }

    function getProgress(){
        let bufferedEnd;
        let totalBuff = 0;
        let duration = video.duration;

        if(video.buffered.length - 1 >= 0){
            bufferedEnd = video.buffered.end(video.buffered.length - 1);
            timeTrack();
        }

        for(i = 0; i <= video.buffered.length - 1; i++){
            let bufLeft = Math.round(video.buffered.start(i) / duration * 100);
            let endLessStart = video.buffered.end(i) - video.buffered.start(i);
            let bufWidth = Math.round(endLessStart / duration * 100);

            totalBuff = totalBuff + endLessStart;

            if(document.getElementById('bufProg_' + i) === null){
                let buffedProg = document.createElement('div');
                buffedProg.id = 'bufProg_' + i;
                buffedProg.className = 'buffProg';
                buffedOut.appendChild(buffedProg);
                buffedProg.style.position = 'absolute';

                // console.log('hereThere = '+setCtrl.bufferColor);
                // console.log(Object.prototype.toString.call(setCtrl.bufferColor));
                if(Object.prototype.toString.call(setCtrl.bufferColor) === '[object Array]'){
                    buffedProg.style.backgroundColor = randClor(setCtrl.bufferColor);
                }else if(setCtrl.bufferColor == 'rainbow'){
                    console.log(setCtrl.bufferColor);
                    buffedProg.style.backgroundColor = randClor();
                }else{
                    console.log('fuck');
                    buffedProg.style.backgroundColor = setCtrl.bufferColor;
                }

                mkLog('Progress Buffer backgroundColor = '+buffedProg.style.backgroundColor);
                
                buffedProg.style.height = buffedOut.style.height;
                buffedProg.style.alpha = 0.75;
                buffedProg.style.bottom = '0px'
                buffedProg.style.display = 'inline-block';
                buffedProg.style.left = bufLeft + '%';
                buffedProg.style.width = bufWidth + '%';
            }
            else{
                let buffedProg = document.getElementById('bufProg_' + i);
                buffedProg.style.left = bufLeft + '%';
                buffedProg.style.width = bufWidth + '%';
            }

            if(userA == 'firefox' && Math.ceil(totalBuff) >= (Math.floor(duration) * 0.95)){
                // This li'l ack is 4 fFox mate. fF wont tell ya fuck all about being done!
                // so ya's got's to's lead's dem round's by their dick and pretend that 95% of something
                // is good enough!
                let buffedProg = document.getElementById('bufProg_' + i);
                buffedProg.style.width = '100%';
            }
        }
    }

    function randClor(colArr){
        if(colArr === undefined || Object.prototype.toString.call(colArr) !== '[object Array]'){
            let leterColor = ['A', 'B', 'C', 'D', 'E', 'F'];
            let newColor = '';
            for(i = 6; i >= 1; i--){
                if((Math.floor(Math.random() * (1+498-1)) + 1) %2 === 0){
                    newColor = newColor + leterColor[Math.floor(Math.random() * 6)];
                }else{
                    newColor = newColor + Math.floor(Math.random() * 10);
                }
            }
            return '#' + newColor;
        }
        else{
            let newArr = Array();
            for(let color in colArr){
                if(colArr[color].match(/^#?[A-Fa-f0-9]{6}[\s]{0,1}$/g)){
                    if(colArr[color].substring(0, 1) !== '#'){
                        newArr.push('#'+colArr[color].toUpperCase());
                    }else{
                        newArr.push(colArr[color].toUpperCase());
                    }
                }
            }
            return newArr[randNum(0, newArr.length - 1)];
        }
    }

    function randNum(low, high){
        return Math.floor(Math.random() * (1+high-low)) + low;
    }

    function volumeSlider(){
        video.volume = vol.value / 100;
    }

    function timeTrack(){
        if(video.ended){
            video.currentTime = 0;
            isPlaying = false;
        }

        let now = video.currentTime * (100 / video.duration);
        let nowMin = Math.floor(video.currentTime / 60);
        let nowSec = Math.floor(video.currentTime - nowMin * 60);
        let nowHr = Math.floor(nowMin / 60);
        let totalMin = Math.floor(video.duration / 60);
        let totalSec = Math.round(video.duration - totalMin * 60);
        let totalHr = Math.floor(totalMin / 60);
        if(!isNaN(totalSec)){
            seek.value = now;

            if(nowSec < 10){
                nowSec = '0'+nowSec;
            }
            if(totalSec < 10){
                totalSec = '0'+totalSec;
            }
            if(nowMin < 10){
                nowMin = '0'+nowMin;
            }
            if(totalMin < 10){
                totalMin = '0'+totalMin;
            }
            if(totalHr >= 1){
                if(nowMin >= 60){
                    nowMin = nowMin - (60 * totalHr);
                    if(nowMin < 10){
                        nowMin = '0'+nowMin;
                    }
                }
                
                totalMin = totalMin - (60 * totalHr);
                timeTxt.innerHTML = nowHr+':'+nowMin+':'+nowSec+' | ';
                totalTxt.innerHTML = totalHr+':'+totalMin+':'+totalSec;
            }else{
                timeTxt.innerHTML = nowMin+':'+nowSec+' | ';
                totalTxt.innerHTML = totalMin+':'+totalSec;
            }
        }
        else{
            seek.value = 1;
        }
    }

    function shoVidCtrl(){
        ctrlHidden = false;
        video.style.cursor = 'default';
        ctrl.style.visibility = 'visible';
        ctrlFade_tween.play();
        video.style.cursor = 'default';
    }

    function hideVidCtrl(){
        ctrlHidden = true;
        ctrlFade_tween.reverse();
        video.style.cursor = 'none';
    }

    let volSizR_tween = gsap.fromTo(volCtrl, {duration: 0.5, height: mkInt(btnH), ease: Power4.easeOut},
                                             {duration: 0.5, height: (mkInt(playerH) * 0.35), ease: Power2.easeIn});

    volSizR_tween.reverse();

    function shoVol(){
        volSizR_tween.play();
    }

    function hideVol(){
        volSizR_tween.reverse();
    }

    function mkInt(str){
        str = str.replace("px", "").replace("%", ""); 
        return Number(str);
    }

    function mkLog(txt){
        if(verbose){
            console.log(txt);
        }
    }

    function getUserAgent(listAvailableUAs){
        if(listAvailableUAs === undefined || listAvailableUAs === null || listAvailableUAs !== true){
            listAvailableUAs = false;
        }
        let userArr = {
            chrome: /Chrome/.test(navigator.userAgent),
            safari: /Safari/.test(navigator.userAgent),
            iPad: /iPad/.test(navigator.userAgent),
            iPhone: /iPhone/.test(navigator.userAgent),
            droid: /Android/.test(navigator.userAgent),
            firefox: /Firefox/.test(navigator.userAgent)
        };
        let uaList = '';
        let noUserAgent;
        for(let obj in userArr){
            if(listAvailableUAs){
                if(uaList === ''){
                    uaList = obj;
                }else{
                    uaList = uaList + ', ' + obj;
                }
            }else{
                if(userArr[obj]){
                    return obj;
                }else{
                    noUserAgent = true;
                }
            }
        }
        if(listAvailableUAs){
            return uaList;
        }
        if(noUserAgent){
            return 'The User-Agent your looking for is not in the list of User-Agents to search for. You can add it manually in the getUserAgent.userArr Object.';
        }
    }

    function scrolTxtPos(holdWidth, txtWidth){
        if(holdWidth < txtWidth){
            let txtRight = txtWidth - holdWidth;
            let txtPad = holdWidth * 0.25;
            return -(txtRight + txtPad)+ 'px';
        }else{
            return '3px';
        }
    }

    function elmPosNorm(){
        // video (html video element)

        showSplash = false;

        // destroy the splash if it exists.
        noSplash();
        if(document.getElementById('showSplash_btn')){
            document.getElementById('showSplash_btn').removeEventListener('click', splashAction, false);
            sideCtrl.removeChild(document.getElementById('splashBG'));
        }

        // remove event listener for video end
        video.removeEventListener('ended', fullScreenSplash);

        video.style.height = playerH;
        video.style.width = playerW;

        // player (div), holds the video and the controls
        player.style.height = playerH;
        player.style.width = playerW;
        player.style.top = playerT;
        player.style.left = playerL;
        
        // ctrl (div), the complete controls div
        ctrl.style.width = mkInt(playerW) + 'px';
        ctrl.style.height = mkInt(btnH) + 10 + 'px';
        ctrl.style.position = 'absolute';
        ctrl.style.bottom = '0px';
        ctrl.style.left = '0px';

        // ctrls (div), the left controls, play, seek, time
        ctrls.style.width = playerW;
        ctrls.style.height = ctrl.style.height;

        // vidCtrlBG (div), the background for the controls
        vidCtrlBG.style.width = ctrl.style.width;
        vidCtrlBG.style.height = ctrl.style.height;

        // sideCtrl (div), holds -> volCtrl, sizeCtrl, fsb
        sideCtrl.style.bottom = '0px';
        sideCtrl.style.right = '0px';
        sideCtrl.style.height = btnH;

        // volCtrl (div), holds -> mute, un-mute, volume slider
        volCtrl.style.bottom = '0px';

        // sizeCtrl (div), holds -> resize btn and size controls.
        // size controls are added from ajax 
        sizeCtrl.style.left = mkInt(muteVid.style.width) + 5 +'px';
        sizeCtrl.style.bottom = volCtrl.style.bottom;

        // play (img:button)
        play.style.top = '0px';
        play.style.left = '0px';

        // pause (img:button)
        pause.style.top = '0px';
        pause.style.left = '0px';

        // fsb:FullScreen (img:button)
        fsb.style.left = mkInt(sizeCtrl.style.left) + mkInt(sizeCtrl.style.width) + 'px';
        fsb.style.bottom = '0px';

        muteVid.style.bottom = '0px';
        unMuteVid.style.bottom = '0px';

        // vol (input:range), controls the volume
        vol.style.bottom = mkInt(muteVid.style.height) + mkInt(volCtrl.style.height) - mkInt(btnH) + setCtrl.volOffset + 'px';
        vol.style.left = mkInt(muteVid.style.width) / 50 + 'px';

        // seek (input:range), controls the video scrubber
        seek.style.left = mkInt(play.style.left) + mkInt(play.style.width) + 10 + 'px';
        seek.style.top = (mkInt(play.style.height) * .5) + mkInt(seek.style.height) +'px';
        seek.style.width = mkInt(ctrl.style.width) - mkInt(sideCtrl.style.width) - mkInt(play.style.width) - 20 + 'px';

        // buffedOut (div:container). Holds the buffer objects
        buffedOut.style.position = seek.style.position;
        buffedOut.style.top = mkInt(seek.style.top) + 1 + 'px';
        buffedOut.style.left = mkInt(seek.style.left) + 2 + 'px';
        buffedOut.style.zIndex = seek.style.zIndex - 1;
        buffedOut.style.height = '10px';
        buffedOut.style.width = seek.style.width;
        buffedOut.style.margin = '5px 0px 5px 0px';

        // Minor fix for webkit browsers
        if(userA === 'chrome' || userA === 'safari' || userA === 'opera'){
            seek.style.top = '18px';
            buffedOut.style.top = mkInt(seek.style.top) - 4 + 'px';
        }
        else{
            // Minor fix for firefox
            seek.style.top = (mkInt(play.style.height) * .5) + mkInt(seek.style.height) + 3 +'px';
        }

        // time (div), holds the time spans [0:25:32 : 1:27:49]
        time.style.top = '-2px';
        time.style.right = mkInt(sideCtrl.style.right) + mkInt(sideCtrl.style.width) + (mkInt(btnW) * 1.25) + 'px';
    }

    function elmPosFS(){
        // console.log(screen.width);
        setTimeout(null, 300);
        shoMkSplash();
        let fPad = 8;
        player.style.top = '0px';
        player.style.left = '0px';
        player.style.width = screen.width + 'px';
        player.style.height = screen.height + 'px';
        video.style.width = screen.width + 'px';
        video.style.height = screen.height + 'px';
        ctrl.style.width = screen.width + 'px';
        ctrl.style.bottom = '0px';
        ctrls.style.width = screen.width + 'px';
        vidCtrlBG.style.width = screen.width + 'px';
        seek.style.left = mkInt(seek.style.left) + fPad + 'px';
        seek.style.width = wW - (mkInt(sideCtrl.style.width) + mkInt(seek.style.left)) - (fPad * 2) + 'px';
        time.style.top = '-2px';
        buffedOut.style.left = mkInt(buffedOut.style.left) + fPad + 'px';
        buffedOut.style.width = seek.style.width;
        // listen for end of playback
        video.addEventListener('ended', fullScreenSplash, false);

    }

    function fullScreenSplash(){

        noSplash();
        showSplash = true;
        gsap.to(ellipse, {duration: 0.1, scaleX: 1.0, scaleY: 1.0,
                          x: 0, y: 0, overwrite: true});

        shoVidCtrl();
        clearTimeout(mouseIdel);

        let cTitle = document.createElement('div');
        cTitle.id = 'fsTitle';
        cTitle.className = 'aboutBG';
        cTitle.style.textShadow = '0px 0px, 40px, #000000';
        cTitle.style.position = 'absolute';
        cTitle.style.overflow = 'visible';
        cTitle.style.color = setCtrl.nowPlayColor;
        if(isDark(setCtrl.nowPlayColor, 111)){
            cTitle.style.textShadow = "0px 0px 20px #ffffff";
        }else{
            cTitle.style.textShadow = "0px 0px 20px #000000";
        }

        let cAbout = document.createElement('div');
        cAbout.id = 'aboutVidTxt_FS';
        cAbout.className = 'aboutVidTxt_FS';
        cAbout.style.position = 'absolute';
        cAbout.style.display = 'block';

        let aboutHold = document.createElement('div');
        aboutHold.style.position = 'absolute';
        aboutHold.style.overflow = 'auto';

        let padding = 8;

        let imgW = 0;

        let imgH = 0;

        let widestImg = 0;

        let tallestImg = 0;

        let vidArr = Array();

        let imgArr = Array();

        let aboutTxt = '';

        // loop through objct to find all video objects and setup.
        for(let obj in objct){ 
            if(obj !== 'settings'){
                // console.log(objct[obj]);
                imgW = imgW + Object.values(objct[obj].images)[0].width;

                vidArr.push(objct[obj]);

                if(Object.values(objct[obj].images)[0].width > widestImg){
                    widestImg = Object.values(objct[obj].images)[0].width;
                    imgH = Object.values(objct[obj].images)[0].height;
                }

                if(objct[obj].loc.includes(video.id)){
                    cTitle.innerHTML = objct[obj].name;
                    aboutTxt = objct[obj].about;
                    imgArr = objct[obj].images;
                }

            }
        }

        if(imgW%Object.keys(imgArr).length !== 0){
            // Not all widths of the video placholders images are lekely not the same width.
            // This is why we created the widestImg variable. Im leaving this here because
            // I have never tried to use a video that was taken in portrate mode and I'm
            // not sure if this will come in handy yet!
            imgW = widestImg;
        }else{
            imgW = widestImg;
        }

        let mainBG = document.createElement('div');
        mainBG.style.position = 'absolute';
        mainBG.style.zIndex = mkInt(player.style.zIndex) + 1;
        mainBG.style.height = wH * 0.9 + 'px';
        mainBG.style.width = wW * 0.95 + 'px';
        mainBG.style.top = (wH - mkInt(mainBG.style.height)) * 0.5 + 'px';
        mainBG.style.left = (wW - mkInt(mainBG.style.width))* 0.5 + 'px';
        mainBG.id = 'splash';

        let bgTint = document.createElement('div');
        bgTint.style.position = 'absolute';
        bgTint.style.zIndex = mainBG.style.zIndex;
        bgTint.style.top = '0px';
        bgTint.style.left = '0px';
        bgTint.style.width = mainBG.style.width;
        bgTint.style.height = mainBG.style.height;
        bgTint.style.background = '#000000';
        bgTint.style.opacity = 0.3333333333;
        bgTint.style.borderRadius = '15px 15px 15px 15px';

        mainBG.appendChild(bgTint);

        player.appendChild(mainBG);

        cTitle.style.zIndex = mkInt(mainBG.style.zIndex )+ 1;
        mainBG.appendChild(cTitle);

        cTitle.style.fontSize = wH*0.15 + '%';
        cTitle.style.top = mkInt(mainBG.style.top) + (padding * 4) + 'px';
        cTitle.style.left = (mkInt(mainBG.style.left) + ((mkInt(mainBG.style.width) * 0.45) - getTextWidth(cTitle.innerHTML, cTitle.style.fontSize +font_family)) * 0.5) + 'px';

        mkAbout(aboutTxt, cAbout)

        aboutHold.style.height = mkInt(mainBG.style.height) * 0.80 + 'px';
        aboutHold.style.width = mkInt(mainBG.style.width) * 0.46 + 'px';
        aboutHold.style.top = mkInt(mainBG.style.height) * 0.18 + 'px';
        aboutHold.style.left = (mkInt(mainBG.style.left) + (mkInt(mainBG.style.width) * 0.45)) - mkInt(aboutHold.style.width) + 'px';

        cAbout.style.width = mkInt(aboutHold.style.width) * 0.98 + 'px';
 
        cAbout.style.top = '0px';
        cAbout.style.left = '0px';
        cAbout.style.color = '#ffffff';
        cAbout.style.fontSize = wH*0.1 + '%';
        
        aboutHold.appendChild(cAbout);
        mainBG.appendChild(aboutHold);

        let rightSide = (mkInt(mainBG.style.left) + Math.floor(mkInt(mainBG.style.width) * 0.45)) + Math.floor(mkInt(mainBG.style.width) - ((mkInt(mainBG.style.left) + (mkInt(mainBG.style.width) * 0.45)) * 2))

        let vSplash = document.createElement('div');
        vSplash.style.position = 'absolute';
        vSplash.style.overflow = 'auto';
        vSplash.style.width = mkInt(mainBG.style.width) * 0.46 + 'px';
        vSplash.style.height = imgH * 2 + 'px';
        vSplash.style.top = mkInt(mainBG.style.top) + (mkInt(mainBG.style.height) * 0.93) - mkInt(vSplash.style.height) + 'px';
        vSplash.style.left = rightSide + 'px';

        let vHolder = document.createElement('div');
        vHolder.id = 'vHold-here';
        vHolder.style.position = 'absolute';
        vHolder.style.width = Math.floor(mkInt(vSplash.style.width) * 0.98) + 'px';
        vHolder.style.top = '0px';
        vHolder.style.left = '0px';
        vHolder.style.zIndex = mkInt(mainBG.style.zIndex) + 1;

        vSplash.appendChild(vHolder);
        mainBG.appendChild(vSplash);

        let vX = 0;//Math.round(mkInt(vHolder.style.left));
        let vY = -Math.abs(imgH);
        // console.log('here --> x = '+vX+", y = "+vY);

        for(let x in vidArr){
            // console.log(vidArr[x]);
            if(x !== 0 && x%Math.floor(mkInt(vHolder.style.width) / imgW) === 0){
                // here we need to figure out some position based on size
                if(mkInt(vHolder.style.width) / imgW < 2){
                    // our image is so big that we only have space for one so we need to resize/pos 
                    // its holder div and center the image in it.
                    // console.log(x);
                    vSplash.style.height = imgH * 1.5 + 'px';
                    vSplash.style.top = mkInt(mainBG.style.top) + (mkInt(mainBG.style.height) * 0.93) - mkInt(vSplash.style.height) + 'px';
                    vY = vY + imgH + padding;
                    vX = ((mkInt(vHolder.style.width) - imgW) * 0.5) + padding;
                }else{
                    // so the images can be set up into rows in a div. Here we will calculate our height
                    // and reset our left most x position for our new row.
                    // console.log(x);
                    vY = vY + imgH + padding;
                    vX = padding;
                }
            }

            let vObj = document.createElement('div');
            vObj.style.position = 'absolute';
            vObj.style.top = vY + 'px';
            vObj.style.left = vX + 'px';
            vObj.style.overflow = 'hidden';
            vObj.style.width = Object.values(vidArr[x].images)[0].width + 'px';
            vObj.style.height = Object.values(vidArr[x].images)[0].height + 'px';

            // Create the image element
            let img = document.createElement('img');
            img.id = vidArr[x].name+'_vPic';
            img.style.position = 'absolute';
            img.style.width = vObj.style.width;
            img.style.height = vObj.style.height;
            img.style.top = '0px';
            img.style.left = '0px';
            img.style.zIndex = mkInt(vHolder.style.zIndex) + 1;
            vObj.appendChild(img);

            let vidTitle = document.createElement('p');
            vidTitle.id = strUnFix(vidArr[x].name+'_vTitle');
            vidTitle.className = 'posterTitle';
            vidTitle.style.position = 'absolute';
            vidTitle.style.bottom = '0px';
            vidTitle.innerHTML = vidArr[x].name;
            vidTitle.style.zIndex = '15';
            vidTitle.style.overflow = 'visible';
            vidTitle.style.fontSize = 12;
            vidTitle.style.color = '#ffffff';

            if(nowPlay !== null && nowPlay !== undefined){
                if(strUnFix(nowPlay) == strUnFix(vidArr[x].name+'_title')){
                    vidTitle.style.color = setCtrl.nowPlayColor;
                }
            }
            
            vidTitle.style.width = getTextWidth(vidArr[x].name, vidTitle.style.fontSize + 'pt '+font_family) + 'px';
            vidTitle.zIndex = mkInt(img.style.zIndex) + 1;

            let titleHold = document.createElement('div');
            titleHold.id = vidArr[x].name+'_titleHold';
            titleHold.className = 'vidTitleHolder';
            titleHold.style.position = 'absolute';
            titleHold.style.bottom = '0px';
            titleHold.style.left = '0px';
            titleHold.style.width = mkInt(vObj.style.width) + 'px';
            titleHold.style.height = vidArr[x].images.p_0.height * .25 + 'px';
            titleHold.style.zIndex = '14';
            titleHold.style.backgroundColor = '#000000';
            titleHold.style.opacity = 0.7;
            titleHold.zIndex = mkInt(vidTitle.style.zIndex) + 1;

            titleHold.appendChild(vidTitle);
            vObj.appendChild(titleHold);

            // Create blank
            let blank = document.createElement('div');
            blank.id = vidArr[x].name+'_id';
            blank.style.position = 'absolute';
            blank.style.width = vObj.style.width;
            blank.style.height = vObj.style.height;
            blank.style.top = '0px';
            blank.style.left = '0px';
            blank.style.zIndex = mkInt(vidTitle.style.zIndex) + 1;

            vObj.appendChild(blank);

            blank.addEventListener('click', vidChoice, false);
            blank.addEventListener('mouseover', vidOver, false);
            blank.addEventListener('mouseout', vidOut, false);

            vX = vX + imgW + padding;

            vHolder.appendChild(vObj);

            img.style.visibility = 'hidden';
            let tLoc = getTopDir();
            if(tLoc === ''){
                img.src = Object.values(vidArr[x].images)[0].loc;
            }
            else{
                let pLoc = tLoc+'/';
                img.src = pLoc+Object.values(vidArr[x].images)[0].loc;
            }
            
            img.addEventListener('load', function(){
                img.style.visibility = 'visible';
                gsap.from(img, {opacity: 0,
                                ease: Power2.easeOut,
                                duration: 0.5});
            });
        }

        let cPics = document.createElement('div');
        cPics.id = 'slideShow'
        cPics.style.position = 'absolute';
        cPics.style.overflow = 'hidden';
        cPics.style.width = imgW * 1.25 + 'px';
        cPics.style.height = imgH * 1.25 + 'px';
        cPics.style.top = mkInt(mainBG.style.top) + ((mkInt(mainBG.style.height) - mkInt(vSplash.style.height) - mkInt(cPics.style.height)) * 0.5) + 'px';
        cPics.style.left = rightSide + (mkInt(vSplash.style.width) - mkInt(cPics.style.width)) * 0.5 + 'px';

        mainBG.appendChild(cPics);

        mkSlide(cPics, imgArr, cTitle.innerHTML);
        
        function vidChoice(e){
            let tgt = e.target || e.srcElement || window.event;
            // console.log(objct[strUnFix(tgt.id).replace('_id', '')]);
            let obj = objct[strUnFix(tgt.id).replace('_id', '')];
            
            if(nowPlay !== null || nowPlay !== undefined){
                document.getElementById(strUnFix(nowPlay)).style.color = '#fff';
                document.getElementById(strUnFix(nowPlay.replace('_title', '_vTitle'))).style.color = '#fff';
            }

            nowPlay = obj.name+'_title';
            document.getElementById(strUnFix(nowPlay)).style.color = setCtrl.nowPlayColor;
            video.id = strUnFix(obj.name);
            document.getElementById(strUnFix(obj.name+'_vTitle')).style.color = setCtrl.nowPlayColor;
            vidSwich(getTopDir(), obj);
            document.getElementById('fsTitle').innerHTML = obj.name;
            document.getElementById('aboutVidName').innerHTML = obj.name;
            mkAbout(obj.about, document.getElementById('aboutVidTxt_FS'));
            mkAbout(obj.about, document.getElementById('aboutVidTxt'));
            mkSlide(document.getElementById('slideShow'), obj.images, obj.name);
        }

        function vidOver(e){
            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_id', '_vPic');
            let txt = document.getElementById(strUnFix(tgt.id.replace('_id', '_vTitle')));
            let txtHoldWidth = mkInt(txt.style.width);
            let theWidth = mkInt(document.getElementById(tgt.id.replace('_id', '_titleHold')).style.width);
            txtScroller(txt, txtHoldWidth, theWidth);
            let pic = document.getElementById(tId);
            gsap.to(pic, {duration: 0.1, scaleX: 1.15, scaleY: 1.15, overwrite: true});
        }

        function vidOut(e){
            let tgt = e.target || e.srcElement || window.event;
            let tId = tgt.id.replace('_id', '_vPic');
            let pic = document.getElementById(tId);
            gsap.to(pic, {duration: 0.1, scaleX: 1, scaleY: 1, overwrite: true});
        }
    }

    function mkSlide(mDiv, pics, title){

        let tW = mkInt(mDiv.style.width);
        let tH = mkInt(mDiv.style.height);

        let btnS = Array();

        if(document.getElementById('mkSlideShow') !== null && document.getElementById('mkSlideShow') !== undefined){
            let slideShow = document.getElementById('mkSlideShow');
            slideShow.parentNode.removeChild(slideShow);
        }

        let slideShow = document.createElement('div');
        slideShow.id = 'mkSlideShow';
        slideShow.style.position = 'absolute';
        slideShow.style.width = tW + 'px';
        slideShow.style.height = tH + 'px';
        slideShow.style.top = '0px';
        slideShow.style.left = '0px';

        let bg = slideShow.cloneNode(true);
        bg.style.backgroundColor = '#ffffff';
        bg.style.boxShadow = '0px 0px 40px 10px #ffffff';
        bg.style.opacity = 0;
        bg.style.zIndex = 1;

        slideShow.appendChild(bg);

        let plusHolder = document.createElement('div');
        plusHolder.style.position = 'absolute';
        plusHolder.style.width = tW * 0.1 + 'px';
        plusHolder.style.height = tH * 0.1 + 'px';
        plusHolder.style.top = tH - mkInt(plusHolder.style.height) + 'px';
        plusHolder.style.left = tW - mkInt(plusHolder.style.width) + 'px';
        plusHolder.style.zIndex = '50';

        let plus = document.createElement('div');
        plus.id = 'fwdBtn';
        plus.style.width = btnW;
        plus.style.height = btnH;
        plus.style.position = 'absolute';
        plus.style.visibility = 'visible';
        plus.style.top = '0px';
        plus.style.left = '0px';

        var plus_bg = play_bg.cloneNode(true);

        let plusBtn = document.createElementNS(xmlns, 'svg');
        plusBtn.setAttribute('xlmns', xmlns);
        plusBtn.setAttribute('viewBox', '0 0 20 15');
        plusBtn.setAttribute('preserveAspectRatio', 'none');
        plusBtn.setAttribute('height', mkInt(btnH));
        plusBtn.setAttribute('width', mkInt(btnW));
        plusBtn.setAttribute('x', '0px');
        plusBtn.setAttribute('y', '0px');
        plusBtn.style.position = 'absolute';

        let plus_btn = document.createElementNS(xmlns, 'path');
        plus_btn.setAttribute('d', 'M 6.2128906,3.9746094 V 4.0761719 L 8.7460938,7.7871094 6.2128906,11.5 v 0.142578 L 14.125,7.8085938 Z');
        plus_btn.setAttribute('fill', setCtrl.btnColUp);
        plus_btn.setAttribute('id', 'plus_btn');
        plus_btn.style.zIndex = 13;

        let plusTop = document.createElement('div');
        plusTop.id = 'plusTop';
        plusTop.style.height = btnH;
        plusTop.style.width = btnW;
        plusTop.style.position = 'absolute';
        plusTop.style.zIndex = '15';
        plusTop.style.top = '0px';
        plusTop.style.left = '0px';
        plusTop.style.padding = '5px';

        plus.appendChild(plusTop);
        plusBtn.appendChild(plus_btn);
        plus.appendChild(plusBtn);
        plus.appendChild(plus_bg);

        plusHolder.appendChild(plus);
        slideShow.appendChild(plusHolder);

        btnS.push(plus);

        let minusHolder = document.createElement('div');
        minusHolder.style.position = 'absolute';
        minusHolder.style.width = tW * 0.1 + 'px';
        minusHolder.style.height = tH * 0.1 + 'px';
        minusHolder.style.top = tH - mkInt(minusHolder.style.height) + 'px';
        minusHolder.style.left = mkInt(minusHolder.style.width) - mkInt(minusHolder.style.width) + 'px';
        minusHolder.style.zIndex = '5';
        minusHolder.style.transform="scale(-1,1)";

        let minus = document.createElement('div');
        minus.id = 'bckBtn';
        minus.style.width = btnW;
        minus.style.height = btnH;
        minus.style.position = 'absolute';
        minus.style.visibility = 'visible';
        minus.style.top = '0px';
        minus.style.left = '0px';

        var minus_bg = play_bg.cloneNode(true);

        let minusBtn = document.createElementNS(xmlns, 'svg');
        minusBtn.setAttribute('xlmns', xmlns);
        minusBtn.setAttribute('viewBox', '0 0 20 15');
        minusBtn.setAttribute('preserveAspectRatio', 'none');
        minusBtn.setAttribute('height', mkInt(btnH));
        minusBtn.setAttribute('width', mkInt(btnW));
        minusBtn.setAttribute('x', '0px');
        minusBtn.setAttribute('y', '0px');
        minusBtn.style.position = 'absolute';

        let minus_btn = document.createElementNS(xmlns, 'path');
        minus_btn.setAttribute('d', 'M 6.2128906,3.9746094 V 4.0761719 L 8.7460938,7.7871094 6.2128906,11.5 v 0.142578 L 14.125,7.8085938 Z');
        minus_btn.setAttribute('fill', setCtrl.btnColUp);
        minus_btn.setAttribute('id', 'minus_btn');
        minus_btn.style.zIndex = 13;

        let minusTop = document.createElement('div');
        minusTop.id = 'minusTop';
        minusTop.style.height = btnH;
        minusTop.style.width = btnW;
        minusTop.style.position = 'absolute';
        minusTop.style.zIndex = '15';
        minusTop.style.top = '0px';
        minusTop.style.left = '0px';
        minusTop.style.padding = '5px';

        minus.appendChild(minusTop);
        minusBtn.appendChild(minus_btn);
        minus.appendChild(minusBtn);
        minus.appendChild(minus_bg);

        minusHolder.appendChild(minus);
        slideShow.appendChild(minusHolder);
        mDiv.appendChild(slideShow);

        btnS.push(minus);

        // create the button mouseover:out listeners and events
        for(let btn in btnS){
            let btnDiv = btnS[btn];
            btnDiv.addEventListener('mouseover', btnOver, false);
            btnDiv.addEventListener('mouseout', btnOut, false);
            btnDiv.addEventListener('click', picIndex, false);
        }

        // create the slideShow image
        let obj = Object.values(pics);
        let img = document.createElement('img');
        img.style.visibility = 'hidden';

        img.id = title + '_' + (obj.length - 1);
        let tLoc = getTopDir();
        if(tLoc === ''){
            img.src = obj[obj.length - 1].loc;
        }
        else{
            let pLoc = tLoc+'/';
            img.src = pLoc+obj[obj.length - 1].loc;
        }
        
        img.style.width = obj[obj.length - 1].width * 1.1 + 'px';
        img.style.height = obj[obj.length - 1].height * 1.1 + 'px';

        img.addEventListener('load', function(){
            img.style.visibility = 'visible';
            gsap.from(img, {scaleX: 0,
                            scaleY: 0,
                            opacity: 0,
                            ease: Power2.easeOut,
                            duration: 0.5});
        });

        let mainImg = document.createElement('div');
        mainImg.id = 'imgHolder';
        mainImg.style.position = 'absolute';
        mainImg.style.width = mkInt(img.style.width) + 'px';
        mainImg.style.height = mkInt(img.style.height) + 'px';
        mainImg.style.top = (tH - mkInt(mainImg.style.height)) * 0.25 + 'px';
        mainImg.style.left = (tW - mkInt(mainImg.style.width)) * 0.5 + 'px';

        mainImg.appendChild(img);
        slideShow.appendChild(mainImg);
    }

    function picIndex(e){
        let tgt = e.target || e.srcElement || window.event;
        let pName = tgt.id.replace('Top', '');

        let mainDiv = document.getElementById('mkSlideShow');
        let imgHold = document.getElementById('imgHolder');

        if(pName == 'minus'){
            imgTrac(imgHold, 'left', mainDiv);
        }else{
            imgTrac(imgHold, 'right', mainDiv);
        }
    }

    function imgTrac(iDiv, direction, stage){
        let tW = mkInt(stage.style.width);
        let tH = mkInt(stage.style.height);
        let img = iDiv.children[0];

        let time = 1;

        let pIndex = mkInt(img.id.split('_')[1]);
        let pKey = img.id.split('_')[0];

        let pObj = objct[strUnFix(pKey)].images;
        let imgcount = Object.values(pObj).length;

        let tLoc = getTopDir();

        if(direction == 'left'){
            // the minus function. 
            if(mkInt(iDiv.style.left) == (mkInt(stage.style.width) - mkInt(iDiv.style.width)) * 0.5){

                pIndex--;

                if(pIndex < 0){
                    // pIndex = Object.values(pObj).length - 1;
                    pIndex = imgcount - 1;
                }else if(pIndex >= imgcount){
                    pIndex = 0;
                }
                
                gsap.to(iDiv, {
                            left: -Math.abs(mkInt(iDiv.style.width) + 10) + 'px',
                            duration: 1,
                            ease: Power2.easeOut,
                            onComplete: function(){
                                iDiv.style.left = tW + 2 +'px';
                                if(tLoc === ''){
                                    img.src = Object.values(pObj)[pIndex].loc;
                                }
                                else{
                                    let pLoc = tLoc+'/';
                                    img.src = pLoc+Object.values(pObj)[pIndex].loc;
                                }

                                img.addEventListener('load', function(){
                                    img.id = pKey +'_'+ pIndex;
                                    img.style.height = Object.values(pObj)[pIndex].height * 1.1 + 'px';
                                    img.style.width = Object.values(pObj)[pIndex].width * 1.1 + 'px';
                                    gsap.to(iDiv, {
                                        left: (tW - mkInt(img.style.width)) * 0.5 + 'px',
                                        duration: 1,
                                        ease: Power2.easeIn
                                    });

                                });
                           }
                });
            }
        }
        if(direction == 'right'){
            if(mkInt(iDiv.style.left) == (mkInt(stage.style.width) - mkInt(iDiv.style.width)) * 0.5){

                pIndex++;

                if(pIndex < 0){
                    pIndex = imgcount - 1
                }else if(pIndex >= imgcount){
                    pIndex = 0;
                }
                
                gsap.to(iDiv, {
                            left: tW + 10 + 'px',
                            duration: 1,
                            ease: Power2.easeOut,
                            onComplete: function(){
                                iDiv.style.left = -Math.abs(mkInt(iDiv.style.width) + 10) + 'px';
                                if(tLoc === ''){
                                    img.src = Object.values(pObj)[pIndex].loc;
                                }
                                else{
                                    let pLoc = tLoc+'/';
                                    img.src = pLoc+Object.values(pObj)[pIndex].loc;
                                }

                                img.addEventListener('load', function(){
                                    img.id = pKey +'_'+ pIndex;
                                    img.style.height = Object.values(pObj)[pIndex].height * 1.1 + 'px';
                                    img.style.width = Object.values(pObj)[pIndex].width * 1.1 + 'px';
                                    gsap.to(iDiv, {
                                        left: (tW - mkInt(img.style.width)) * 0.5 + 'px',
                                        duration: 1,
                                        ease: Power2.easeIn
                                    });

                                });
                           }
            });
            }
        }
    }

    function mkAbout(aboutTxt, aboutDiv){

        aboutDiv.style.zIndex = 12;
        if(aboutTxt.includes('\n')){
            aboutDiv.innerHTML = '';
            let abtSplit = aboutTxt.split('\n');

            for(i in abtSplit){
                let pTag = document.createElement('p');
                pTag.className = 'videoDesription_P';
                pTag.innerHTML = abtSplit[i];
                aboutDiv.appendChild(pTag);
            }
        }else{
            aboutDiv.innerHTML = aboutTxt;
        }
    }

    function splashAction(){
        let vidSplash = document.getElementById('splashBG');
        
        if(showSplash === true){
            noSplash();
        }
        else{
            fullScreenSplash();
        }
    }

    function noSplash(){
        // remove event listener for video end
        showSplash = false;
        gsap.to(ellipse, {duration: 0.1, scaleX: 0.0, scaleY: 0.0,
                          x: mkInt(btnH)*0.1, y: mkInt(btnW)*0.1, overwrite: true});
        video.removeEventListener('ended', fullScreenSplash);
        if(document.getElementById('splash')){
            let splash = document.getElementById('splash');
            splash.parentNode.removeChild(splash);
            splash = null;
        }
    }

    function shoMkSplash(){
        // gives a button to control our splash.

        let fsBtn = document.getElementById('fScreenBtn');

        let splashBG = document.createElement('div');
        splashBG.id = 'splashBG';
        splashBG.className = 'rSizeRC';
        splashBG.style.position = 'absolute';
        splashBG.style.overflow = 'hidden';
        splashBG.style.bottom = mkInt(fsBtn.style.bottom) + mkInt(fsBtn.style.height) * 1.2 + '0px';
        splashBG.style.left = fsBtn.style.left;
        splashBG.style.height = '0px';
        splashBG.style.width = fsBtn.style.width;
        splashBG.style.display = 'block';

        let splashBtn = document.createElement('div');
        splashBtn.id = 'showSplash_btn';
        splashBtn.style.position = 'absolute';
        splashBtn.style.bottom = '0px';
        splashBtn.style.left = '0px';
        splashBtn.style.height = fsBtn.style.height;
        splashBtn.style.width = fsBtn.style.width;

        splashBtn.appendChild(infoBG);
        splashBG.appendChild(splashBtn);

        sideCtrl.appendChild(splashBG);

        splashBtn.addEventListener('click', splashAction, false);

        // let fsBtn = document.getElementById('fScreenBtn');
        let splashTween = gsap.to(splashBG, {duration: 0.1, height: mkInt(fsBtn.style.height) + 5 + 'px', overwrite: true});

        fsBtn.addEventListener('mouseover', function(){splashTween.play(), false});
        fsBtn.addEventListener('mouseout', function(){splashTween.reverse(), false});
        splashBG.addEventListener('mouseover', function(){splashTween.play(), false});
        splashBG.addEventListener('mouseout', function(){splashTween.reverse(), false});
    }

    function vidSwich(tLoc, oVbj){

        // if has top location make link absolute
        if(tLoc === ''){
            video.poster = oVbj.images[Object.keys(oVbj.images)[0]].loc;
            video.innerHTML = "<source src="+oVbj.loc+'/'+oVbj.mp4[setCtrl.uSetRez]+" type='video/mp4'>\n<source src="+oVbj.loc+'/'+oVbj.webm[setCtrl.uSetRez]+" type='video/webm'>";
        }
        else{
            tLoc = tLoc+'/';
            video.poster = tLoc+oVbj.images[Object.keys(oVbj.images)[0]].loc;
            video.innerHTML = "<source src="+tLoc+oVbj.loc+'/'+oVbj.mp4[setCtrl.uSetRez]+" type='video/mp4'>\n<source src="+tLoc+oVbj.loc+'/'+oVbj.webm[setCtrl.uSetRez]+" type='video/webm'>";
        }
        
        buffedOut.innerHTML = '';
        video.load();
        video.addEventListener('progress', getProgress, false);
        seek.value = 1;
    }

    function getTextWidth(text, font) {
        // re-use canvas object for better performance
        let canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
        let context = canvas.getContext("2d");
        context.font = font;
        let metrics = context.measureText(text);
        return Math.ceil(metrics.width);
    }

    function txtScroller(txt, txtWidth, txtHold){
        
        let time = txtWidth / txtHold * 3.5 ;
        let timeStart = time * .75;
        let timeEnd = time * .25;

        if(!gsap.isTweening(txt)){
            let txTween = gsap.to(txt, {
                duration: timeStart,
                right: txtHold + 3 + 'px',
                delay: 0.2,
                ease: Power0.easeIn,
                onComplete:function(){
                    gsap.fromTo(txt, {
                        duration: timeEnd,
                        right: - txtWidth
                    }, {
                        right: scrolTxtPos(txtHold, txtWidth),
                        ease: Sine.easeOut
                    });
                }
            });
        }
    }

    function getTopDir(){
        if(setCtrl.topDir === ''){
            if(objct.settings.topDir === ''){
                return '';
            }else{
                return objct.settings.topDir;
            }
        }else{
            return setCtrl.topDir;
        }
    }

    function strFix(fStr){
        // Not used anywhere yet
        return fStr.replace(/_/g, ' ');
    }

    function strUnFix(fStr){
        return fStr.replace(/ /g, '_');
    }

    function hexToRgb(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    function isDark(color, threshold){

        if(threshold == undefined || threshold < 2){
            threshold = 111;
        }
        let colorObj, red, green, blue, num;

        colorObj = hexToRgb(color.replace('#', ''));
        red = colorObj.r;
        green = colorObj.g;
        blue = colorObj.b;
        num = ((red * 299) + (green * 587) + (blue * 114)) / 1000;

        if(num >= threshold){
            return false;
        }else{
            return true;
        }
    }

    function onZoom(event){
        if(event.ctrlKey && [96, 107, 109, 61, 173].includes(event.keyCode)){
            console.log(event.keyCode);
            window.location.reload(false);
        }
    }
}