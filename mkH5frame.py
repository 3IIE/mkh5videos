# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Feb 16 2016)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.richtext

###########################################################################
## Class mkHframe
###########################################################################

def create(parent):
    return mkHframe(parent)

class mkHframe ( wx.Frame ):
    
    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"mkH5videos", pos = wx.DefaultPosition, size = wx.Size( 1024,786 ), style = wx.DEFAULT_FRAME_STYLE|wx.DOUBLE_BORDER|wx.TAB_TRAVERSAL, name = u"mkH5videos" )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        self.SetExtraStyle( wx.FRAME_EX_METAL )
        self.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.main_menubar = wx.MenuBar( 0 )
        self.main_menubar.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
        self.main_menubar.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.file_menu = wx.Menu()
        self.openFile_menuItem = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Open File", wx.EmptyString, wx.ITEM_NORMAL )
        self.file_menu.AppendItem( self.openFile_menuItem )
        
        self.openProject_menuItem = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Open mkH5 Project", wx.EmptyString, wx.ITEM_NORMAL )
        self.file_menu.AppendItem( self.openProject_menuItem )
        
        self.file_menu.AppendSeparator()
        
        self.saveStyle_menuItem = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Save Player Style", wx.EmptyString, wx.ITEM_NORMAL )
        self.file_menu.AppendItem( self.saveStyle_menuItem )
        
        self.saveProject_menuItem = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Save mkH5 Project", wx.EmptyString, wx.ITEM_NORMAL )
        self.file_menu.AppendItem( self.saveProject_menuItem )
        
        self.file_menu.AppendSeparator()
        
        self.exit_menuItem = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
        self.file_menu.AppendItem( self.exit_menuItem )
        
        self.main_menubar.Append( self.file_menu, u"File" ) 
        
        self.help_menu = wx.Menu()
        self.about_menuItem = wx.MenuItem( self.help_menu, wx.ID_ANY, u"About mkH5videos", wx.EmptyString, wx.ITEM_NORMAL )
        self.help_menu.AppendItem( self.about_menuItem )
        
        self.main_menubar.Append( self.help_menu, u"Help" ) 
        
        self.SetMenuBar( self.main_menubar )
        
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.topBottom_splitter = wx.SplitterWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,200 ), wx.SP_3D )
        self.topBottom_splitter.Bind( wx.EVT_IDLE, self.topBottom_splitterOnIdle )
        
        self.topBottom_splitter.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
        self.topBottom_splitter.SetBackgroundColour( wx.Colour( 128, 128, 128 ) )
        
        self.top_panel = wx.Panel( self.topBottom_splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.top_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        topSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.leftRight_splitter = wx.SplitterWindow( self.top_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D|wx.SP_3DBORDER )
        self.leftRight_splitter.Bind( wx.EVT_IDLE, self.leftRight_splitterOnIdle )
        
        self.leftRight_splitter.SetBackgroundColour( wx.Colour( 128, 128, 128 ) )
        
        self.browser_panel = wx.Panel( self.leftRight_splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.browser_panel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
        self.browser_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        browserSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.browser_toolBar = wx.ToolBar( self.browser_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
        self.browser_toolBar.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.back_tool = self.browser_toolBar.AddLabelTool( wx.ID_ANY, u"Back", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, u"Back a Page", wx.EmptyString, None ) 
        
        self.forward_tool = self.browser_toolBar.AddLabelTool( wx.ID_ANY, u"Forward", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, u"Forward a page", wx.EmptyString, None ) 
        
        self.URL_textCtrl = wx.TextCtrl( self.browser_toolBar, wx.ID_ANY, u"http://localhost:8088/index.html", wx.DefaultPosition, wx.Size( 400,-1 ), wx.TE_AUTO_URL )
        self.browser_toolBar.AddControl( self.URL_textCtrl )
        self.mkH5_tool = self.browser_toolBar.AddLabelTool( wx.ID_ANY, u"mkH5", wx.NullBitmap, wx.NullBitmap, wx.ITEM_NORMAL, u"Reload mkH5videos", wx.EmptyString, None ) 
        
        self.browser_toolBar.Realize() 
        
        browserSizer.Add( self.browser_toolBar, 0, wx.EXPAND, 5 )
        
        self.browser_notebook = wx.Notebook( self.browser_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.browser_notebook.SetForegroundColour( wx.Colour( 80, 80, 80 ) )
        self.browser_notebook.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.Web = wx.Panel( self.browser_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.FULL_REPAINT_ON_RESIZE|wx.TAB_TRAVERSAL )
        self.Web.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.browser_notebook.AddPage( self.Web, u"mkH5videos", True )
        self.HTML_panel = wx.Panel( self.browser_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.FULL_REPAINT_ON_RESIZE|wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
        self.HTML_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        htmlSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.html_richText = wx.richtext.RichTextCtrl( self.HTML_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
        self.html_richText.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        htmlSizer.Add( self.html_richText, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        self.HTML_panel.SetSizer( htmlSizer )
        self.HTML_panel.Layout()
        htmlSizer.Fit( self.HTML_panel )
        self.browser_notebook.AddPage( self.HTML_panel, u"HTML", False )
        
        browserSizer.Add( self.browser_notebook, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        self.browser_panel.SetSizer( browserSizer )
        self.browser_panel.Layout()
        browserSizer.Fit( self.browser_panel )
        self.right_panel = wx.Panel( self.leftRight_splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
        self.right_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.leftRight_splitter.SplitVertically( self.browser_panel, self.right_panel, 710 )
        topSizer.Add( self.leftRight_splitter, 1, wx.EXPAND, 5 )
        
        
        self.top_panel.SetSizer( topSizer )
        self.top_panel.Layout()
        topSizer.Fit( self.top_panel )
        self.bottom_panel = wx.Panel( self.topBottom_splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        self.bottom_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        boittomSizer = wx.BoxSizer( wx.VERTICAL )
        
        self.encoderConsole_notebook = wx.Notebook( self.bottom_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.encoderConsole_notebook.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.encoder_panel = wx.Panel( self.encoderConsole_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.FULL_REPAINT_ON_RESIZE|wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
        self.encoder_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.encoderConsole_notebook.AddPage( self.encoder_panel, u"Video Encoder", False )
        self.console_panel = wx.Panel( self.encoderConsole_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.FULL_REPAINT_ON_RESIZE|wx.SUNKEN_BORDER|wx.TAB_TRAVERSAL )
        self.console_panel.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        self.encoderConsole_notebook.AddPage( self.console_panel, u"Console", False )
        
        boittomSizer.Add( self.encoderConsole_notebook, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        self.bottom_panel.SetSizer( boittomSizer )
        self.bottom_panel.Layout()
        boittomSizer.Fit( self.bottom_panel )
        self.topBottom_splitter.SplitHorizontally( self.top_panel, self.bottom_panel, 604 )
        mainSizer.Add( self.topBottom_splitter, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( mainSizer )
        self.Layout()
        self.mkH5_statusBar = self.CreateStatusBar( 2, wx.ST_SIZEGRIP|wx.ALWAYS_SHOW_SB, wx.ID_ANY )
        self.mkH5_statusBar.SetBackgroundColour( wx.Colour( 64, 64, 64 ) )
        
        
        self.Centre( wx.BOTH )
    
    def __del__( self ):
        pass
    
    def topBottom_splitterOnIdle( self, event ):
        self.topBottom_splitter.SetSashPosition( 604 )
        self.topBottom_splitter.Unbind( wx.EVT_IDLE )
    
    def leftRight_splitterOnIdle( self, event ):
        self.leftRight_splitter.SetSashPosition( 710 )
        self.leftRight_splitter.Unbind( wx.EVT_IDLE )
    
if __name__ == '__main__':
    app = wx.App()
    frame = create(None)
    frame.Show()
    app.MainLoop()
